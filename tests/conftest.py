import pytest
from .context import adw
import pandas as pd
import os

NON_MCC_CLIENT = '3696721160'
ACCOUNT_CID = '5016858558'
API_VERSION = "v201809"  # would also need to upgrade lib > pip install --upgrade googlead

# Custome functions for testing
def getSharedBudgetConstant():
    """
    Returns:
        dict for shared budgets for initial setup of test account before pytest  
    """

    shared_budgets = [('Budget_1', 2), ('Budget_2', 2)]
    return shared_budgets

def getCampaignConstants():
    """
    Returns:
        dataframe for campaigns details for initial setup of test account before pytest runs 
    """
    campaign_df = {
        'CampaignName':['G:Generic:HSBC:M:Exact', 'P:ICBC:C:D:Broad'],
        'AdGroupName':['adGroup1', 'adGroup2'],
        'Keyword':['Initial_kw_1', 'Initial_kw_2']
    }
    campaign_df = pd.DataFrame(campaign_df)
    return campaign_df

def getNegativeSharedSet():
    """
    Returns:
        name for shared negative set for initial setup of test account before pytest runs 
    """
    name_neg_shared_set = 'First_One'
    return name_neg_shared_set

def getTargetCPA():
    """
    Returns:
        list of tuples in form [(<name>, <cpa_in_dollars>), ...]
    """
    target_CPA = [('tCPA1', 2), ('tCPA2', 2)]
    return target_CPA

def getTextLabel():
    """
    Returns:
        a simple text label object
    """
    text_label = {
        'label_name':'test_1',
        'label_color':'#ffff00',
        'label_desc':'for testing'
    }
    return text_label

def checkAvailableTextLabels(label_name):
    """
    Checks if the a label with label_name exists in the test accout or not

    Args:
        label_name: Name of the label
    
    Returns:
        boolean value; True when same label name exists and False when it doesn't
    """
    client = adw.get_adwords_client()
    client.SetClientCustomerId(NON_MCC_CLIENT)
    label_svc = client.GetService('LabelService', version=API_VERSION)
    sel = adw.create_selector(cols_list=['LabelId', 'LabelName'])
    response = label_svc.get(sel) 
    label_names = [
        val['name'] for val in response['entries']
    ]
    return (label_name in label_names)

def getExpandedTextAdObj():
    """
    Returns:
        Object for expanded text ad
    """
    from datetime import datetime
    return ({
        'xsi_type': 'ExpandedTextAd',
        'headlinePart1': ( datetime.now().strftime('%Y-%m-%d %H:%M:%S')),
        'headlinePart2': 'Testing headline 2',
        'headlinePart3': 'Testing headline 3',
        'description': 'Testing description',
        'description2': 'Testing description 2',
        'finalUrls': ['http://testing.com/'],
        })
    
def getAdsActiveCamAdGroups(client):
    """
    In few cases the campaings and adgroups are/were removed  before removing the ads. Adwords does not allows removing
    ads with already removed campaign/adgroups. This functions filter ads that have ENABLED campaigns & adgroups

    Args:
        client: NON_MCC_Client Obj

    Returns:
        list of ads: returns list of ads with active campaigns/adgroups
    """
    sel_ad = adw.create_selector(['Id', 'AdGroupId', 'Status','CampaignId'])     
    ad_group_ad_service = client.GetService('AdGroupAdService', version=API_VERSION)                                
    response_ads = ad_group_ad_service.get(sel_ad)['entries']

    #getting ad_group_ids
    ad_group_ids = adw.get_all_but_removed_campaigns_ad_adgroups(client)['AdGroupId'].tolist()
    
    #filter out ads of old ad group ids
    ads = []
    for val in response_ads:
        if(str(val['adGroupId']) in ad_group_ids):
            ads.append(val)
    return ads

def pytest_sessionstart(session):
    print('session start')
    """
    1. creates budget with values (call getSharedBudgetConstant()):
         [('Budget_1', 2), ('Budget_2', 2)]
    2. creates campaigns with values (call get_campaigns_function()):
        {
        'CampaignName':['G:Generic:HSBC:E:E', 'P:ICBC:C:D:Broad'],
        'AdGroupName':['adGroup1', 'adGroup2'],
        'Keyword':['Initial_kw_1', 'Initial_kw_2']
        }
    """

    #remvoing from the previous test session, if anything is not removed
    sessionfinish()

    # Initiate everything in this function  before the session starts
    #getting non-mcc client 
    client = adw.get_adwords_client()
    client.SetClientCustomerId(NON_MCC_CLIENT)

    #creating shared budgets
    shared_budgets = getSharedBudgetConstant()
    creating_sb_response = adw.create_shared_budgets(client, shared_budgets=shared_budgets)
    budget_df = {
        'budgetId':[],
        'budgetName':[]
    }
    for key in creating_sb_response:
        budget_df['budgetId'].append(creating_sb_response[key])
        budget_df['budgetName'].append(key)
    budget_df = pd.DataFrame(budget_df)

    #creating campaigns
    info_df = getCampaignConstants()
    budget_mapper = lambda e: budget_df['budgetId'][0]
    #getting the batch job helper
    batch_job_helper = adw.get_batch_job_helper(client)
    #getting cm_ops, ap_ops, kw_ops lists for testing purposes
    cm_ops, ag_ops, kw_ops = adw.create_bj_ops_for_campaigns(info_df, batch_job_helper, budget_mapper,  max_cpc=0.02 )
    #testing run_batch_create; parameters tested campaign_ops, adgroup_ops, keyword_ops
    adw.run_batch_create(client, batch_job_helper, cm_ops, ag_ops, kw_ops)

    #creating shared negative set
    adw.create_shared_negatives_set(client, getNegativeSharedSet())

    #creating target CPA bidding strategies in the account
    target_CPA = getTargetCPA()
    bid_strat_svc = client.GetService('BiddingStrategyService', version=API_VERSION)
    ops = [{'operator': 'ADD',
            'operand': {'name': tgt[0],
                        'type': 'TARGET_CPA',
                        'biddingScheme': {'xsi_type': 'TargetCpaBiddingScheme',
                                          'targetCpa': {'microAmount': tgt[1] * 1000000}
                                          }
                        }
            } for tgt in target_CPA]
    response = bid_strat_svc.mutate(ops)

    #creating a text label
    #checking if the same text label already exists
    if(not(checkAvailableTextLabels(getTextLabel()['label_name']))):
        label_svc = client.GetService('LabelService', version=API_VERSION)
        text_label = getTextLabel()
        op = {'operator': 'ADD',
            'operand': {'name': text_label['label_name'],
                        'xsi_type': 'TextLabel'}
            }
        color, desc = {}, {}
        color = {'xsi_type': 'DisplayAttribute',
                    'backgroundColor': text_label['label_color']}
        desc = {'xsi_type': 'DisplayAttribute',
                'description': text_label['label_desc']}
        op['operand']['attribute'] = {**color, **desc}
        response = label_svc.mutate(op)

    #creating expanded text ads
    ad_group_ids = adw.get_all_but_removed_campaigns_ad_adgroups(client)['AdGroupId'].tolist()
    ops = []
    for ad_group_id in ad_group_ids:
        for i in range(0, 2):
            ops.append({
                'operator': 'ADD',
                'operand': {
                    'xsi_type': 'AdGroupAd',
                    'adGroupId': ad_group_id,
                    'ad': getExpandedTextAdObj()
                }
            })
    ad_group_ad_service = client.GetService('AdGroupAdService', version=API_VERSION)
    ads_response = ad_group_ad_service.mutate(ops)
    


@pytest.fixture(autouse=True, scope='session')
def get_client():
    """
    Returns a client - mcc
    """
    client = adw.get_adwords_client()
    print
    yield client
    print('done')

@pytest.fixture(autouse=True, scope='session')
def get_client_non_mcc():
    """
    Returns a non_mcc client
    """
    client = adw.get_adwords_client()
    client.SetClientCustomerId(NON_MCC_CLIENT)
    yield client
    print('done')

@pytest.fixture(autouse=True, scope='session')
def get_raw_api_df():
    """
    Returns sample report dataframe stored locally
    """
    current_dir = os.getcwd()
    full_path = os.path.join(current_dir,'tests/DATA/uncleaned_data.csv')
    raw_df = pd.read_csv(full_path)
    yield raw_df
    print('done')

@pytest.fixture(autouse=True, scope='session')
def get_rsa_sample():
    """
    Returns a sample of Responsive Search Ad
    """
    headlines = ['headline1', 'headline2', 'headline3']
    descriptions = ['description1', 'description2', 'description3', 'description4']
    headline_list = []
    for headline in headlines:
        if type(headline) == str:
            headline_list.append({
                'asset': {
                    'xsi_type': 'TextAsset',
                    'assetText': headline
                }
            })
        elif type(headline) == tuple or type(headline) == list:
            headline_list.append({
                'asset': {
                    'xsi_type': 'TextAsset',
                    'assetText': headline[0]
                },
                'pinnedField': headline[1]  # e.g., HEADLINE_1 (or 2 or 3)
            })
    description_list = []
    for description in descriptions:
        if type(description) == str:
            description_list.append({
                'asset': {
                    'xsi_type': 'TextAsset',
                    'assetText': description
                }
            })
        elif type(description) == tuple or type(description) == list:
            description_list.append({
                'asset': {
                    'xsi_type': 'TextAsset',
                    'assetText': description[0]
                },
                'pinnedField': description[1]  # e.g., DESCRIPTION_1 (or 2)
            })
    rsa_sample = {
        'xsi_type': 'AdGroupAd',
        'adGroupId': 'adgroup_id',
        'ad': {
            'xsi_type': 'ResponsiveSearchAd',
            'finalUrls': ['finalUrl1'],
            'headlines': headline_list,
            'descriptions': description_list,
            'path1':'pathOne',
            'path2':'path2',
        }
    }

    rsa_test = {
        'rs_sample':rsa_sample,
        'descriptions':descriptions,
        'headlines':headlines
    }
    yield rsa_test
    print('done')

@pytest.fixture(autouse=True, scope='function')
def get_df_camp_ad_keyW():
    # Limit the scope of this fixture to funtion and do this for rest as well
    """
    Returns input dataframe for creating campaingns, group name, keyword
    """
    info_df = {
        'CampaignName':['G:Generic:TESTFUNC1:D:Broad', 'P:Amex:TESTFUNC2:D:Broad'],
        'AdGroupName':['TEST_FUNC1_adg1', 'TEST_FUNC1_adg2'],
        'Keyword':['hello', 'seeya']
    }
    info_df = pd.DataFrame(info_df)
    yield info_df

@pytest.fixture(autouse=True, scope='function')
def get_df_camp_ad_keyW_1():
    # Limit the scope of this fixture to funtion and do this for rest as well
    """
    Returns input dataframe for creating campaingns, group name, keyword
    """
    info_df = {
        'CampaignName':['G:Generic:TESTFUNC:D:Broad', 'P:Amex:TESTFUNC:D:Broad'],
        'AdGroupName':['TEST_FUNC1', 'TEST_FUNC1'],
        'Keyword':['hello', 'seeya']
    }
    info_df = pd.DataFrame(info_df)
    yield info_df
    
@pytest.fixture(autouse=True, scope='function')
def get_shared_budgets_wNID():
    """
    Returns details of available shared budgets on client acc from local copy 
    """
    shared_budgets = getSharedBudgetConstant()
    shared_budgets_df = []
    for key in shared_budgets:
        shared_budgets_df.append(key)
    shared_budgets_df = pd.DataFrame({'Name':shared_budgets_df})
    yield shared_budgets_df

@pytest.fixture(autouse=True, scope='function')
def get_shared_budgets_wID():
    """
    Returns details of available shared budgets on client acc, retreived from API (i.e. includes ID)
    """
    client = adw.get_adwords_client()
    client.SetClientCustomerId(NON_MCC_CLIENT)
    shared_budgets = adw.get_shared_budgets(client)
    yield shared_budgets

@pytest.fixture(autouse=True, scope='function')
def get_campaigns_details_wNID():
    """
    Returns details of available shared budgets on client acc from local copy
    """
    campaigns = getCampaignConstants()
    yield campaigns
    
@pytest.fixture(autouse=True, scope='function')
def get_shared_set_wID():
    """
    Returns details as df of available shared sets on client acc, retreived from API (i.e. includes ID)
    """
    client = adw.get_adwords_client()
    client.SetClientCustomerId(NON_MCC_CLIENT)
    shared_sets = adw.get_all_shared_sets(client)
    yield shared_sets

@pytest.fixture(autouse=True, scope='function')
def get_shared_set_wNID():
    """
    Returns name of available shared set on client acc from local copy 
    """
    shared_negative_name = getNegativeSharedSet()
    yield shared_negative_name

@pytest.fixture(autouse=True, scope='function')
def get_campaigns_details_wID():
    """
    Returns details of available campaign,  retreived from API (i.e. includes ID)
    """
    client = adw.get_adwords_client()
    client.SetClientCustomerId(NON_MCC_CLIENT)
    yield adw.get_all_but_removed_campaigns_ad_adgroups(client)  

@pytest.fixture(autouse=True, scope='function')
def get_tCPA_wNID():
    """
    Returns target CPA strategies from local copy 
    """
    tCPA = getTargetCPA()
    tCPA_df = []
    for i in tCPA:
        tCPA_df.append(i[0])
    tCPA_df = pd.DataFrame({
                'Name':tCPA_df
                })
    yield tCPA_df

@pytest.fixture(autouse=True, scope='function')
def get_tCPA_wID():
    """
    Returns details of target CPA strategies on client acc, retreived from API (i.e. includes ID)
    """
    client = adw.get_adwords_client()
    client.SetClientCustomerId(NON_MCC_CLIENT)
    selector = adw.create_selector(cols_list=['BiddingScheme', 'Id', 'Name', 'Status', 'Type'],pred_fields=['Status'],pred_operators=['EQUALS'], pred_values=['ENABLED'])
    yield adw.get_paged_data_from_svc('BiddingStrategyService', selector=selector, adwords_client=client)

@pytest.fixture(autouse=True, scope='function')
def get_text_label_wNID():
    """
    Returns text label from local copy 
    """
    text_label = getTextLabel()
    yield text_label

@pytest.fixture(autouse=True, scope='function')
def get_text_label_wID():
    """
    Returns list of text labels on client acc, retreived from API (i.e. includes ID)
    """
    client = adw.get_adwords_client()
    client.SetClientCustomerId(NON_MCC_CLIENT)
    svc = client.GetService('LabelService', version=API_VERSION)
    sel = adw.create_selector(cols_list=['LabelId', 'LabelName'])
    response = svc.get(sel) 
    yield response['entries']

    
@pytest.fixture(autouse=True, scope='function')
def get_ads_wID():
    """
    Returns list of ads on client acc, retreived from API (i.e. includes ID)
    """
    client = adw.get_adwords_client()
    client.SetClientCustomerId(NON_MCC_CLIENT)
    ads = getAdsActiveCamAdGroups(client)   
    yield ads    
    


@pytest.fixture(autouse=True, scope='function')
def get_ad_extension_account():
    yield([
       { 'STRUCTURED_SNIPPET':[{'xsi_type': 'StructuredSnippetFeedItem', 'header': 'Brands', 'values': ['HSBC', 'UOB', 'BOC']}]},
        {'CALLOUT':[{'calloutText': 'Completely free of charge', 'xsi_type': 'CalloutFeedItem'}]},
       { 'SITELINK':[{'sitelinkFinalUrls': {'urls': ['https://www.moneymax.ph/car-insurance']},
                    'sitelinkLine2': 'Your Car Insurance Expiring?',
                    'sitelinkLine3': 'Get The Best Car Insurance Here!',
                    'sitelinkText': 'Policy Expiring',
                    'xsi_type': 'SitelinkFeedItem'}
        ]},
        {'CALL':[{'xsi_type': 'CallFeedItem',
                'callPhoneNumber': '021-501-01707',
                'callCountryCode': 'ID',
                'callTracking': False} ]
        }])

@pytest.fixture(autouse=True, scope='function')
def get_ad_extension_campaign():
    client = adw.get_adwords_client()
    client.SetClientCustomerId(NON_MCC_CLIENT)
    campaign_details=adw.get_all_but_removed_campaigns_ad_adgroups(client)  
    campaign_id=int(campaign_details['CampaignId'][0])
    yield([
       { 'STRUCTURED_SNIPPET':[{'campaignId':  campaign_id,'xsi_type': 'StructuredSnippetFeedItem', 'header': 'Brands', 'values': ['HSBC', 'UOB', 'BOC']} ]},
        {'CALLOUT':[{'campaignId':  campaign_id, 'calloutText': 'Completely free of charge', 'xsi_type': 'CalloutFeedItem'}]},
       { 'SITELINK':[{'campaignId':  campaign_id,
              'sitelinkFinalUrls': {'urls': ['https://www.moneymax.ph/car-insurance']},
              'sitelinkLine2': 'Your Car Insurance Expiring?',
              'sitelinkLine3': 'Get The Best Car Insurance Here!',
              'sitelinkText': 'Policy Expiring',
              'xsi_type': 'SitelinkFeedItem'}]},
        {'CALL':[{'campaignId':  campaign_id,
              'xsi_type': 'CallFeedItem',
              'callPhoneNumber': '021-501-01707',
              'callCountryCode': 'ID',
              'callTracking': False} ]
        }])

@pytest.fixture(autouse=True, scope='function')
def get_ad_extension_adgroup():
    client = adw.get_adwords_client()
    client.SetClientCustomerId(NON_MCC_CLIENT)
    campaign_details=adw.get_all_but_removed_campaigns_ad_adgroups(client)  
    adgroup_id=int(campaign_details['AdGroupId'][0])
    yield([
       { 'STRUCTURED_SNIPPET':[{'adGroupId':  adgroup_id,'xsi_type': 'StructuredSnippetFeedItem', 'header': 'Brands', 'values': ['HSBC', 'UOB', 'BOC']}]},
        {'CALLOUT':[{'adGroupId':  adgroup_id, 'calloutText': 'Completely free of charge', 'xsi_type': 'CalloutFeedItem'}]},
       { 'SITELINK':[{'adGroupId':  adgroup_id,
              'sitelinkFinalUrls': {'urls': ['https://www.moneymax.ph/car-insurance']},
              'sitelinkLine2': 'Your Car Insurance Expiring?',
              'sitelinkLine3': 'Get The Best Car Insurance Here!',
              'sitelinkText': 'Policy Expiring',
              'xsi_type': 'SitelinkFeedItem'}]},
        {'CALL':[{'adGroupId':  adgroup_id,
              'xsi_type': 'CallFeedItem',
              'callPhoneNumber': '021-501-01707',
              'callCountryCode': 'ID',
              'callTracking': False} ]
        }])

@pytest.fixture(autouse=True, scope='function')
def get_new_ads_df():
    """
    Fixture for yielding a dataframe containing new ads (Campaigns and Ads are mapped using Regex)
    """
    from datetime import datetime
    new_ads_df = []
    new_ads_obj = {
        'CampaignRegex':'Generic',
        'AdGroupRegex':'',
        'Headline1': ( datetime.now().strftime('%Y-%m-%d %H:%M:%S')),
        'Headline2': 'headline 2',
        'Description': 'headline 3',
        'Path1': 'description',
        'Path2': 'description 2',
        'Headline3': 'headline 4',
        'Description2':'description 3'
    }

    for i in range(0, 2):
        new_ads_df.append(new_ads_obj)
    
    new_ads_df = pd.DataFrame(new_ads_df)
    yield new_ads_df

@pytest.fixture(autouse=True, scope='function')
def get_lp_regex_df():
    yield pd.DataFrame([{ 
            'CampaignRegex':'Generic', 
            'AdGroupRegex':'', 
            'FinalURL':'http://example.com'}]) 
    
# resets the whole account. This means Removes everything from the account that was created for testing
def sessionfinish():
    print('session finished')
    #cleaning up the temporary data

    #getting the client
    client = adw.get_adwords_client()
    client.SetClientCustomerId(NON_MCC_CLIENT)

    #removing the adverstisements under camps
    ad_group_ad_service = client.GetService('AdGroupAdService', version='v201809')
    ads = getAdsActiveCamAdGroups(client)
    # Construct operations and delete ad.
    ops = [{
        'operator': 'REMOVE',
        'operand': {
            'xsi_type': 'AdGroupAd',
            'adGroupId': ad_obj['adGroupId'],
            'ad': {
                'id': ad_obj['ad']['id'],
            }
        }
    } for ad_obj in ads]
    if(len(ops)!=0):
        removed_response = ad_group_ad_service.mutate(ops)
        print('ads removed')

    #getting all campaigns and adgroups
    response_campaign = adw.get_all_but_removed_campaigns_ad_adgroups(client)
    #removing the campaigns
    campaign_service = client.GetService('CampaignService', API_VERSION)
    if(type(response_campaign) == type(pd.DataFrame())):
        ops = [{
        'operator': 'SET',
        'operand': {
            'id': campaign_id,
            'status': 'REMOVED'
        }     
        } for campaign_id in response_campaign['CampaignId']]
        if(len(ops)!= 0):
            removed_response = campaign_service.mutate(ops)
            print('Campaigns removed')

    #removing the budgets created in the fixtures
    selector = adw.create_selector(
        cols_list=['BudgetName', 'BudgetId', 'BudgetStatus'],
        pred_fields=['BudgetStatus'],
        pred_operators=['IN'],
        pred_values=['ENABLED']
    )
    enabled_budgets = adw.get_paged_data_from_svc('BudgetService', selector=selector, adwords_client=client)
    budget_service = client.GetService('BudgetService', version=API_VERSION)
    if (type(enabled_budgets) == type(pd.DataFrame())):
        ops = [{
        'operator': 'REMOVE',
        'operand': {
            'budgetId': budget_id,
            'status': 'REMOVED'
        }     
        } for budget_id in enabled_budgets['BudgetId']]
        if(len(ops)!=0):
            remove_response = budget_service.mutate(ops)
            print('Budgets removed')
    
    #removing the shared sets created during the test
    #removing the shared sets attached to the campaigns
    cols_list = ["SharedSetId", "CampaignId", "SharedSetName","SharedSetType", "Status"]                             
    selector_attached_camps = adw.create_selector(cols_list, pred_fields=['Status'], pred_operators=['IN'], pred_values=['ENABLED'])  
    campaign_shared_set_service = client.GetService('CampaignSharedSetService', version=API_VERSION)
    attached_camps_response = campaign_shared_set_service.get(selector_attached_camps)                          
    #creating a list of removing attached shared sets operations
    if(attached_camps_response!=None):
        cm_sets = [{'campaignId': info.campaignId, 'sharedSetId': info.sharedSetId} for info in attached_camps_response['entries']]
        if(len(cm_sets)!=0):   
            ops = [{'operator': 'REMOVE', 'operand': cs} for cs in cm_sets]
            remove_response = campaign_shared_set_service.mutate(ops)
            print('Removed shared sets from campaigns')
    #creating ops for removing all shared sets from client's account
    shared_sets = adw.get_all_shared_sets(client)
    shared_set_service = client.GetService('SharedSetService', version=API_VERSION)
    if(type(shared_sets) == type(pd.DataFrame())):
        ops = [{
        'operator': 'REMOVE',
        'operand': {
            'sharedSetId': shared_set_id,
            'status': 'REMOVED'
        }     
        } for shared_set_id in shared_sets['SharedSetId']]
        if(len(ops)!=0):
            remove_response = shared_set_service.mutate(ops)
            print('Shared sets removed')

    #removing tCPA strategies
    bid_strat_svc = client.GetService('BiddingStrategyService', version=API_VERSION)
    selector = adw.create_selector(cols_list=['BiddingScheme', 'Id', 'Name', 'Status', 'Type'],
                                pred_fields=['Status'],
                                pred_operators=['EQUALS'],
                                pred_values=['ENABLED'])
    existing_tCPAs_df = adw.get_paged_data_from_svc('BiddingStrategyService', selector=selector, adwords_client=client)
    if(type(existing_tCPAs_df)==type(pd.DataFrame())):
        ops = [{'operator': 'REMOVE',
                'operand': {'id': _id,
                            'type': 'TARGET_CPA',
                            'status':'REMOVED'
                            }
                } for _id in existing_tCPAs_df['Id']]
        if(len(ops)!=0):
            response = bid_strat_svc.mutate(ops)
            print('Targets CPAs Removed')
    
    #removing added text labels
    #getting ids of labels
    label_svc = client.GetService('LabelService', version=API_VERSION)
    sel = adw.create_selector(cols_list=['LabelId', 'LabelName'])
    response = label_svc.get(sel) 
    response = response['entries']
    ops = [{
            'operator': 'REMOVE',
            'operand': {
                 'id': vals.id,
                 'status':'REMOVED',
            }
        } for vals in response]
    if(len(ops)!=0):
        response = label_svc.mutate(ops)
        print('Labels Removed')
    
def pytest_sessionfinish(session, exitstatus):
    sessionfinish()