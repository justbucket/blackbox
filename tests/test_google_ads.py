import datetime as dt
import pytest
import pytz
import blackbox.ytilities.googleads
import pandas as pd
import os
import numpy as np
import re

from googleads.errors import GoogleAdsServerFault
from zeep.helpers import serialize_object

from .context import adw

current_date = dt.datetime.today().strftime('%Y%m%d')
LABEL_TEXT = 'account_label1'
NEW_ACCOUNT_NAME = f'AdWordsAPI_Test_Account_{current_date}'
ACCOUNT_CID = '5016858558'
NON_MCC_CLIENT = '3696721160'
API_VERSION = "v201809"  # would also need to upgrade lib > pip install --upgrade googlead


# For production testing, uncomment and set to 1+, for testing testing leave
# commented to set attempts to 0 and immediately raise Exception
# MAX_POLL_ATTEMPTS = 1

tz = pytz.timezone('Asia/Hong_Kong')
budget_id = None
campaign_id = None
adgroup_id = None
glovar = None


#Functions for testing 
def get_campaign_details():
    client = adw.get_adwords_client()
    client.SetClientCustomerId(NON_MCC_CLIENT)
    return adw.get_all_but_removed_campaigns_ad_adgroups(client) 

# # functions that don't require AdWords API
def test_clean_adwords_df_from_api():
    with pytest.raises(TypeError):
        adw.clean_adwords_df_from_api()


def test_clean_adwords_df_from_ui():
    with pytest.raises(TypeError):
        adw.clean_adwords_df_from_ui()


def test_create_eta_op():
    with pytest.raises(TypeError):
        adw.create_eta_op()

    assert adw.create_eta_op(adgroup_id=46234635636,
                             headline1='test headline part 1',
                             headline2='test headline part 2',
                             description='Nice ad description in here.',
                             finalUrls=['https://www.example.com'],
                             path1='world',
                             path2='credit-card') == {
               'xsi_type': 'AdGroupAdOperation',
               'operand': {'adGroupId': 46234635636,
                           'ad': {'xsi_type': 'ExpandedTextAd',
                                  'headlinePart1': 'test headline part 1',
                                  'headlinePart2': 'test headline part 2',
                                  'description': 'Nice ad description in here.',
                                  'finalUrls': [
                                      'https://www.example.com'],
                                  'path1': 'world',
                                  'path2': 'credit-card'}},
               'operator': 'ADD'
           }


def test_create_keyword_op():
    with pytest.raises(TypeError):
        adw.create_keyword_op()

    assert adw.create_keyword_op(adgroup_id=46234635636,
                                 keyword='best credit card',
                                 match_type='BROAD',
                                 finalUrls=['https://www.example.com/credit-card/best-deals']) == {
               'xsi_type': 'AdGroupCriterionOperation',
               'operand': {'xsi_type': 'BiddableAdGroupCriterion', 'adGroupId': 46234635636,
                           'criterion': {'xsi_type': 'Keyword', 'text': 'best credit card', 'matchType': 'BROAD'},
                           'finalUrls': ['https://www.example.com/credit-card/best-deals']}, 'operator': 'ADD'
           }


def test_create_selector():
    with pytest.raises(TypeError):
        adw.create_selector()

    with pytest.raises(Exception):
        adw.create_selector(cols_list=['one', 'two'],
                            pred_fields=['one', 'two'],
                            pred_operators=['one', 'two'],
                            pred_values=['one', 'two', 'three'])

    assert adw.create_selector(cols_list=['AccountLabels',
                                          'CanManageClients',
                                          'CompanyName',
                                          'CurrencyCode',
                                          'CustomerId',
                                          'DateTimeZone',
                                          'Name',
                                          'TestAccount'],
                               pred_fields=['AccountLabels'],
                               pred_operators=['CONTAINS_ANY'],
                               pred_values=[['4038216L', '4038261L']],
                               date_from='20180101',
                               date_to='20180201') == {
               'fields':
                   ['AccountLabels',
                    'CanManageClients',
                    'CompanyName', 'CurrencyCode',
                    'CustomerId',
                    'DateTimeZone', 'Name',
                    'TestAccount'],
               'predicates': [
                   {'field': 'AccountLabels',
                    'operator': 'CONTAINS_ANY',
                    'values': ['4038216L',
                               '4038261L']}],
               'dateRange': {'max': '20180201', 'min': '20180101'}
           }


def test_read_adwords_csv():
    # this test requires an AdWords Campaign Report, found under
    # Reports -> Predefined reports -> Basic -> Campaign
    with pytest.raises(TypeError):
        adw.read_adwords_csv()

    assert adw.read_adwords_csv('tests/DATA/',
                                'campaign_report.csv',
                                sep=',',
                                skiprows=2,
                                encoding=None).columns.tolist() == ['Campaign',
                                                                    'Campaign state',
                                                                    'Campaign type',
                                                                    'Clicks',
                                                                    'Impressions',
                                                                    'CTR',
                                                                    'Avg. CPC',
                                                                    'Cost',
                                                                    'Conversions',
                                                                    'View-through conv.',
                                                                    'Cost / conv.',
                                                                    'Conv. rate']


def test_text2cols():
    with pytest.raises(TypeError):
        adw.text2cols()


################################################################################
# functions that require AdWords API
################################################################################
def test_get_adwords_client():
    # if adw.TEST_MCC_CID != TEST_MCC_CID:
    #     with pytest.raises(AssertionError):
    #         adw.get_adwords_client()
    #
    #     # patch TEST_MCC_CID
    #     adw.TEST_MCC_CID = TEST_MCC_CID
    #
    # adw.get_adwords_client()
    #
    # try:  # if using different production MCC
    #     if adw.CAG_TOP_MCC_ID != CAG_TOP_MCC_ID:
    #         with pytest.raises(AssertionError):
    #             adw.get_adwords_client(is_prod=True)
    #
    #         # patch CAG_TOP_MCC_ID
    #         adw.CAG_TOP_MCC_ID = CAG_TOP_MCC_ID
    # except:
    #     pass

    adw.get_adwords_client(is_prod=True)


def test_get_list_of_accounts():
    adwords_client = adw.get_adwords_client()

    assert type(adw.get_list_of_accounts(adwords_client,
                                         adw.TEST_MCC_CID)) == list

    adwords_client = adw.get_adwords_client(is_prod=True)

    assert type(adw.get_list_of_accounts(adwords_client,
                                         adw.CAG_TOP_MCC_ID)) == list


def test_get_account_label_ids_by_name():
    # todo: check with Yang how function works
    # todo: fix get_account_label_ids_by_name for this line:
    # if 'labels' in label_list:
    # should be if label_list is not None and 'labels' in label_list:

    with pytest.raises(TypeError):
        adw.get_account_label_ids_by_name()

    adwords_client = adw.get_adwords_client()

    assert type(adw.get_account_label_ids_by_name(LABEL_TEXT,
                                                  adwords_client)) == list


def test_get_accounts_info_by_label_ids():
    # todo: check with Yang how function works
    with pytest.raises(TypeError):
        adw.get_accounts_info_by_label_ids()

    adwords_client = adw.get_adwords_client()
    label_ids = adw.get_account_label_ids_by_name(LABEL_TEXT,
                                                  adwords_client)
    account_columns = ['AccountLabels', 'CanManageClients', 'CustomerId',
                       'Name', 'TestAccount']

    df = adw.get_accounts_info_by_label_ids(label_ids,
                                            account_columns)

    assert df.__class__.__name__ == 'DataFrame'

    assert len(df) > 0

    assert df.columns.tolist() == account_columns


def test_get_paged_data_from_svc():
    with pytest.raises(TypeError):
        adw.get_paged_data_from_svc()

    selector_columns = ['AccountLabels', 'CanManageClients', 'CustomerId',
                        'Name', 'TestAccount']

    selector = adw.create_selector(selector_columns)

    adwords_client = adw.get_adwords_client()

    df = adw.get_paged_data_from_svc('ManagedCustomerService',
                                     selector,
                                     adwords_client)
    assert df.__class__.__name__ == 'DataFrame'

    assert len(df) > 0

    assert df.columns.tolist() == selector_columns


def test_create_new_accounts():
    with pytest.raises(TypeError):
        adw.create_new_accounts()

    # first get list of existing accounts to check against NEW_ACCOUNT_NAME
    selector_columns = ['AccountLabels', 'CanManageClients', 'CustomerId',
                        'Name', 'TestAccount']

    selector = adw.create_selector(selector_columns)

    adwords_client = adw.get_adwords_client()

    df = adw.get_paged_data_from_svc('ManagedCustomerService',
                                     selector,
                                     adwords_client)

    account_name = NEW_ACCOUNT_NAME

    if account_name not in set(df.Name.values):
        new_accounts = adw.create_new_accounts(adwords_client,
                                               [account_name])

        assert account_name in new_accounts.keys()

    else:
        assert account_name in set(df.Name.values)


def test_create_shared_budgets():
    with pytest.raises(TypeError):
        adw.create_shared_budgets()

    budget_name = 'Test_Budget_{}'.format(
        dt.datetime.now(tz).strftime('%Y-%m-%d'))
    shared_budgets = [(budget_name, 100)]

    # retrieve customer_id for account name
    selector_columns = ['AccountLabels', 'CanManageClients', 'CustomerId',
                        'Name', 'TestAccount']
    pred_fields = ['Name']
    pred_operators = ['EQUALS']
    pred_values = [NEW_ACCOUNT_NAME],

    selector = adw.create_selector(selector_columns, pred_fields, pred_operators, pred_values)

    adwords_client = adw.get_adwords_client()

    df = adw.get_paged_data_from_svc('ManagedCustomerService',
                                     selector,
                                     adwords_client)

    if len(df.CustomerId.values) == 1:
        adwords_client.SetClientCustomerId(df.CustomerId.values[0])
    else:
        raise ValueError('Expected one and only one matching CustomerID')

    # retrieve budgets to check if any Enabled budgets with same name exist
    selector_columns = ['Amount', 'BudgetId', 'BudgetName',
                        'BudgetReferenceCount', 'BudgetStatus',
                        'DeliveryMethod', 'IsBudgetExplicitlyShared']

    selector = adw.create_selector(selector_columns)

    df = adw.get_paged_data_from_svc('BudgetService',
                                     selector,
                                     adwords_client)

    # create budget if not exists or retrieve budget id for existing budget
    global budget_id

    if (df is not None and
            len(df[(df.Name == budget_name) & (df.Status == 'ENABLED')]) > 0):
        with pytest.raises(GoogleAdsServerFault):
            adw.create_shared_budgets(adwords_client, shared_budgets)

        budget_id = df[(df.Name == budget_name) &
                       (df.Status == 'ENABLED')]['BudgetId'].values[0]

    else:
        create_shared_budget_results = adw.create_shared_budgets(
            adwords_client, shared_budgets)
        assert budget_name in create_shared_budget_results.keys()

        budget_id = create_shared_budget_results[budget_name]


def test_add_batch_job():
    with pytest.raises(TypeError):
        adw.add_batch_job()

    adwords_client = adw.get_adwords_client()

    assert set(
        serialize_object(
            adw.add_batch_job(
                adwords_client)).keys()) == {'diskUsageQuotaBalance',
                                             'downloadUrl',
                                             'id',
                                             'processingErrors',
                                             'progressStats',
                                             'status',
                                             'uploadUrl'}


def test_get_batch_job():
    with pytest.raises(TypeError):
        adw.get_batch_job()

    adwords_client = adw.get_adwords_client()

    batch_job_id = adw.add_batch_job(adwords_client)['id']

    assert adw.get_batch_job(adwords_client,
                             batch_job_id)['status'] == 'AWAITING_FILE'


def test_get_batch_job_download_url():
    with pytest.raises(TypeError):
        adw.get_batch_job_download_url()

    adwords_client = adw.get_adwords_client()
    batch_job_id = adw.add_batch_job(adwords_client)['id']

    try:
        max_poll_attempts = MAX_POLL_ATTEMPTS
    except:
        max_poll_attempts = 0

    with pytest.raises(Exception):
        adw.get_batch_job_download_url(adwords_client,
                                       batch_job_id,
                                       max_poll_attempts)


def test_create_campaign_op():
    with pytest.raises(TypeError):
        adw.create_campaign_op()

    adwords_client = adw.get_adwords_client()

    batch_job_helper = adwords_client.GetBatchJobHelper()

    create_campaign_op_results = adw.create_campaign_op(
        batch_job_helper, budget_id, 'test_campaign')

    assert type(create_campaign_op_results[0]) is int

    assert type(create_campaign_op_results[1]) is dict

    assert set(create_campaign_op_results[1].keys()) == {
        'operand', 'operator', 'xsi_type'}


def test_create_adgroup_op():
    with pytest.raises(TypeError):
        adw.create_adgroup_op()

    adwords_client = adw.get_adwords_client()

    batch_job_helper = adwords_client.GetBatchJobHelper()

    campaign_id = adw.create_campaign_op(batch_job_helper,
                                         budget_id,
                                         'test_campaign')[0]

    create_adgroup_op_results = adw.create_adgroup_op(
        batch_job_helper, campaign_id, 'test_adgroup')

    assert type(create_adgroup_op_results[0]) is int

    assert type(create_adgroup_op_results[1]) is dict

    assert set(create_adgroup_op_results[1].keys()) == {
        'operand', 'operator', 'xsi_type'}



# Testing rest of the functions
@pytest.mark.done
def test_text2cols():
    df = pd.read_csv(os.path.join(os.getcwd(), 'tests/DATA/campain_names.csv'))
    #testing without dropping the original column; rename=None
    new_dataframe = adw.text2cols(df, 'Campaign')
    assert(len(new_dataframe.columns) == 6)
    #testing with dropping the original column; rename=None
    new_dataframe = adw.text2cols(df, 'Campaign', drop_orig=True)
    assert(len(new_dataframe.columns) == 5)
    #testing naming the new columns; drop_orig = True
    new_cols = ['1', '2', '3', '4', '5']
    new_cols_dict = {0:'1', 1:'2', 2:'3', 3:'4', 4:'5'}
    new_dataframe = adw.text2cols(df, 'Campaign', drop_orig=True, rename=new_cols_dict)
    assert(len(new_dataframe.columns) == 5)
    assert([i for i,j in zip(new_cols, new_dataframe.columns) if i==j])

@pytest.mark.done
def test_create_rsa_op(get_rsa_sample):
    #creating a Responsive search ad
    created_rsa = adw.create_rsa_op(get_rsa_sample['rs_sample']['adGroupId'], get_rsa_sample['headlines'], get_rsa_sample['descriptions'], get_rsa_sample['rs_sample']['ad']['finalUrls'], get_rsa_sample['rs_sample']['ad']['path1'],get_rsa_sample['rs_sample']['ad']['path2'], None)
    assert(get_rsa_sample['rs_sample'] == created_rsa['operand'])
    assert(created_rsa['operator']=='ADD')
    
@pytest.mark.done
def test_create_bj_ops_for_campaigns(get_client, get_df_camp_ad_keyW, get_shared_budgets_wID):
    #getting bjh 
    batch_job_helper = adw.get_batch_job_helper(get_client)
    budget_mapper = lambda e: get_shared_budgets_wID['BudgetId'][0]

    #testing the function with input_df, bjh, budget_mapper, and rest of the parameters are default  
    #For passing it just evaluates the length of returned values for cm_ops, ag_ops, kw_ops against expected
    cm_ops, ag_ops, kw_ops = adw.create_bj_ops_for_campaigns(get_df_camp_ad_keyW, batch_job_helper,  budget_mapper)
    assert(len(cm_ops)==len(get_df_camp_ad_keyW['CampaignName']))
    assert(len(ag_ops)==len(get_df_camp_ad_keyW['AdGroupName']))
    assert(len(kw_ops)==len(get_df_camp_ad_keyW['Keyword']))

    """
    Left tests:
    1. default_ads, kw_url_mapper, cm_col, ag_col, kw_col, max_cpc; Only tested against their respective default values
    """

# @pytest.mark.left_rn
# def test_upsert_bj_ops_for_campaigns(get_client ,get_client_non_mcc, get_df_camp_ad_keyW_1, get_shared_budgets_wID):
#     #getting bjh 
#     batch_job_helper = adw.get_batch_job_helper(get_client_non_mcc)
#     budget_mapper = lambda e: get_shared_budgets_wID['BudgetId'][0]
#     print('budget_mapper', budget_mapper)

#     #testing the function with input_df, bjh, budget_mapper, and rest of the parameters are default  
#     dw = adw.upsert_bj_ops_for_campaigns(client=get_client_non_mcc ,input_df=get_df_camp_ad_keyW_1, bjh=batch_job_helper, budget_mapper=budget_mapper, max_cpc=0.02 )
#     print('wewewm;lk;mlme')
#     print(dw)
#     assert(False)
# #     assert(len(cm_ops)==len(get_df_camp_ad_keyW_1['CampaignFullName']))
# #     assert(len(ag_ops)==len(get_df_camp_ad_keyW_1['AdGroupName']))
# #     assert(len(kw_ops)==len(get_df_camp_ad_keyW_1['Keyword']))

#     """
#     Components not tested for the funtion:
#     1. Not tested parameters default_ads, kw_url_mapper, cm_col, ag_col, kw_col.
#     """

@pytest.mark.done
def test_run_batch_create(get_client_non_mcc, get_df_camp_ad_keyW, get_shared_budgets_wID):
    budget_mapper = lambda e: get_shared_budgets_wID['BudgetId'][0]
    #getting the batch job helper
    batch_job_helper = adw.get_batch_job_helper(get_client_non_mcc)

    #getting cm_ops, ap_ops, kw_ops lists for testing purposes
    cm_ops, ag_ops, kw_ops = adw.create_bj_ops_for_campaigns(get_df_camp_ad_keyW, batch_job_helper, budget_mapper,  max_cpc=0.02 )

    #testing run_batch_create; parameters tested campaign_ops, adgroup_ops, keyword_ops
    adw.run_batch_create(get_client_non_mcc, batch_job_helper, cm_ops, ag_ops, kw_ops)

    #getting all campaigns and adgroups
    response = adw.get_all_but_removed_campaigns_ad_adgroups(get_client_non_mcc)
    # verifying campaigns and adgroups have been created or not
    assert(get_df_camp_ad_keyW['CampaignName'].isin(response['CampaignName']).all())
    assert(get_df_camp_ad_keyW['AdGroupName'].isin(response['AdGroupName']).all())                                    
    """
    Left tests
    1. adcopy_ops; Not tested for any value except default value(=None)
    2. assert_paursed; Not tested for False
    """

@pytest.mark.done
def test_get_shared_budgets(get_client_non_mcc, get_shared_budgets_wNID):
    #getting shared budgets and check=
    shared_budgets = adw.get_shared_budgets(get_client_non_mcc)
    assert(name_local[0] == name_retrieved for name_local,name_retrieved in zip(get_shared_budgets_wNID,shared_budgets['Name']))
    """
    Left tests
    1. budgetStats; onty tested with default value (='ENABLED')
    """

@pytest.mark.done
def test_set_campaign_level_targeting(get_client_non_mcc, get_campaigns_details_wID):
    #getting all the campaign ids
    cms_ids = get_campaigns_details_wID['CampaignId'].tolist()
    #setting the list of criterias
    criteria_list = [{'xsi_type': 'Location', 'id': '2608'}, {'xsi_type': 'Language', 'id': '1000'}]
    response = adw.set_campaign_level_targeting(get_client_non_mcc,NON_MCC_CLIENT, cms_ids, criteria_list)
    response = response['value']

    #evaluating set_campaign_level_targeting  
    for _id in cms_ids:
        assert any(value['campaignId'] == int(_id) for value in response)

@pytest.mark.done
def test_create_shared_negatives_set(get_client_non_mcc):
    #negative list
    negative_shared_list = 'Second_One'

    #creating a shared negative set
    response = adw.create_shared_negatives_set(get_client_non_mcc, negative_shared_list)

    #getting all the shared sets
    shared_sets = adw.get_all_shared_sets(get_client_non_mcc)

    #evaluating if just created negative shared set is present in retrieved shared sets
    shared_set_ids = shared_sets['SharedSetId'].tolist()
    index_shared_set = shared_set_ids.index(int(response))
    assert (index_shared_set != -1)
    if(index_shared_set != -1):
        assert(shared_sets['Type'][index_shared_set]=='NEGATIVE_KEYWORDS')

@pytest.mark.done
def test_update_negatives_in_set(get_shared_set_wID, get_client_non_mcc):
    #updating negatives in set
    negative_kws = [('abc', 'EXACT'), ('xyz', 'PHRASE')]

    #testing with set_id
    set_id = get_shared_set_wID['SharedSetId'][0]
    response = adw.update_negatives_in_set(get_client_non_mcc, negative_kws, set_id)
    response = response[0]
    #evaluating the response
    assert(len(response.value)==2)
    assert all(value['sharedSetId']==int(set_id) for value in response.value)                                           
    for i in range(0, len(response.value)):
        assert(response.value[i]['criterion']['text']==negative_kws[i][0])
        assert(response.value[i]['criterion']['matchType']==negative_kws[i][1])

    #testing with set_name
    set_name = get_shared_set_wID['Name'][0]
    negative_kws = [('abcd', 'EXACT'), ('xyza', 'PHRASE')]
    response = adw.update_negatives_in_set(get_client_non_mcc, negative_kws, set_name=set_name)
    response = response[1]
    #evaluating the response
    assert(len(response.value)==2)
    assert all(value['sharedSetId']==int(set_id) for value in response.value)
    for i in range(0, len(response.value)):
        assert(response.value[i]['criterion']['text']==negative_kws[i][0])
        assert(response.value[i]['criterion']['matchType']==negative_kws[i][1])

@pytest.mark.done
def test_attach_negatives_to_campaigns(get_client_non_mcc,get_campaigns_details_wID , get_shared_set_wID):
    #testing the function 
    cm_list = [int(get_campaigns_details_wID['CampaignId'].tolist()[0])]
    set_ids = get_shared_set_wID['SharedSetId'].tolist()
    #attaching the negative to the campaign
    response = adw.attach_negatives_to_campaigns(get_client_non_mcc, cm_list, set_ids)
    value = response['value']
    val_index = 0
    for i in range(0,len(cm_list)):
        for j in range(0,len(set_ids)):
            assert(value[val_index].sharedSetId == set_ids[j])
            assert(value[val_index].campaignId == cm_list[i])
            assert(value[val_index].status == 'ENABLED')
            val_index += 1

@pytest.mark.done
def test_remove_negatives_from_campaigns(get_client_non_mcc, get_campaigns_details_wID, get_shared_set_wID):
    #getting the list of shared sets attached to campaigns
    cm_list = [int(get_campaigns_details_wID['CampaignId'].tolist()[0])]  # cm_list and sat_ids should be equivalent to their respective values in test_attach_negatives_to_campaigns
    set_ids = get_shared_set_wID['SharedSetId'].tolist()
    #removing the shared sets attached to campaigns
    response = adw.remove_negatives_from_campaigns(get_client_non_mcc, cm_list, set_ids)

    #evaluating remove_negatives_from_campaigns function
    cols_list = ["SharedSetId", "CampaignId", "SharedSetName","SharedSetType", "Status"]                             
    selector_attached_camps = adw.create_selector(cols_list, pred_fields=['Status'], pred_operators=['IN'], pred_values=['ENABLED'])  
    campaign_shared_set_service = get_client_non_mcc.GetService('CampaignSharedSetService', version=API_VERSION)
    attached_camps_response = campaign_shared_set_service.get(selector_attached_camps)                          
    assert(len(attached_camps_response['entries']) == 0)

@pytest.mark.done
def test_create_account_extensions(get_client_non_mcc, get_ad_extension_account):
    #creating account level extension
    response = adw.create_account_extensions(get_client_non_mcc, get_ad_extension_account[0]['STRUCTURED_SNIPPET'], get_ad_extension_account[1]['CALLOUT'],  get_ad_extension_account[2]['SITELINK'],  get_ad_extension_account[3]['CALL'] )
    value = response['value']
    for i in range(0, len(value)):
        assert(list(get_ad_extension_account[i].keys())[0] == value[i]['extensionType'])

@pytest.mark.done
def test_create_campaign_extensions(get_client_non_mcc, get_ad_extension_campaign):
    #creating account level extension
    response = adw.create_campaign_extensions(get_client_non_mcc, get_ad_extension_campaign[0]['STRUCTURED_SNIPPET'], get_ad_extension_campaign[1]['CALLOUT'],  get_ad_extension_campaign[2]['SITELINK'],  get_ad_extension_campaign[3]['CALL'] )
    value = response['value']
    for i in range(0, len(value)):
        assert(list(get_ad_extension_campaign[i].keys())[0] == value[i]['extensionType'])

@pytest.mark.done
def test_create_adgroup_extensions(get_client_non_mcc, get_ad_extension_adgroup):
    #creating account level extension
    print(get_ad_extension_adgroup[0]['STRUCTURED_SNIPPET'])
    response = adw.create_adgroup_extensions(get_client_non_mcc, get_ad_extension_adgroup[0]['STRUCTURED_SNIPPET'], get_ad_extension_adgroup[1]['CALLOUT'],  get_ad_extension_adgroup[2]['SITELINK'],  get_ad_extension_adgroup[3]['CALL'] )
    value = response['value']
    print(value)
    for i in range(0, len(value)):
        assert(list(get_ad_extension_adgroup[i].keys())[0] == value[i]['extensionType'])

@pytest.mark.done
def test_set_campaign_device_bid_modifiers(get_client_non_mcc, get_campaigns_details_wID):
    #creating df containing platform for each campaign
    campaign_details = adw.get_all_but_removed_campaigns_ad_adgroups(get_client_non_mcc)  
    campaign_details['Device'] = campaign_details['CampaignName'].apply(lambda x:x.split(':')[3])
    print('resss', campaign_details)
    #testing the function
    response = adw.set_campaign_device_bid_modifiers(get_client_non_mcc)
    value = response['value']
    print(value)
    i = 0
    j =0 
    
    for index, row in campaign_details.iterrows():
        count = 0
        for val in value:
            if(val['campaignId']==int(row['CampaignId'])):
                count +=1
                if(row['Device']=='M'):
                    assert(val['criterion']['platformName'] == 'Tablet' or val['criterion']['platformName'] == 'Desktop')
                else:
                    assert(val['criterion']['platformName'] == 'HighEndMobile')
        if(row['Device']=='M'):
            assert(count==2)
        else:
            assert(count==1)

@pytest.mark.done
def test_create_tCPA_strategies(get_client_non_mcc):
    #creating tCPA strategy
    target_CPA = [('tCePA1_test', 2)]
    response = adw.create_tCPA_strategies(get_client_non_mcc, target_CPA)

    #testing if the strategy has been added by retrieving exisitng tCPA strategies
    target_CPAs = adw.get_shared_bidding_strategies(get_client_non_mcc)
    assert(strat[0] in target_CPAs['Name'] for strat in target_CPA)

@pytest.mark.done
def test_get_shared_bidding_strategies(get_client_non_mcc, get_tCPA_wNID):
    #retrieving existing tCPAs
    retrieved_tCPAs = adw.get_shared_bidding_strategies(get_client_non_mcc)

    #evaluating sufficient added tCPAs are retrieved or not
    assert(get_tCPA_wNID['Name'].isin(retrieved_tCPAs['Name']).all())

@pytest.mark.done
def test_attach_fbs_to_campaigns(get_client_non_mcc, get_tCPA_wID, get_campaigns_details_wID):
    #adding bid strategies to list of campaigns
    cms_ids = get_campaigns_details_wID['CampaignId'].tolist()
    bid_id  = get_tCPA_wID['Id'][0]
    response = adw.attach_fbs_to_campaigns(get_client_non_mcc, bid_id, cms_ids)

    #evaluating the response
    value = response['value']
    assert((value['id'] == cms) and (value['biddingStrategyConfiguration']['biddingStrategyId']==bid_id) for resp,cms in zip(value, cms_ids))

@pytest.mark.done
def test_create_text_label(get_client_non_mcc, get_text_label_wID, get_text_label_wNID):
    #creating text label
    from datetime import datetime
    label_name = 'test_2' + datetime.now().strftime('%Y-%m-%d %H:%M:%S')
    response = adw.create_text_label(get_client_non_mcc, label_name, get_text_label_wNID['label_color'], get_text_label_wNID['label_desc'])
    print(response)
    #checking if the text label was created or not 
    response = adw.get_label_ids_by_name(get_client_non_mcc, [label_name])
    assert(label_name in response.keys())

@pytest.mark.done
def test_attach_label(get_client_non_mcc,get_text_label_wID,get_campaigns_details_wID, get_text_label_wNID):
    #creating a new label
    from datetime import datetime
    label_name = 'test_2' + datetime.now().strftime('%Y-%m-%d %H:%M:%S')
    label_response = adw.create_text_label(get_client_non_mcc, label_name, get_text_label_wNID['label_color'], get_text_label_wNID['label_desc'])
    label_id = label_response['value'][0]['id']

    #attaching label to campaigns
    #getting the label id
    entity_service_name = 'CampaignService'
    entity_id_name = 'campaignId'
    entity_ids = get_campaigns_details_wID['CampaignId'].unique()
    response = adw.attach_label(get_client_non_mcc, label_id, entity_service_name, entity_id_name, entity_ids)

    #evaluting the response
    assert(len(response['value'])==len(entity_ids))
    assert([obj['campaignId'] in get_campaigns_details_wID['CampaignId'] and obj['labelId'] == label_id for obj in response['value']])

@pytest.mark.done
def test_pause_ads_by_id(get_client_non_mcc, get_ads_wID):
    ads_group_ids = [obj['adGroupId'] for obj in get_ads_wID]
    ads_ids = [obj['ad']['id'] for obj in get_ads_wID]
    #pausing the ads with id
    response = adw.pause_ads_by_id(get_client_non_mcc, ads_group_ids, ads_ids)

    #evaluating paused ad
    sel_ad = adw.create_selector(['Id', 'AdGroupId', 'Status','CampaignId', 'Labels'])     
    ad_group_ad_service = get_client_non_mcc.GetService('AdGroupAdService', version=API_VERSION)                                
    response_ads = ad_group_ad_service.get(sel_ad)['entries']
    for val in response_ads:
        if(str(val['ad']['id']) in ads_ids):
            assert(val['status']=='PAUSED')
            assert(any('Paused' in label_obj['name'] for label_obj in val['labels']))

@pytest.mark.done
def test_create_eta_ads_from_df(get_client_non_mcc, get_new_ads_df, get_lp_regex_df):
    #getting lp_lookup from create_final_urls_lookup_from_df()
    lp_lookup = adw.create_final_urls_lookup_from_df(get_client_non_mcc, get_lp_regex_df, 'www.example.com')
    #calling create_eta_ads_from_df(); label, labe_id, and label_name are set to default 
    response = adw.create_eta_ads_from_df(get_client_non_mcc, get_new_ads_df, lp_lookup) 
    print(response)
    new_ads_ids = [obj['ad']['id'] for obj in response[0]['value']]

    #evaluating newly created ads
    sel_ad = adw.create_selector(['Id', 'AdGroupId', 'Status','CampaignId', 'Labels'])     
    ad_group_ad_service = get_client_non_mcc.GetService('AdGroupAdService', version=API_VERSION)                                
    response_ads = ad_group_ad_service.get(sel_ad)['entries']
    for val in response_ads:
        if(str(val['ad']['id']) in new_ads_ids):
            assert(val['status']=='ENABLED')
            assert(any('Created' in label_obj['name'] for label_obj in val['labels']))

@pytest.mark.done
def test_create_final_urls_lookup_from_df(get_client_non_mcc, get_lp_regex_df, get_campaigns_details_wID):
    response_dict = adw.create_final_urls_lookup_from_df(get_client_non_mcc, get_lp_regex_df, 'http://start1.com')

    #evaluating the response; default_url has not been tested yet
    for index, rows in get_lp_regex_df.iterrows():
        filtered_df = get_campaigns_details_wID[get_campaigns_details_wID['CampaignName'].str.contains(rows.CampaignRegex) & get_campaigns_details_wID['AdGroupName'].str.contains(rows.AdGroupRegex)]
        for index, filtered_rows in filtered_df.iterrows():
            row_dict = (str(filtered_rows.CampaignId), str(filtered_rows.AdGroupId))
            assert(response_dict[row_dict]==rows.FinalURL)
    
@pytest.mark.done
def test_batch_update_kws_final_url_from_df(get_client_non_mcc, get_lp_regex_df):
    #calling create_final_urls_lookup_from_df for lp_lookup
    lp_lookup = adw.create_final_urls_lookup_from_df(get_client_non_mcc, get_lp_regex_df, 'http://start1.com')
    #calling batch_update_kws_final_url_from_df function; args label and label_id are set to default
    bjh, response_batch_op = adw.batch_update_kws_final_url_from_df(get_client_non_mcc, lp_lookup)

    #evaluating the reponse
    import xml.etree.ElementTree as ET 
    root = ET.ElementTree(ET.fromstring(response_batch_op))
    for elem in root.iter():
        if('urls' in elem.tag):
            assert(elem.text in get_lp_regex_df['FinalURL'].tolist() or elem.text == 'http://start1.com')

    """
    Left:
        1. test with arg label=False
        2. test with providing already existing label id in arg label_id
    """

@pytest.mark.done
def test_update_kws_final_url_from_df(get_client_non_mcc, get_lp_regex_df):
    #calling create_final_urls_lookup_from_df for lp_lookup
    lp_lookup = adw.create_final_urls_lookup_from_df(get_client_non_mcc, get_lp_regex_df, 'http://start1.com')
    #calling batch_update_kws_final_url_from_df function; args label and label_id are set to default; passing label_name to avoid duplicted name error as similar label names were created in test_batch_update_kws_final_url_from_df
    from datetime import datetime
    label_name =  datetime.now().strftime('%Y-%m-%d %H:%M:%S') + '- KW FinalURL Updated'
    response_batch_op = adw.update_kws_final_url_from_df(get_client_non_mcc, lp_lookup, label_name=label_name)

    #evaluating the reponse
    assert([obj['finalUrls']['urls'] in get_lp_regex_df['FinalURL'].tolist() or obj['finalUrls']['urls'] == 'http://start1.com' for obj in response_batch_op[0]['value']])
    """
    Left:
        1. test with arg label=False
        2. test with providing already existing label id in arg label_id
    """
    

# @pytest.mark.left
# def test_get_report_as_df_threaded(get_client_non_mcc):

@pytest.mark.done
def test_get_live_ad_count():
    #getting client account of PH Google:S:PH:xx:CC - 5135206347
    client = adw.get_adwords_client()
    client.SetClientCustomerId(5135206347)

    #calling get_live_ad_count; arg asTypesList is default to Nono (i.e. will only look for ETA)
    response_live_ad = adw.get_live_ad_count(client)
    #evaluating the response
    assert(all(response_live_ad.columns == ['CampaignStatus', 'AdGroupStatus', 'NumLiveAds', 'NumEvergreenAds', 'NumNonEvergreenAds', '_merge']))
    assert(not(response_live_ad.empty))
    
    

    
#Testing all get functions
@pytest.mark.done
def test_get_all_but_removed_campaigns_ad_adgroups(get_client_non_mcc, get_campaigns_details_wNID):
    response_df = adw.get_all_but_removed_campaigns_ad_adgroups(get_client_non_mcc)
    #cols list that response_df is expected to have
    cols_list=['AdGroupId', 'AdGroupName', 'CampaignId', 'CampaignName']
    assert(cols_list == response_df.columns.tolist())
    #checking if all campaigns added during setup are present in retrieved (Matching is not based on id, it is based on rest of the columns)
    assert(get_campaigns_details_wNID['CampaignName'].isin(response_df['CampaignName']).all())
    assert(get_campaigns_details_wNID['AdGroupName'].isin(response_df['AdGroupName']).all())

@pytest.mark.done
def test_get_active_exact_kws(get_client_non_mcc):
    response_list = adw.get_active_exact_kws(get_client_non_mcc)
    assert(type(response_list)==type([]))

@pytest.mark.done
def test_get_all_shared_sets(get_client, get_shared_set_wNID):
    response_df = adw.get_all_shared_sets(get_client)
    #cols list that response_df is expected to have
    cols_list=['SharedSetId', 'Name', 'Type', 'MemberCount', 'ReferenceCount', 'Status']
    if((response_df!=None)):
        shared_set_names = response_df['Name'].tolist()
        index_shared_set = shared_set_names.index(int(get_shared_set_wNID))
        assert (index_shared_set != -1)
        if(index_shared_set != -1):
            assert(shared_sets['Type'][index_shared_set]=='NEGATIVE_KEYWORDS')
    else:
        assert(True)

@pytest.mark.done
def test_get_label_ids_by_name(get_client_non_mcc, get_text_label_wNID):
    response_dict = adw.get_label_ids_by_name(get_client_non_mcc, [get_text_label_wNID['label_name']])
    assert(get_text_label_wNID['label_name'] in response_dict.keys())

@pytest.mark.done
def test_get_report_as_df (get_client_non_mcc):
    selector_columns = ['AccountCurrencyCode', 'AccountDescriptiveName', 'ExternalCustomerId']
    selectors = adw.create_selector(selector_columns)
    report_type = 'ACCOUNT_PERFORMANCE_REPORT'
    returnedDf = adw.get_report_as_df(get_client_non_mcc, selectors, report_type)
    assert([i for i,j in zip(selector_columns, returnedDf.columns) if i == j])

@pytest.mark.done
def test_clean_adwords_df_from_ui(get_raw_api_df):
    #cleaning get_raw_api_df
    clean_df = adw.clean_adwords_df_from_ui(get_raw_api_df)

    #check if any of the undesired values are present in clean_df
    for col in clean_df.columns:
        assert(not(clean_df[col].isin([' --', '< 10%', '> 90%']).any()))

@pytest.mark.done
def test_get_eta_performance(get_client_non_mcc, get_ads_wID):
    #getting ets performace for each ad
    response_eta = adw.get_eta_performance(get_client_non_mcc)
    response_eta['Id'] =  response_eta['Id'].str.split('|') 
    all_uniq_ids = [int(_id) for val in response_eta['Id'] for _id in val]          
    all_uniq_ids.sort()
    print(all_uniq_ids)

    #evaluating the eta peformance reponse df
    #getting active ads
    sel_ad = adw.create_selector(['Id', 'AdGroupId', 'Status','CampaignId', 'AdType'], 
                                        pred_fields=['Status', 'AdNetworkType2', 'AdType'],
                                        pred_operators=['EQUALS', 'EQUALS', 'EQUALS'],
                                        pred_values=['ENABLED', 'SEARCH', 'EXPANDED_TEXT_AD'])     
    ad_group_ad_service = get_client_non_mcc.GetService('AdGroupAdService', version=API_VERSION)                                
    response_ads_ids = [obj['ad']['id'] for obj in ad_group_ad_service.get(sel_ad)['entries']]
    #sorting them asc
    response_ads_ids.sort()

    assert(all_uniq_ids == response_ads_ids)








