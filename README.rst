blackbox
========


Python for CAG analytics and automation

Usage
-----

Installation
------------

Requirements
^^^^^^^^^^^^

Compatibility
-------------

Licence
-------

Authors
-------

`blackbox` was written by `Yang Gao <yang@compareasiagroup.com>`_.
