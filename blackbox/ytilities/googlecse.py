#!/usr/bin/env python
"""
Functions to work with Google CSE API.
"""
import pickle
from datetime import datetime as dt
import time

import pandas as pd
import json

from googleapiclient.discovery import build
from googleapiclient.errors import HttpError
from google.oauth2 import service_account


def ping_cse_and_store(queries,
                       api_key,
                       cse_name,
                       cse_params_dict,
                       path_to_service_key,
                       bq_project_id_str,
                       bq_table,
                       output_dir=None,
                       file_root=None,
                       logger=None,
                       skip_bq=False):
    resps = {}
    svc_cse = build('customsearch', 'v1', developerKey=api_key)

    #     cse_params_dict['cx'] = cse_id
    for q in queries:
        cse_params_dict['q'] = q
        key = (q, cse_name)

        if logger:
            logger.info(f'Running {q} on {cse_name}')

        retries = 0
        while retries < 3:
            # noinspection PyBroadException
            try:
                resps[key] = svc_cse.cse().siterestrict().list(
                    **cse_params_dict).execute()
                break
            # error thrown is googleclient HttpError and NOT urllib HTTPError
            # https://github.com/googleapis/google-api-python-client/blob/master/googleapiclient/errors.py
            except HttpError as e:
                # only backoff if its a rate limit issue, otherwise retry immediately
                if 'User Rate Limit Exceeded' in e._get_reason():
                    sleep_secs = 1 + (0.5 * (2 ** retries))
                    s = f'User Rate Limit Exceeded. {retries} retry with {q} after sleeping for {sleep_secs} seconds.'
                    if logger:
                        logger.warning(s)
                    else:
                        print(s)
                else:
                    if logger:
                        logger.exception(
                            f'Not User Rate Limit Exceeded error. Retrying number {retries} with {q} IMMEDIATELY.'
                        )
                    else:
                        print(f'Not User Rate Limit Exceeded error. Retrying number {retries} with {q} IMMEDIATELY.')
            except Exception as e:
                if logger:
                    logger.exception(
                        f'{e} - Non HTTPError. Retrying number {retries} with {q} IMMEDIATELY.'
                    )
                else:
                    print(f'{e} - Non HTTPError. Retrying number {retries} with {q} IMMEDIATELY.')
            finally:
                retries += 1
        else:
            # after max retries ...
            if logger:
                logger.error(f'Max retries reached with {q}, no results returned.')
            else:
                print(f'Max retries reached with {q}, no results returned.')
        # sleep 0.5 FULL second in bewteen queries, NOT requests since there could be 3 retries
        time.sleep(0.5)

    # OPTIONALLY: pickle to file after done with all queries
    if output_dir and file_root:
        with open(
                f'{output_dir}/Chunk_{file_root}_{int(dt.timestamp(dt.now()))}.pkl',
                'wb') as f:
            pickle.dump(resps, f)

    # Stream insert into BigQuery
    if skip_bq:
        return resps

    df = create_df_from_resps(resps, cse_params_dict)
    #     df = df.drop('pagemap', axis=1)  # already excluded in create_df_from_resps()
    #     print(df.info())

    bq_credentials = service_account.Credentials.from_service_account_file(path_to_service_key)

    df.to_gbq(
        project_id=bq_project_id_str,
        destination_table=bq_table,
        if_exists='append', credentials=bq_credentials)

    return resps


COLS = [
    'cacheId', 'displayLink', 'formattedUrl', 'htmlFormattedUrl',
    'htmlSnippet', 'htmlTitle', 'kind', 'link', 'snippet', 'title',
    'originalQuery', 'cseName', 'cseID', 'countryRestrict', 'userGeolocation',
    'languageRestrict', 'dateRestrict', 'ogTitle', 'ogDescription',
    'twitterTitle', 'twitterDescription', 'dateYmd', 'resultOrder',
    'correctedQuery', 'pagemapJSONDumps'
]


def create_df_from_resps(resps, input_dict, cols=None, logger=None):
    if not cols:
        cols = COLS

    if logger:
        logger.info(f'Size of resps is {len(resps)}')
    else:
        print(f'Size of resps is {len(resps)}')
    current_date = dt.today().strftime('%Y%m%d')

    dfs = []

    # now including additional items from the input dict
    for k, v in resps.items():
        if logger:
            logger.info(f'{k}')
        else:
            print(f'{k}')

        # create the base df from items, or "No result returned" if no items
        # note v from resps.items() returns the ENTIRE response for a given query
        # whereas v['items'] is a specific result within that response
        _itemsdf = pd.read_json(json.dumps(
            v['items'])) if 'items' in v else pd.DataFrame(
            'No result returned',
            columns=[
                'cacheId', 'displayLink', 'formattedUrl', 'htmlFormattedUrl',
                'htmlSnippet', 'htmlTitle', 'kind', 'link', 'pagemap',
                'snippet', 'title'
            ],
            index=[0])

        # parse query and cse from the key
        # staying with SNAKE case
        _itemsdf['originalQuery'] = k[0]
        _itemsdf['cseName'] = k[1]
        _itemsdf['cseID'] = v['queries']['request'][0]['cx']

        # from input_dict
        _itemsdf['countryRestrict'] = input_dict['cr']
        _itemsdf['userGeolocation'] = input_dict['gl']
        _itemsdf['languageRestrict'] = input_dict['lr']
        _itemsdf['dateRestrict'] = input_dict['dateRestrict'] if 'dateRestrict' in input_dict else ''

        # from pagemap, if any items
        try:
            _itemsdf['ogTitle'] = _itemsdf.pagemap.apply(
                lambda x: x['metatags'][0]['og:title'] if type(x) is dict and 'og:title' in x['metatags'][0] else x)
            _itemsdf['ogDescription'] = _itemsdf.pagemap.apply(
                lambda x: x['metatags'][0]['og:description'] if type(x) is dict and 'og:description' in x['metatags'][
                    0] else x)
            _itemsdf['twitterTitle'] = _itemsdf.pagemap.apply(
                lambda x: x['metatags'][0]['twitter:title'] if type(x) is dict and 'twitter:title' in x['metatags'][
                    0] else x)
            _itemsdf['twitterDescription'] = _itemsdf.pagemap.apply(
                lambda x: x['metatags'][0]['twitter:description']
                if type(x) is dict and 'twitter:description' in x['metatags'][0] else x)
        except KeyError:
            print('KeyError triggered for missing pagemap items ...')
            _itemsdf['ogTitle'] = 'No result returned'
            _itemsdf['ogDescription'] = 'No result returned'
            _itemsdf['twitterTitle'] = 'No result returned'
            _itemsdf['twitterDescription'] = 'No result returned'

        # other meta data
        _itemsdf['dateYmd'] = current_date
        _itemsdf['resultOrder'] = _itemsdf.index + 1
        _itemsdf['correctedQuery'] = v['spelling'][
            'correctedQuery'] if 'spelling' in v.keys() else ''

        # puttng JSONDumps last
        _itemsdf['pagemapJSONDumps'] = _itemsdf.pagemap.apply(lambda x: json.dumps(x))

        # conform to BQ schema, also dropping pagemap
        missing_cols = set(cols) - set(_itemsdf.columns)
        for c in missing_cols:
            print(f'{c} column is missing from {k}')
            _itemsdf[c] = ''
        _itemsdf = _itemsdf[cols]

        dfs.append(_itemsdf)

    return pd.concat(dfs, ignore_index=True, sort=False)  # sort=False leaves the column order as-is
