import os
import random
import re
import time
from urllib import request
import pickle
from datetime import datetime as dt

# noinspection PyPackageRequirements
import numpy as np
# noinspection PyPackageRequirements
import pandas as pd
# noinspection PyPackageRequirements
from googleads import adwords
from googleads.errors import GoogleAdsError
from googleapiclient.discovery import build
from googleapiclient.errors import HttpError
from google.oauth2 import service_account

API_VERSION = "v201809"  # would also need to upgrade lib > pip install --upgrade googleads
PAGE_SIZE = 500
USER_HOME_DIR = os.path.expanduser('~')
PRODUCTION_YAML_PATH = os.path.join(USER_HOME_DIR, '.credentials/googleads_production.yaml')
PRODUCTION_TOP_MCC_ID = 4699702057  # CAG MCC CID 2972130200
TEST_YAML_PATH = os.path.join(USER_HOME_DIR, '.credentials/googleads_test.yaml')
TEST_MCC_CID = 3956668664  # <-ygao_test_mcc_20190825; OLD yg_adwords_test_mcc 5016858558; Both using outlook
MIL = 1000000  # used to convert dollars to micros


# todo: create client as module-level constant which will be instantiated at import, default to TEST
# todo: create function to allow user to explicitly set client to production

def create_selector(cols_list,
                    pred_fields=None, pred_operators=None, pred_values=None,
                    date_from=None, date_to=None,
                    order_fields=None, order_hows=None):
    """
    Convenience function that creates a selector for the AdWords API. Can be used for both services
    or reports. All pred_* lists should be in the same order. Paging specified by service, NOT here
    in selector. Also need to add 'dateRangeType': 'CUSTOM_DATE' to report definition IF using date_from and date_to.
    Note: specify segments by simply including the segments in the columns of the report.

    Args:
        cols_list: list of fields. Only required param.
        pred_fields: list of fields to be filtered against. Defaults to None but if not,
            must correspond with all pred_* lists.
        pred_operators: list of operators to be applied. Defaults to None but if not,
            must correspond with all pred_* lists.
        pred_values: list of values to filter for. Defaults to None but if not, must correspond with all pred_* lists.
        date_from: min of date range, date formatted as YYYYMMDD string
            (must add to report 'dateRangeType': 'CUSTOM_DATE' definition)
        date_to: max of date range, INCLUSIVE. date formatted as YYYYMMDD string
            (must add to report 'dateRangeType': 'CUSTOM_DATE' definition)
        order_fields: list of fields to sort the results on.
        order_hows: corresponding list of ASCENDING or DESCENDING to sort by. Defaults to ASC.
    Returns:
        A dictionary-based selector object.
        For example:
        {'fields': ['AccountLabels',
          'CanManageClients',
          'CompanyName',
          'CurrencyCode',
          'CustomerId',
          'DateTimeZone',
          'Name',
          'TestAccount'],
         'predicates': [{'field': 'AccountLabels',
           'operator': 'CONTAINS_ANY',
           'values': [4038216L, 4038261L]}]}

    Raises:
        Exception: Mis-match in number of predicate fields, operators, and values.
    """
    selector = {}

    # create fields
    selector['fields'] = cols_list

    # create predicates
    if pred_fields:
        if len(pred_fields) == len(pred_operators) == len(pred_values):
            pred_list = []
            for pred_tuple in zip(pred_fields, pred_operators, pred_values):
                pred_list.append(dict(zip(('field', 'operator', 'values'), pred_tuple)))
            selector['predicates'] = pred_list
        else:
            raise Exception('Mis-match in number of predicate fields, operators, and values.')

    # create date range
    if date_from and date_to:
        selector['dateRange'] = {
            'min': date_from,
            'max': date_to
        }

    # create ordering, default sortOrder to ASCENDING
    if order_fields:
        import itertools
        order_hows = [] if not order_hows else order_hows
        sortby = list(itertools.zip_longest(order_fields, order_hows, fillvalue='ASCENDING'))
        selector['ordering'] = [{'field': f, 'sortOrder': s} for f, s in sortby]

    return selector


def get_report_as_df(client, selector, report_type, report_name='TPS_REPORT', date_range_type='YESTERDAY',
                     dl_format='TSV', skip_report_header=True, skip_column_header=True,
                     skip_report_summary=True, include_zero_impressions=True, safe_mode=False):
    """
    Runs a SINGLE report against the given account and returns the data as a pandas dataframe.
    More @ https://developers.google.com/adwords/api/docs/appendix/reports and
    https://developers.google.com/adwords/api/docs/guides/reporting
    Note: to run the same report across multiple accounts, use get_report_as_df_threaded() instead.

    Args:
        client: adwords client for the given account
        selector: selector {} that defines most of the report. Use create_selector() to generate.
        report_type: base report types e.g. ACCOUNT_PERFORMANCE_REPORT
        report_name: name of the report, not really used. This is NOT the report base type.
        date_range_type: https://developers.google.com/adwords/api/docs/guides/reporting#date_ranges
            Need to be 'CUSTOM_DATE' if selector specifies dateRange
        dl_format: download format, defaults to TSV
        skip_report_header: defaults to True
        skip_column_header: defaults to True, will get column names from selector['fields']
        skip_report_summary: skip totals at the bottom of the report, defaults to True
        include_zero_impressions: defaults to True
        safe_mode: allow data with possibly additional columns to be parsed from the response. I.e. would
            NOT try to apply the predefined column names to the return df in case there are non-matching
            columns. Defaults to False.

    Returns:
        Data as a pandas dataframe
    """
    report_downloader = client.GetReportDownloader(API_VERSION)

    if 'dateRange' in selector:
        date_range_type = 'CUSTOM_DATE'

    report = {'reportName': report_name,
              'dateRangeType': date_range_type,
              'reportType': report_type,
              'downloadFormat': dl_format,
              'selector': selector}

    # Changing to using DownloadReportAsString due to https://github.com/googleads/googleads-python-lib/issues/281

    stringOutput = report_downloader.DownloadReportAsString(report,
                                                            skip_report_header=skip_report_header,
                                                            skip_column_header=skip_column_header,
                                                            skip_report_summary=skip_report_summary,
                                                            include_zero_impressions=include_zero_impressions
                                                            )

    if stringOutput == '':
        return pd.DataFrame(columns=selector['fields'])

    if safe_mode:
        return pd.DataFrame([[y for y in x.split('\t')] for x in stringOutput.strip().split('\n')])

    return pd.DataFrame([[y for y in x.split('\t')] for x in stringOutput.strip().split('\n')],
                        columns=selector['fields'])


def get_report_as_df_threaded(cred_yaml, account_ids, report_def,
                              skip_report_header=True,
                              skip_column_header=True,
                              skip_report_summary=True,
                              include_zero_impressions=False,
                              col_names=None, api_version=API_VERSION,
                              yaml_from_storage=True
                              ):
    """
    Pulls a given adwords built-in report across a list of accounts and
    returns the data in a pandas dataframe.

    Args:
        cred_yaml: either path to the googleads credential file for this project OR yaml_doc as string
        account_ids: id of the account you want to run the report for.
        report_def: definition of report including reportType, selector, etc.
        skip_report_header: don't include report header by default
        skip_column_header: don't include column header by default, will pull from report def
        skip_report_summary: don't include summary by default
        include_zero_impressions: exclude zero impression entities by default
        col_names: names of the columns in the returned DataFrame. Defaults to None, in which case
            column names will be inferred from report_definition['selector']['fields']
        api_version: defaults to API_VERSION
        yaml_from_storage: specify if yaml is from a file or a string. Defaults to True, which is file

    Returns:
        Pandas dataframe contain the data from running the report against the given accounts

    """
    from concurrent.futures import ThreadPoolExecutor, as_completed

    def worker(cid):
        if yaml_from_storage:
            client = adwords.AdWordsClient.LoadFromStorage(cred_yaml)
        else:
            client = adwords.AdWordsClient.LoadFromString(cred_yaml)

        client.SetClientCustomerId(cid)

        rpt_downloader = client.GetReportDownloader(api_version)

        # Changing to using DownloadReportAsString due to https://github.com/googleads/googleads-python-lib/issues/281
        stringOutput = rpt_downloader.DownloadReportAsString(report_def,
                                                             skip_report_header=skip_report_header,
                                                             skip_column_header=skip_column_header,
                                                             skip_report_summary=skip_report_summary,
                                                             include_zero_impressions=include_zero_impressions)

        if stringOutput == '':
            df = pd.DataFrame(columns=col_names)
        else:
            df = pd.DataFrame([[y for y in x.split('\t')] for x in stringOutput.strip().split('\n')], columns=col_names)

        return df

    dfs = []

    if not col_names:
        col_names = report_def['selector']['fields']

    with ThreadPoolExecutor(max_workers=16) as ex:
        futures = {ex.submit(worker, cid): cid for cid in account_ids}
        for future in as_completed(futures):
            acct = futures[future]
            try:
                data = future.result()
            except Exception as exc:
                print('%d generated an exception: %s' % (acct, exc))
            else:
                print('Account %d has %d rows' % (acct, len(data)))
                dfs.append(data)

    return pd.concat(dfs).reset_index(drop=True).sort_index()


def clean_adwords_df_from_ui(df, drop_cols=None, percent_cols=None, obj2num_cols=None, rename=True):
    """Cleans up dataframe from adwords report, downloaded from the web UI.

    Default data from adwords reports are not pandas friendly. They include headers,
    totals, weird values for %'s etc. This function will clean up the report is it becomes
    Pandas-friendly.

    Note: function will replace the following values in df. {' --':'0%','< 10%':'5%','> 90%':'95%'}

    Args:
        df: raw dataframe resulting from initial read_csv call on adwords report.
        drop_cols: list of columns names to drop, if any.
        percent_cols: list of percent columns to convert to floats.
        obj2num_cols: list of perceived object columns, i.e. containing str, that really should be floats.
        rename: boolean to rename df columns to lower-case and replace spaces with underscore
                so columns can be accessed as attributes. Default to True.

    Returns:
        Clean, Pandas-friendly dataframe.

    Raises:
        None

    """
    if drop_cols is None:
        drop_cols = []

    if percent_cols is None:
        percent_cols = []

    if obj2num_cols is None:
        obj2num_cols = []

    # drop all rows after the totals line, sometimes there could be errors after that row from google report
    # 'Total', if present, will be in the first column of the dataframe
    df = df[df[df.columns[0]] != 'Total']

    # drop any unneeded columns
    df = df.drop(drop_cols, axis=1)

    # Max. CPC columns can have funky values like 'auto: 2.23'
    # Need to cast to str first as object series can contain mixed types so sometimes you'll have floats with strs
    try:
        df['Max. CPC'] = df['Max. CPC'].astype('str').str.strip('auto: ')
    except KeyError:  # if df doesn't have Max. CPC column then just pass
        pass

    # clean up any weird entries in numeric cols
    # making some assumptions here that could impact the regression
    # intentionally replacing -- with 0 and not 0% because it can also show up in non-percentage columns
    df = df.replace({' --': '0', '< 10%': '5%', '> 90%': '95%'})

    # convert %'s to floats
    for c in percent_cols:
        df[c] = df[c].apply(lambda x: float(x.strip('%')) / 100)

    # convert obj cols to numerics
    df[obj2num_cols] = df[obj2num_cols].replace(',', '', regex=True)
    df[obj2num_cols] = df[obj2num_cols].replace('--', 0, regex=True)
    for c in obj2num_cols:
        df[c] = df[c].astype('float')

    if rename:
        df.columns = ["_".join(re.split(r'\W+', x)).strip("_").lower() for x in df.columns]

    return df


def clean_adwords_df_from_api(df, drop_cols=None, percent_cols=None,
                              obj2num_cols=None, money_cols=None, date_cols=None,
                              url_cols=None, rename=False):
    """Cleans up dataframe from adwords report, downloaded from api.

    Reports from API is better than from UI as we can exclude totals and summaries, etc. However columns names
    are different from their display values in the UI.
    See https://developers.google.com/adwords/api/docs/guides/uireports

    Note: function will replace the following values in df. {' --':'0%','< 10%':'5%','> 90%':'95%'}

    Args:
        df: raw dataframe resulting from initial read_csv call on adwords report.
        drop_cols: list of columns names to drop, if any.
        percent_cols: list of percent columns to convert to floats.
        obj2num_cols: list of perceived object columns, i.e. containing str, that really should be floats. Using
                pd.to_numeric( ) with errors='coerce' so will insert NaN for any non-numbers
        money_cols: list of cols of the type money, which is in micros. Will divide by 1M to convert to dollars.
        date_cols: list of columns to be converted to datetime64
        url_cols: list of columns that contain urls. Should be JSON list format but will just pick the first url for now
        rename: boolean to rename df columns to lower-case and replace spaces with underscore
                so columns can be accessed as attributes. Default to False for api as cols are already CamelCased.

    Returns:
        Clean, Pandas-friendly dataframe.

    Raises:
        None

    """

    if drop_cols is None:
        drop_cols = []

    if percent_cols is None:
        percent_cols = []

    if obj2num_cols is None:
        obj2num_cols = []

    if money_cols is None:
        money_cols = []

    if date_cols is None:
        date_cols = []

    if url_cols is None:
        url_cols = []

    # drop any unneeded columns
    df = df.drop(drop_cols, axis=1)

    # convert %'s to floats
    for c in percent_cols:
        # clean up any weird entries in numeric cols
        # making some assumptions here that could impact the regression
        df[c] = df[c].replace({' --': '0%', '< 10%': '5%', '> 90%': '95%'}).apply(lambda x: float(x.strip('%')) / 100)

    # convert obj cols to numerics, some columns might have ""s which need to be striped
    for c in set(obj2num_cols + money_cols):
        df[c] = pd.to_numeric(df[c].str.replace(r'"?(-?\d+)"?', r'\1'), errors='coerce')

    # divide all money cols by MIL to convert micros to dollars
    df[money_cols] = df[money_cols] / MIL

    # convert dates
    for c in date_cols:
        df[c] = df[c].astype('datetime64[D]')

    # parse urls
    # Todo: handle cases where there could be multiple URLs passed back in the JSON list

    def extract_url(s):
        from urllib import parse
        return parse.unquote(s).replace('"[""', '').replace('""]"', '')

    for c in url_cols:
        df[c] = df[c].apply(extract_url)

    # rename cols if needed
    if rename:
        df.columns = ["_".join(re.split(r'\W+', x)).strip("_").lower() for x in df.columns]

    return df


def read_adwords_csv(dir_path, filename, sep='\t', skiprows=5, encoding='utf-16le', **kwargs):
    """Loads csv report from adwords UI into a pandas dataframe.

    Convenience function that pre-populates select parameters for pandas.read_csv().
    Additional kwargs will be simply passed through. Will import os module if not done so already.

    Args:
        dir_path: directory path. E.g. 'Data/sub_dir'
        filename: name of the csv file, downloaded directly from adwords UI
        sep: separator, defaults to tab
        skiprows: initial lines to skip, defaults to 5
        encoding: file encoding, defaults to 'utf-16le'
        **kwargs: any additional keyword args that are passed to pandas.read_csv()

    Returns:
        Pandas dataframe, though not necessarily "clean". Consider running clean_adwords_df() after.

    Raises:
        None

    """
    import os

    return pd.read_csv(os.path.join(dir_path, filename), sep=sep, skiprows=skiprows, encoding=encoding, **kwargs)


def text2cols(df, col, delimiter=':', rename=None, drop_orig=False):
    """Text-to-column on a given column within the dataframe.

    Mimics text-to-column functionality from excel. Good for parsing info out of structured account, campaign,
    ad group names. IFF rename is provide, will only retain len(rename) number of columns, even if text-to-column
    produces more.

    Args:
        df: DataFrame
        col: string name of target column to perform text-to-column on
        delimiter: delimiter string, defaults to colon per Hcom SEM naming convention
        rename: dict of new column names for the resulting columns of the format {0:'col1', 1:'col2', 2:'col3'},
            defaults to None
        drop_orig: boolean for whether to drop original target column from df. Default is False, i.e. keep col.

    Returns:
        Dataframe with additional columns.


    """
    '''takes in df, splits col by delimiter, appends to df and renames accordingly'''
    t2c = df[col].str.split(delimiter, expand=True)

    if rename:
        # this might be a bug because ix will assume .loc if index has integers as labels?
        # t2c = t2c.ix[:,:len(rename)]

        t2c = t2c.iloc[:, :len(rename)]

        t2c = t2c.rename(columns=rename)

    df = df.join(t2c)

    if drop_orig:
        df.drop(col, axis=1, inplace=True)

    return df


def get_list_of_accounts(adwords_client, mcc_id,
                         can_manage_clients=False,
                         exclude_hidden_accounts=True,
                         page_size=PAGE_SIZE):
    """
    Get list of children accounts under a given MCC

    Args:
        adwords_client: adwords client
        mcc_id: CID of the MCC
        can_manage_clients: include sub-mccs in returned accounts list. Defaults to False,
            i.e. only return "regular" accounts
        exclude_hidden_accounts: NOT include hidden accounts. Defaults to True
        page_size: paging size. Defaults to module const PAGE_SIZE

    Returns: list of CIDs

    """
    # Set mcc_id
    adwords_client.SetClientCustomerId(mcc_id)

    # Initialize appropriate service.
    managed_customer_service = adwords_client.GetService(
        'ManagedCustomerService', version=API_VERSION)

    # Construct selector to get all accounts.
    offset = 0
    selector = create_selector(['CustomerId'], ['CanManageClients', 'ExcludeHiddenAccounts'],
                               ['EQUALS', 'EQUALS'], [can_manage_clients, exclude_hidden_accounts])
    # Add paging
    selector['paging'] = {
        'startIndex': str(offset),
        'numberResults': str(page_size)
    }

    more_pages = True
    cid_list = []

    while more_pages:
        # Get serviced account graph.
        page = managed_customer_service.get(selector)

        # print(page)

        if 'entries' in page and page['entries']:
            for account in page['entries']:
                cid_list.append(account['customerId'])
        offset += PAGE_SIZE
        selector['paging']['startIndex'] = str(offset)
        more_pages = offset < int(page['totalNumEntries'])

    return cid_list


def get_account_label_ids_by_name(label_text, client, api_version=API_VERSION):
    """Returns a list of label IDs from the top level mcc that contains the partial text in the label name.

    Note: this uses the AccountLabelService which is different from LabelService. I.e. this is only meant to be
    called at the top level mcc. Check Google AdWords API documentation for details.

    Args:
        label_text: partial string to be matched against label name. Case INSENSITIVE.
        client: client object, representing a top level mcc.
        api_version: String indicating the version of the AdWords API, defaults to API_VERSION

    Returns:
        List of label IDs that match label text in the given top level mcc or empty list if none found.

    """
    label_svc = client.GetService('AccountLabelService', api_version)
    selector = {'fields': ['labelId', 'labelName']}

    # Have to pull all labels out and then filter because labelName doesn't allow predicates
    label_list = label_svc.get(selector)

    id_list = []
    label_text = label_text.lower()

    if 'labels' in label_list:
        id_list = [x.id for x in label_list.labels if x.name.lower().find(label_text) != -1]

    return id_list


def get_accounts_info_by_label_ids(label_ids, account_columns, is_prod_mcc=False, api_version=API_VERSION):
    """Returns pandas dataframe with info for accounts that have been labelled with the given labels
    Note: account labels ONLY work if client_id is the top-level MCC, due to adwords permission restrictions

    Args:
        label_ids: list of label IDs. Can use get_account_label_ids_by_name() to get id list
        account_columns: list of account fields
        is_prod_mcc: flag for production or test MCC. Defaults to False, i.e. uses test MCC
        api_version: version of the api. Defaults to API_VERSION

    Returns:
        Pandas dataframe containing info for accounts that have been labelled with the given labels.

    """
    top_mcc = get_adwords_client(is_prod_mcc)

    sel = create_selector(account_columns, pred_fields=['AccountLabels'], pred_operators=['CONTAINS_ANY'],
                          pred_values=[label_ids])

    return get_paged_data_from_svc('ManagedCustomerService', sel, top_mcc, api_version=api_version)


def get_paged_data_from_svc(service_name, selector, adwords_client, api_version=API_VERSION, page_size=500):
    """Returns data from adwords api service. Paging in the selector argument is IGNORED.
    Some services in adwords do not support paging, e.g. AccountLabelService. This function only works for "page-able"
    services.

    Note: API Services typically do NOT return performance data. For performance data you should use reports.

    Note: Some fields are returned with a completely different name that can not be translated by just uppercasing
    the first letter. There are also instances where additional columns are returned that were NOT in the selector.

    Note: There could also be nested fields that are returned, which dataframes CAN handle although they might appear
    as list of columns only when displayed in a notebook, you CAN access the underlying data with comprehensions.

    Args:
        service_name: string of the service name from which data will be retrieved
        selector: dictionary containing desired fields and optionally predicates. But paging is ignored.
        adwords_client: account or MCC object in AdWords
        api_version: string of the version of the adwords api.
        page_size: defaults to 500

    Returns:
        A Pandas DataFrame that contains the data from all the pages or None if there's no data returned.
        Will attempt to align df columns to selector fields but if KeyError, then just return data as-is.

    """
    data_pages = []

    svc = adwords_client.GetService(service_name, api_version)

    offset = 0

    selector['paging'] = {'numberResults': str(page_size), 'startIndex': str(offset)}

    more_pages = True

    cnt = 0
    while more_pages:

        page = svc.get(selector)

        # check to see if there's any data, if not, just return None
        if int(page['totalNumEntries']) < 1:
            return None

        if 'entries' in page:
            data_pages.append(page)
            # print "page " + str(cnt)
            cnt += 1

        offset += page_size
        selector['paging']['startIndex'] = str(offset)
        more_pages = offset < int(page['totalNumEntries'])

    # get column names from the first entry of the first page returned
    col_names = [x[0].upper() + x[1:] for x in data_pages[0].entries[0]]

    data = []
    for p in data_pages:
        for entry in p.entries:
            # entry is of type ManagedCustomer, which is NOT a dict but does support [] access
            data.append([entry[k] for k in entry])

    df = pd.DataFrame(data, columns=col_names)

    try:
        return df[selector['fields']]
    except KeyError:
        return df


def get_adwords_client(is_prod=False, full_path_to_yaml=None):
    """
    Return either the test or production client. Assumes MCC CID in yaml file.

    Args:
        is_prod: defaults to False, i.e. returns Test client
        full_path_to_yaml: if given, will just use instead of PRODUCTION_YAML_PATH or TEST_YAML_PATH.
            e.g. use-case is on GAE where you can only write to /tmp

    Returns:
         AdWords client
    """
    if full_path_to_yaml:
        return adwords.AdWordsClient.LoadFromStorage(full_path_to_yaml)

    yaml_path = PRODUCTION_YAML_PATH if is_prod else TEST_YAML_PATH
    expected_cid = PRODUCTION_TOP_MCC_ID if is_prod else TEST_MCC_CID
    client = adwords.AdWordsClient.LoadFromStorage(yaml_path)
    assert client.client_customer_id == expected_cid
    return client


def create_new_accounts(mcc_client, acct_names, currency='USD', tz='Asia/Hong_Kong'):
    """
    Creates new accounts within the given mcc

    Args:
        mcc_client: adwords mcc client
        acct_names: list of account names for the new accounts
        currency: currency of the account, defaults to USD
        tz: timezone of the account, defaults to Asia/Hong_Kong

    Returns:
         new_accounts: dict of names and cids for the newly created accounts
    """
    mcc_service = mcc_client.GetService('ManagedCustomerService', version=API_VERSION)
    ops = [{'operator': 'ADD',
            'operand': {'name': name,
                        'currencyCode': currency,
                        'dateTimeZone': tz
                        }
            } for name in acct_names]

    response = mcc_service.mutate(ops)  # todo: handle errors in repsonse

    new_accounts = {}

    for account in response['value']:
        new_accounts[account['name']] = account['customerId']

    return new_accounts


def create_shared_budgets(client, shared_budgets, delivery='STANDARD'):
    """
    Creates new shared budgets within the given non-MCC client
    Note: MIL is multiplied to the daily amt given

    Args:
        client: adwords non-MCC client
        shared_budgets: list of tuples, [('budget_name', 'daily_amt_dollars'),]
        delivery: delivery method, default to STANDARD
    Returns:
        new_budgets: dict of names and ids for the newly created budgets

    """
    budget_service = client.GetService('BudgetService', version=API_VERSION)
    ops = []
    for btuple in shared_budgets:
        ops.append({'operator': 'ADD',
                    'operand': [{'name': btuple[0],
                                 'amount': {'microAmount': btuple[1] * MIL},
                                 'deliveryMethod': delivery}]})
    response = budget_service.mutate(ops)  # todo: handle errors in response if any

    budget_lookup = dict([(b.name, b.budgetId) for b in response['value']])

    return budget_lookup


def get_shared_budgets(client, budgetStatus='ENABLED'):
    """
    Get the list of existing shared budgets for the given account

    Args:
        client: adwords non-MCC client
        budgetStatus: in ['ENABLED', 'REMOVED', 'UNKNOWN'] but defaults to ['ENABLED']

    Returns:
        Pandas dataframe with meta info on shared budgets

    """
    sel = create_selector(
        cols_list=['BudgetName', 'BudgetId', 'BudgetStatus'],
        pred_fields=['BudgetStatus'],
        pred_operators=['IN'],
        pred_values=[budgetStatus]
    )

    return get_paged_data_from_svc('BudgetService', selector=sel, adwords_client=client)


####################
# Batch job code borrowed from googleads sample code
# https://developers.google.com/adwords/api/docs/samples/python/campaign-management#add-complete-campaigns-using-batch-jobs
####################

MAX_POLL_ATTEMPTS = 5
PENDING_STATUSES = ('ACTIVE', 'AWAITING_FILE', 'CANCELING')


def add_batch_job(client):
    """Add a new BatchJob to upload operations to.

  Args:
    client: an instantiated AdWordsClient used to retrieve the BatchJob.

  Returns:
    The new BatchJob created by the request.
  """
    # Initialize appropriate service.
    batch_job_service = client.GetService('BatchJobService', version=API_VERSION)
    # Create a BatchJob.
    batch_job_operations = [{
        'operand': {},
        'operator': 'ADD'
    }]
    return batch_job_service.mutate(batch_job_operations)['value'][0]


def get_batch_job(client, batch_job_id):
    """Retrieves the BatchJob with the given id.

  Args:
    client: an instantiated AdWordsClient used to retrieve the BatchJob.
    batch_job_id: a long identifying the BatchJob to be retrieved.
  Returns:
    The BatchJob associated with the given id.
  """
    batch_job_service = client.GetService('BatchJobService', version=API_VERSION)

    selector = {
        'fields': ['Id', 'Status', 'DownloadUrl'],
        'predicates': [
            {
                'field': 'Id',
                'operator': 'EQUALS',
                'values': [batch_job_id]
            }
        ]
    }

    return batch_job_service.get(selector)['entries'][0]


def get_batch_job_download_url(client, batch_job_id,
                               max_poll_attempts=MAX_POLL_ATTEMPTS):
    """Retrieves the downloadUrl when the BatchJob is complete.

  Args:
    client: an instantiated AdWordsClient used to poll the BatchJob.
    batch_job_id: a long identifying the BatchJob to be polled.
    max_poll_attempts: an int defining the number of times the BatchJob will be
      checked to determine whether it has completed.

  Returns:
    A str containing the downloadUrl of the completed BatchJob.

  Raises:
    Exception: If the BatchJob hasn't finished after the maximum poll attempts
      have been made.
  """
    batch_job = get_batch_job(client, batch_job_id)
    poll_attempt = 0
    while (poll_attempt in range(max_poll_attempts) and
           batch_job['status'] in PENDING_STATUSES):
        sleep_interval = (30 * (2 ** poll_attempt) +
                          (random.randint(0, 10000) / 1000))
        print('Batch Job not ready, sleeping for %s seconds.' % sleep_interval)
        time.sleep(sleep_interval)
        batch_job = get_batch_job(client, batch_job_id)
        poll_attempt += 1

    if batch_job['downloadUrl']:  # it seems batch_job will always have 'downloadUrl' key but vaule might be none
        url = batch_job['downloadUrl']['url']
        print(('Batch Job with Id "%s", Status "%s", and DownloadUrl "%s" ready.'
               % (batch_job['id'], batch_job['status'], url)))
        return url

    raise Exception('Batch Job not finished downloading. Try checking later.')


def print_response(batch_job_helper, response_xml):
    """Prints the BatchJobService response.

  Args:
    batch_job_helper: a BatchJobHelper instance.
    response_xml: a string containing a response from the BatchJobService.
  """
    response = batch_job_helper.ParseResponse(response_xml)

    if 'rval' in response['mutateResponse']:
        for data in response['mutateResponse']['rval']:
            if 'errorList' in data:
                try:
                    print('Operation %s - FAILURE:' % data['index'])
                    print('\terrorType=%s' % data['errorList']['errors']['ApiError.Type'])
                    print('\ttrigger=%s' % data['errorList']['errors']['trigger'])
                    print('\terrorString=%s' % data['errorList']['errors']['errorString'])
                    print('\tfieldPath=%s' % data['errorList']['errors']['fieldPath'])
                    print('\treason=%s' % data['errorList']['errors']['reason'])
                except KeyError as ke:
                    print(ke)
            if 'result' in data:
                print('Operation %s - SUCCESS.' % data['index'])


def print_errors(batch_job_helper, response_xml):
    """Prints the only the errors from the BatchJobService response.

  Args:
    batch_job_helper: a BatchJobHelper instance.
    response_xml: a string containing a response from the BatchJobService.
  """
    response = batch_job_helper.ParseResponse(response_xml)

    if 'rval' in response['mutateResponse']:
        for data in response['mutateResponse']['rval']:
            if 'errorList' in data:
                try:
                    print('***')
                    print('Operation %s - FAILURE:' % data['index'])
                    print('\terrorType=%s' % data['errorList']['errors']['ApiError.Type'])
                    print('\ttrigger=%s' % data['errorList']['errors']['trigger'])
                    print('\terrorString=%s' % data['errorList']['errors']['errorString'])
                    print('\tfieldPath=%s' % data['errorList']['errors']['fieldPath'])
                    print('\treason=%s' % data['errorList']['errors']['reason'])
                except KeyError as ke:
                    print(ke)


############################
# end of batch job section #
############################

def create_campaign_op(batch_job_helper, budget_id, campaign_name):
    """
    Creates a single new campaign, with many assumed default settings
    todo: pythonically extend this function to take in additional settings WITHOUT creating a long list of parameters

    Args:
        batch_job_helper: from client.GetBatchJobHelper, needed for temp Id generation
        budget_id: Id of the shared budget to be used for the campaign
        campaign_name: name of the new campaign

    Returns:
        _id: temp Id assigned to the new campaign, used to associate children entities to this campaign
        op: the campaign creation operation

    """
    _id = batch_job_helper.GetId()
    op = {'xsi_type': 'CampaignOperation',
          'operand': {
              'name': campaign_name,
              'status': 'PAUSED',
              'id': _id,
              'advertisingChannelType': 'SEARCH',
              # Note that only the budgetId is required
              'budget': {
                  'budgetId': budget_id
              },
              'biddingStrategyConfiguration': {
                  'biddingStrategyType': 'MANUAL_CPC'
              },
              'networkSetting': {
                  'targetGoogleSearch': 'true',
                  'targetSearchNetwork': 'true',
                  'targetContentNetwork': 'false',
                  'targetPartnerSearchNetwork': 'false'
              },
              'settings': [
                  {
                      'xsi_type': 'GeoTargetTypeSetting',
                      'positiveGeoTargetType': 'LOCATION_OF_PRESENCE',
                      'negativeGeoTargetType': 'LOCATION_OF_PRESENCE'
                  }
              ]
          },
          'operator': 'ADD'
          }
    return _id, op


def create_adgroup_op(batch_job_helper, campaign_id, adgroup_name, default_max_cpc=0):
    """
    Creates a single new ad group under the given campaign

    Args:
        batch_job_helper: from client.GetBatchJobHelper, needed for temp Id generation
        campaign_id: Id of the parent campaign
        adgroup_name: name of the new ad group
        default_max_cpc: ad group default max cpc in account currency dollars, NOT micros. defaults to 0.

    Returns:
        _id: temp Id of the new ad group, to be used for its children
        op: the ad group creation operation

    """
    _id = batch_job_helper.GetId()
    op = {'xsi_type': 'AdGroupOperation',
          'operand': {
              'campaignId': campaign_id,
              'id': _id,
              'name': adgroup_name,
              'biddingStrategyConfiguration': {
                  'bids': [
                      {
                          'xsi_type': 'CpcBid',
                          'bid': {
                              'microAmount': int(default_max_cpc * MIL)
                          }
                      }
                  ]
              }
          },
          'operator': 'ADD'}
    return _id, op


def create_eta_op(adgroup_id, headline1, headline2, description, finalUrls,
                  path1=None, path2=None, headline3=None, description2=None,
                  status=None):
    """
    Creates a single ETA for the given ad group, including DKIs. Note
    that using headline3 or description2 requries AdWords v201809.
    todo: include some validation here, around char length limits and punctuations? Bit tricky if using DKI.

    Args:
        adgroup_id: parent ad group of the ETA
        headline1: headline part 1, required, max 30 chars
        headline2: headline part 2, required, max 30 chars
        description: description line, is required, max 90 chars
        finalUrls: list of URLs as strings. Required, optionally include
            mobile preferred. e.g. ['cag.com', 'm.cag.com']
        path1 (optional): first path following display URL, max 15 char
        path2 (optional): second path following display URL and path1,
            max15 chars
        headline3 (optional): headline part 3, optional, max 30 chars
        description2 (optional): description line 2, optional, max 90 chars
        status (optional): set to 'PAUSED' to add the ad as paused

    Returns:
        op: dict representing the eta add operation

    """
    op = {'xsi_type': 'AdGroupAdOperation',
          'operand': {
              'adGroupId': adgroup_id,
              'ad': {
                  'xsi_type': 'ExpandedTextAd',
                  'headlinePart1': headline1,
                  'headlinePart2': headline2,
                  'description': description,
                  'finalUrls': finalUrls
              }
          },
          'operator': 'ADD'}

    if headline3:
        op['operand']['ad']['headlinePart3'] = headline3

    if description2:
        op['operand']['ad']['description2'] = description2

    if path1:
        op['operand']['ad']['path1'] = path1
        if path2:  # ignore path2 if path1 wasn't provided
            op['operand']['ad']['path2'] = path2

    if status == 'PAUSED':
        op['operand']['status'] = 'PAUSED'

    return op


# todo: add labeling logic


def create_rsa_op(adgroup_id, headlines, descriptions, finalUrls, path1=None,
                  path2=None, status=None):
    """
    Creates a single Responsive Search Ad for the given ad group. A RSA
    can have 3-15 headlines and 2-4 descriptions, which Google will mix
    and match to optimize ad performance.

    Headlines can be pinned to position 1, 2, or 3 (1 and 2 will always
    show) and Descriptions can be pinned to positions 1 or 2 (1 will
    always show). Can pin multiple items to one position, in which case
    they will rotate.

    More details: https://support.google.com/google-ads/answer/7684791

    Args:
        adgroup_id: parent ad group of the RSA
        headlines: a list of headlines or (headline, pinned position)
            tuples, min 3, max 15, max 30 chars each e.g.,
                [('High Minimum Loan Amount', 'HEADLINE_1'),
                 ('Low Monthly Interest', 'HEADLINE_1),
                 'Quick Loans']
        descriptions: a list of descriptions or (description, pinned
            position) tuples, min 2, max 4, max 90 chars each e.g.,
                [('Compare Loan Packages Online At MoneyMax.Ph!', 'DESCRIPTION_2'),
                 'Reach For Your Dreams With MoneyMax.Ph And The Best Personal Loans Available!']
        finalUrls: list of URLs as strings. Required, optionally include
            mobile preferred. e.g. ['cag.com', 'm.cag.com']
        path1 (optional): first path following display URL, max 15 char
        path2 (optional): second path following display URL and path1,
            max15 chars
        status (optional): set to 'PAUSED' to add the ad as paused

    Returns:
        op: dict representing the rsa add operation

    """

    if type(finalUrls) == list:
        pass
    elif type(finalUrls) == str:
        finalUrls = [finalUrls]
    else:
        raise TypeError('unexpected final URL type')

    if type(headlines) != list:
        raise TypeError('unexpected headlines type')
    elif len(headlines) < 3 or len(headlines) > 15:
        raise ValueError('expected from 3 to 15 headlines')

    if type(descriptions) != list:
        raise TypeError('unexpected descriptions type')
    elif len(descriptions) < 2 or len(descriptions) > 4:
        raise ValueError('expected from 2 to 4 descriptions')

    headline_list = []
    for headline in headlines:
        if type(headline) == str:
            headline_list.append({
                'asset': {
                    'xsi_type': 'TextAsset',
                    'assetText': headline
                }
            })
        elif type(headline) == tuple or type(headline) == list:
            headline_list.append({
                'asset': {
                    'xsi_type': 'TextAsset',
                    'assetText': headline[0]
                },
                'pinnedField': headline[1]  # e.g., HEADLINE_1 (or 2 or 3)
            })
        else:
            raise TypeError('unexpected headline item type')

    description_list = []
    for description in descriptions:
        if type(description) == str:
            description_list.append({
                'asset': {
                    'xsi_type': 'TextAsset',
                    'assetText': description
                }
            })
        elif type(description) == tuple or type(description) == list:
            description_list.append({
                'asset': {
                    'xsi_type': 'TextAsset',
                    'assetText': description[0]
                },
                'pinnedField': description[1]  # e.g., DESCRIPTION_1 (or 2)
            })
        else:
            raise TypeError('unexpected headline item type')

    responsive_search_ad = {
        'xsi_type': 'AdGroupAd',
        'adGroupId': adgroup_id,
        'ad': {
            'xsi_type': 'ResponsiveSearchAd',
            'finalUrls': finalUrls,
            'headlines': headline_list,
            'descriptions': description_list,
        }
    }

    if path1 and path2:
        responsive_search_ad['ad']['path1'] = path1
        responsive_search_ad['ad']['path2'] = path2
    elif path1:
        responsive_search_ad['ad']['path1'] = path1
    elif path2:
        responsive_search_ad['ad']['path1'] = path2  # setting the ad's path1 to path2 because no path1 value was passed

    if status == 'PAUSED':
        responsive_search_ad['status'] = 'PAUSED'

    op = {
        'operand': responsive_search_ad,
        'operator': 'ADD'
    }

    return op


def create_keyword_op(adgroup_id, keyword, match_type='EXACT', finalUrls=None):
    """
    Creates a single keyword

    Args:
        adgroup_id: parent ad group
        keyword: keyword text
        match_type: match type, defaults to EXACT
        finalUrls: list of landing page URLs, optional

    Returns:
        op: dict representing the keyword add operation

    """
    op = {'xsi_type': 'AdGroupCriterionOperation',
          'operand': {
              'xsi_type': 'BiddableAdGroupCriterion',
              'adGroupId': adgroup_id,
              'criterion': {
                  'xsi_type': 'Keyword',
                  'text': keyword,
                  'matchType': match_type
              }
          },
          'operator': 'ADD'}

    if finalUrls:
        op['operand']['finalUrls'] = finalUrls
    return op


def get_batch_job_helper(client, _version=API_VERSION):
    """
    Get batch job help from client

    Args:
        client: adwords client
        _version: api version, defaults to package const API_VERSION

    Returns:
        helper: instance of batch job helper tied to client
    """
    return client.GetBatchJobHelper(version=_version)


def get_full_campagin_example():
    """
    Just sample code from PHCI full build for Exacts.

    Steps:
    0. read in and clean up base list of kws along with suggested cm and themes (e.g. from google sheet)
    1. Generate permutations and concatenate into full build using pandas dfs
    2. Build out full hierarchy of campaigns -> ad groups -> ads and kws using pandas groupbys
    3. Check ops and ASSERT all campaigns are created as PAUSED
    4. Batch job create
    5. Check responses / errors

    #####

    DEVICE_TYPES = ['D', 'M']
    MATCH_TYPES = ['Exact']  # Campaign name ending in 'Exact' rather just 'E' for easier filtering on UI

    # Use a groupby instead to break out by campaigns, ad groups, and keywords - concat by device and match type
    # permutations - then iterate through the groupby to build operations

    dfs = []

    for device in DEVICE_TYPES:
        for match in MATCH_TYPES:
            df = meta[meta.columns]
            df['CampaignFullName'] = df.SuggestedCampaign + ':' + device + ':' + match
            dfs.append(df)

    df = pd.concat(dfs)

    # df now consists of cols Query, SuggestedCampaign, SuggestedAdGroup, Cost, and CampaignFullName.

    cm_ops = []
    ag_ops = []
    kw_ops = []
    ad_ops = []

    def assign_lp_url(ad_group_name):
        if ad_group_name == 'Balance Transfer':
            return 'https://www.singsaver.com.sg/personal-loan/balance-transfer/results'
        elif ad_group_name == 'Debt Consolidation':
            return 'https://www.singsaver.com.sg/personal-loan/debt-consolidation/results'
        elif ad_group_name == 'Standby Cash':
            return 'https://www.singsaver.com.sg/personal-loan/standby-cash/results'
        elif ad_group_name == 'Home Improvement':
            return 'https://www.singsaver.com.sg/personal-loan/home-improvement/results'
        else:
            return 'https://www.singsaver.com.sg/personal-loan/instalment/results'

    for cname, cframe in df.groupby('CampaignFullName'):
        # create the campaign, separate budget for CI vs DS[A]
        budget_id = budget_id_lkup['CI'] if ':CI:' in cname else budget_id_lkup['DS']
        cm_id, cm_op = create_campaign_op(batch_job_helper, budget_id, cname)
        cm_ops.append(cm_op)
        for agname, agframe in cframe.groupby('SuggestedAdGroup'):
            # create the ag group
            ag_id, ag_op = create_adgroup_op(batch_job_helper, cm_id, agname)
            ag_ops.append(ag_op)
            # create default ads
            ad_ops.append(CreateDefaultAd1Op(ag_id))
            ad_ops.append(CreateDefaultAd2Op(ag_id))
            ad_ops.append(CreateDefaultAd3Op(ag_id))
            ad_ops.append(CreateDefaultDKI1Op(ag_id))
            # no DKI2
            ad_ops.append(CreateDefaultDKI3Op(ag_id))
            # create kws
            for kw in agframe.Query:
                # match_type based on campaign name
                # campaign name will contain Broad or Exact rather than just B or E
                mtype = 'BROAD' if cname.split(':')[-1][0] == 'B' else 'EXACT'
                # for now we just have ONE url, no different mobile one
                kw_op = CreateKeywordOp(ag_id, kw, mtype,
                                        finalUrls=[assign_lp_url(agname)]
                                       )
                kw_ops.append(kw_op)

    pprint(ag_ops[3])

    assert all([c['operand']['status'] == 'PAUSED' for c in cm_ops])

    # Create a BatchJob.
    batch_job = add_batch_job(client)

    # Retrieve the URL used to upload the BatchJob operations.
    upload_url = batch_job['uploadUrl']['url']
    batch_job_id = batch_job['id']
    print('Created BatchJob with ID "%d", status "%s", and upload URL "%s"' % (
      batch_job['id'], batch_job['status'], upload_url))

    # Upload operations.
    batch_job_helper.UploadOperations(upload_url, cm_ops, ag_ops, ad_ops, kw_ops)
    # batch_job_helper.UploadOperations(upload_url, cm_ops)

    # Download and display results.
    download_url = get_batch_job_download_url(client, batch_job_id)
    response = urllib.request.urlopen(download_url).read()

    # print_response(batch_job_helper, response)

    print_errors(batch_job_helper, response)

    """
    pass


# todo: add logic to parse errors and trigger re-try or manual review if possible


def create_bj_ops_for_campaigns(input_df, bjh, budget_mapper, default_ads=None, kw_url_mapper=None,
                                cm_col='CampaignName', ag_col='AdGroupName', kw_col='Keyword', max_cpc=0):
    """
    Creates batch job ops for campaigns from pandas dataframe, including
    - new campaigns with associated shared budget(s)
    - ad groups and default ads
    - keywords
    Note: generally for new account creations, you can run this WITHOUT the ads or kw urls first, just to create the
    necessary campaigns and ad groups and then, run create_eta_ads_from_df() and update_kws_final_urls_from_df()
    separately, afterwards

    If creating a mix of new and existing campaigns and/or ad groups, see upsert_bj_ops_for_campaigns instead

    Args:
        input_df: dataframe with campaign, ad group, and keywords
        cm_col: column containing campaigns
        ag_col: column containing ad groups
        kw_col: column containing keywords
        bjh: batch job helper, needed to assign temp Ids
        budget_mapper: function to map campaign name -> shared budget Id
        default_ads: default ad copies as a list of {}'s, this is optional
        kw_url_mapper: function to map ad group -> final url(s) AT KEYWORD LEVEL
        # todo: change to use regex logic, need both campaign and adgroup names
        max_cpc: optionally specify an ad group level max cpc

    Returns:
        cm_ops: ops for creating campaigns
        ag_ops: ops for creating ad groups
        ad_ops: (optionally) ops for creating default ads
        kw_ops: ops for creating keywords
    """
    cm_ops = []
    ag_ops = []
    ad_ops = []
    kw_ops = []

    for cname, cframe in input_df.groupby(cm_col):
        budget_id = budget_mapper(cname)
        cm_id, cm_op = create_campaign_op(bjh, budget_id, cname)
        cm_ops.append(cm_op)
        for agname, agframe in cframe.groupby(ag_col):
            ag_id, ag_op = create_adgroup_op(bjh, cm_id, agname, default_max_cpc=max_cpc)
            ag_ops.append(ag_op)

            # create default ads, IF PROVIDED
            if default_ads:
                ad_ops.extend([create_eta_op(ag_id, **ad) for ad in default_ads])

            # assign kw-level url if needed
            print(agframe.columns)
            url_list = kw_url_mapper(agname) if kw_url_mapper else \
                agframe.LandingPageURL[0] if 'LandingPageURL' in agframe.columns else None

            # create kws
            for kw in agframe[kw_col]:
                # match_type based on first letter of the last token in campaign name
                mtype = 'BROAD' if cname.split(':')[-1][0] == 'B' else 'EXACT'
                kw_op = create_keyword_op(ag_id, kw, mtype, finalUrls=url_list)
                kw_ops.append(kw_op)

    assert all([c['operand']['status'] == 'PAUSED' for c in cm_ops])

    if ad_ops:
        return cm_ops, ag_ops, ad_ops, kw_ops

    return cm_ops, ag_ops, kw_ops


def upsert_bj_ops_for_campaigns(client, input_df, bjh, budget_mapper, default_ads=None, kw_url_mapper=None,
                                cm_col='CampaignName', ag_col='AdGroupName', kw_col='Keyword', max_cpc=0):
    """
    Based on create_bj_ops_for_campaigns but will "update rather than insert" if campaigns and/or adgroups exist already

    Creates batch job ops for campaigns from pandas dataframe, including
    - new campaigns with associated shared budget(s) OR insert into existing campaigns
    - ad groups and default ads OR insert into existing ad groups
    - keywords
    Note: generally for new account creations, you can run this WITHOUT the ads or kw urls first, just to create the
    necessary campaigns and ad groups and then, run create_eta_ads_from_df() and update_kws_final_urls_from_df()
    separately, afterwards

    todo: unclear what happens when you try to create a keyword that already exists. Currently we do NOT check.

    Args:
        client: adwords non-MCC client, needed to check for existing entities in the account
        input_df: dataframe with campaign, ad group, and keywords
        cm_col: column containing campaigns
        ag_col: column containing ad groups
        kw_col: column containing keywords
        bjh: batch job helper, needed to assign temp Ids
        budget_mapper: function to map campaign name -> shared budget Id
        default_ads: default ad copies as a list of {}'s, this is optional
        kw_url_mapper: function to map ad group -> final url(s) AT KEYWORD LEVEL
        max_cpc: optionally specify an ad group level max cpc

    Returns:
        cm_ops: ops for creating campaigns
        ag_ops: ops for creating ad groups
        ad_ops: (optionally) ops for creating default ads
        kw_ops: ops for creating keywords
    """
    cm_ops = []
    ag_ops = []
    ad_ops = []
    kw_ops = []

    # check against existing all-but-removed campaigns and ad groups
    # if exists will append CampaignId and AdGroupId
    # Note: this is a 2-step merge as it's possible to have campaigns that match but a new ad group
    cms_ags = get_all_but_removed_campaigns_ad_adgroups(client)
    df = input_df.merge(
        cms_ags[['CampaignName', 'CampaignId']],
        how='left',
        left_on=['CampaignFullName'],
        right_on=['CampaignName'],
        suffixes=('', '_')
    )

    df = df.merge(
        cms_ags[['CampaignName', 'AdGroupName', 'AdGroupId']],
        how='left',
        left_on=['CampaignFullName', 'AdGroupName'],
        right_on=['CampaignName', 'AdGroupName'],
        suffixes=('', '_')
    )

    for cname, cframe in df.groupby(cm_col):
        budget_id = budget_mapper(cname)

        cm_id = cframe.CampaignId.iloc[0]  # should all be the same value for a given cm name

        if pd.isna(cm_id):  # campaign doesn't exist
            cm_id, cm_op = create_campaign_op(bjh, budget_id, cname)
            cm_ops.append(cm_op)
        else:
            cm_id = int(cm_id)

        for agname, agframe in cframe.groupby(ag_col):
            ag_id = agframe.AdGroupId.iloc[0]

            if pd.isna(ag_id):
                ag_id, ag_op = create_adgroup_op(bjh, cm_id, agname, default_max_cpc=max_cpc)
                ag_ops.append(ag_op)
            else:
                ag_id = int(ag_id)

            # create default ads, IF PROVIDED
            if default_ads:
                ad_ops.extend([create_eta_op(ag_id, **ad) for ad in default_ads])

            # assign kw-level url if needed
            url_list = kw_url_mapper(agname) if kw_url_mapper else None

            # create kws
            for kw in agframe[kw_col]:
                # match_type based on first letter of the last token in campaign name
                mtype = 'BROAD' if cname.split(':')[-1][0] == 'B' else 'EXACT'
                kw_op = create_keyword_op(ag_id, kw, mtype, finalUrls=url_list)
                kw_ops.append(kw_op)

    assert all([c['operand']['status'] == 'PAUSED' for c in cm_ops])

    if ad_ops:
        return cm_ops, ag_ops, ad_ops, kw_ops

    return cm_ops, ag_ops, kw_ops


def run_batch_create(client, bjh, campaign_ops, adgroup_ops, keyword_ops, adcopy_ops=None,
                     assert_paused=True, _print_response=False, _print_errors=True):
    """
    Runs batch job to create campaigns, ad groups, keywords, and optionally ads

    Args:
        client: adwords client
        bjh: batch job helper
        campaign_ops: list of ops for creating campaigns
        adgroup_ops: list of ops for creating ad groups
        keyword_ops: list of ops for creating keywords
        adcopy_ops: (optional) list of ops for creating ad copies
        assert_paused: check all campaigns are created as PAUSED
        _print_response: print out response from the batch job (This is only a summary snippet per operation)
        _print_errors: print out errors from the batch job

    Returns:
        response: response from the batch job which is xml, can use bjh.ParseResponse(resp) to parse
            e.g. list_of_rvals = bjh.ParseResponse(resp)['mutateResponse']['rval']

    """
    if assert_paused:
        assert all([c['operand']['status'] == 'PAUSED' for c in campaign_ops])

    # Create a BatchJob.
    batch_job = add_batch_job(client)

    # Retrieve the URL used to upload the BatchJob operations.
    upload_url = batch_job['uploadUrl']['url']
    batch_job_id = batch_job['id']
    print('Created BatchJob with ID "%d", status "%s", and upload URL "%s"' % (
        batch_job['id'], batch_job['status'], upload_url))

    # Upload operations.
    if adcopy_ops:
        bjh.UploadOperations(upload_url, campaign_ops, adgroup_ops, adcopy_ops, keyword_ops)
    else:
        bjh.UploadOperations(upload_url, campaign_ops, adgroup_ops, keyword_ops)

    # Download and display results.
    download_url = get_batch_job_download_url(client, batch_job_id)
    response = request.urlopen(download_url).read()

    if _print_response:
        print_response(bjh, response)

    if _print_errors:
        print_errors(bjh, response)

    return response


def set_campaign_level_targeting(client, account_cid, cm_ids, criteria_list, version=API_VERSION):
    """
    Sets targeting criteria at the campaign level, e.g. Geo and Language
    Guide @ https://developers.google.com/adwords/api/docs/samples/python/targeting#add-targeting-criteria-to-a-campaign
    Lookup @ https://fusiontables.google.com/DataSource?docid=1Jlxrqc1dU3a9rsNW2l5xxlmQEKUu0dIPusImi41B#rows:id=1

    Args:
        client: adwords client
        account_cid: parent account
        cm_ids: list of existing campaign IDs
        criteria_list: list of criteria. E.g. for PH-en
            [{'xsi_type': 'Location', 'id': '2608'}, {'xsi_type': 'Language', 'id': '1000'}]
        version: defaults to API_VERSION  # todo: get rid of version parameter entirely from all functions?

    Returns:
        Response from CampaignCriterionService.mutate()

    """
    client.SetClientCustomerId(account_cid)

    ops = []
    for cm_id in cm_ids:
        for criteria in criteria_list:
            ops.append({'operator': 'ADD',
                        'operand': {'campaignId': cm_id, 'criterion': criteria}})

    cm_criterion_svc = client.GetService('CampaignCriterionService', version=version)

    response = cm_criterion_svc.mutate(ops)

    return response


def get_active_exact_kws(client):
    """
    Get UNIQUE list, i.e. set, of currently enabled exact-match keywords for the account

    Args:
        client: adwords client

    Returns:
        list of keywords

    """
    sel = create_selector(['Id', 'Criteria', 'KeywordMatchType'],
                          pred_fields=['KeywordMatchType', 'Status'],
                          pred_operators=['EQUALS', 'EQUALS'], pred_values=['EXACT', 'ENABLED'])

    exacts_df = get_report_as_df(client, sel, 'KEYWORDS_PERFORMANCE_REPORT')

    return exacts_df.Criteria.unique().tolist()


def create_shared_negatives_set(client, set_name):
    """
    Creates a single shared negatives list

    Args:
        client: adwords client with the correct account ID set
        set_name: name of the shared negatives list

    Returns:
        shared_set_id: ID of the newly created shared set

    """
    shared_set_service = client.GetService('SharedSetService', version=API_VERSION)
    # Create shared negative keyword set.
    shared_set = {
        'name': set_name,
        'type': 'NEGATIVE_KEYWORDS'
    }
    response = shared_set_service.mutate([{'operator': 'ADD', 'operand': shared_set}])
    shared_set_id = response['value'][0]['sharedSetId']
    return shared_set_id


def get_all_shared_sets(client, status='ENABLED'):
    """
    Get meta data on current shared sets in the given account

    Args:
        client: adwords client for the given account
        status: in ['ENABLED', 'REMOVED', 'UNKNOWN'] but defaults to ['ENABLED']


    Returns:
        pandas dataframe containing all shared sets for the given account

    """
    selector = create_selector(cols_list=['SharedSetId', 'Name', 'Type', 'MemberCount', 'ReferenceCount', 'Status'],
                               pred_fields=['Status'], pred_operators=['IN'], pred_values=[status])
    return get_paged_data_from_svc('SharedSetService',
                                   selector=selector,
                                   adwords_client=client)


def update_negatives_in_set(client, neg_kw_tuples, set_id=None, set_name=None):
    """
    Entirely updates the negative keywords in a shared set. I.e. completely replaces pre-existing negative kws
    CAN be used for list creations too, i.e. handles cases where the original list is empty

    Args:
        client: adwords client with the correct account ID set
        neg_kw_tuples: list of tuples containing negative kw and match type. E.g. [('abc', 'EXACT'), ('xyz', 'PHRASE')]
            Note: use get_active_exact_kws() as needed
        set_id: shared set id
        set_name: lookup set id by name, IGNORED IF set_id IS PROVIDED

    Returns:
        responses from SharedCriterionService.mutate() calls
    """
    responses = []

    shared_criterion_service = client.GetService('SharedCriterionService', version=API_VERSION)

    # if not set_id but set_name was provided
    if not set_id and set_name:
        shared_sets_df = get_all_shared_sets(client)
        set_id = shared_sets_df[shared_sets_df.Name == set_name].SharedSetId[0]

    # get current negative kws and remove them from the shared set
    sel = create_selector(['SharedSetId', 'Id', 'KeywordText', 'KeywordMatchType', 'PlacementUrl'],
                          pred_fields=['SharedSetId'], pred_operators=['IN'], pred_values=[set_id])
    current_negs = get_paged_data_from_svc('SharedCriterionService', sel, client)

    if type(current_negs) == type(pd.DataFrame()):
        if not (current_negs.empty):  # if set is not originally empty, then remove contents
            remove_ops = [{
                'operator': 'REMOVE',
                'operand': {
                    'criterion': {
                        'id': n.id
                    },
                    'sharedSetId': set_id
                }
            } for n in current_negs.Criterion.values]
            responses.append(shared_criterion_service.mutate(remove_ops))

    # add new list of negatives to the shared set
    shared_criteria = [
        {
            'criterion': {
                'xsi_type': 'Keyword',
                'text': neg_kw,
                'matchType': matchtype
            },
            'negative': True,
            'sharedSetId': set_id
        } for neg_kw, matchtype in neg_kw_tuples
    ]

    add_ops = [
        {
            'operator': 'ADD',
            'operand': criterion
        } for criterion in shared_criteria
    ]

    responses.append(shared_criterion_service.mutate(add_ops))

    return responses


def attach_negatives_to_campaigns(client, cm_list, set_ids):
    """
    Attaches (i.e. ADD) shared negative list(s) to campaign(s)

    Args:
        client: adwords client for the given account
        cm_list: list of campaign IDs
        set_ids: list of shared set IDs

    Returns:
        response from the CampaignSharedSetService.mutate()

    """
    campaign_shared_set_service = client.GetService('CampaignSharedSetService', version=API_VERSION)
    cm_sets = [{'campaignId': cm, 'sharedSetId': set_id} for cm in cm_list for set_id in set_ids]
    ops = [{'operator': 'ADD', 'operand': cs} for cs in cm_sets]
    response = campaign_shared_set_service.mutate(ops)
    return response


def remove_negatives_from_campaigns(client, cm_list, set_ids):
    """
    Removes (i.e. REMOVE) shared negative list(s) to campaign(s)
    todo: should merge with the attach function since only difference is ADD vs REMOVE operator, req name change

    Args:
        client: adwords client for the given account
        cm_list: list of campaign IDs
        set_ids: list of shared set IDs

    Returns:
        response from the CampaignSharedSetService.mutate()

    """
    campaign_shared_set_service = client.GetService('CampaignSharedSetService', version=API_VERSION)
    cm_sets = [{'campaignId': cm, 'sharedSetId': set_id} for cm in cm_list for set_id in set_ids]
    ops = [{'operator': 'REMOVE', 'operand': cs} for cs in cm_sets]
    response = campaign_shared_set_service.mutate(ops)
    return response


def create_account_extensions(client, snippets=None, callouts=None, sitelinks=None, calls=None):
    """
    Creates account level extensions. Currently supports structured snippets, call outs, and sitelinks
    https://developers.google.com/adwords/api/docs/reference/v201802/CustomerExtensionSettingService.ExtensionFeedItem

    Args:
        client: adwords client for the given account
        snippets: list of StructuredSnippetFeedItems, e.g.
            [{'xsi_type': 'StructuredSnippetFeedItem', 'header': 'Brands', 'values': ['HSBC', 'UOB', 'BOC']}, ...]
            Note: only certain headers are allowed, more at link below
            https://developers.google.com/adwords/api/docs/appendix/structured-snippet-headers
            Note: headers need to be localized. Accepted values @ https://fusiontables.google.com/DataSource?docid=
            1WsjLeMap4eJKDWZSnHFn2231zwBqRFrf2SoDtgLu#rows:id=1 E.g. Types in zh-HK is "類型"
        callouts: list of CalloutFeedItems, e.g
            [{'calloutText': 'Completely free of charge', 'xsi_type': 'CalloutFeedItem'}, ...]
        sitelinks: list of SitelinkFeedItems, e.g
            [{'sitelinkFinalUrls': {'urls': ['https://www.moneymax.ph/car-insurance']},
             'sitelinkLine2': 'Your Car Insurance Expiring?',
             'sitelinkLine3': 'Get The Best Car Insurance Here!',
             'sitelinkText': 'Policy Expiring',
             'xsi_type': 'SitelinkFeedItem'}, ... ]
        calls: list of CallFeedItems, e.g.,
            [{'xsi_type': 'CallFeedItem',
              'callPhoneNumber': '021-501-01707',
              'callCountryCode': 'ID',
              'callTracking': False}, ... ]

    Returns:
        response from CustomerExtensionSettingService.mutate()

    """
    operands = []

    if snippets:
        operands.append({'extensionType': 'STRUCTURED_SNIPPET',
                         'extensionSetting': {'extensions': snippets}})

    if callouts:
        operands.append({'extensionType': 'CALLOUT',
                         'extensionSetting': {'extensions': callouts}})

    if sitelinks:
        operands.append({'extensionType': 'SITELINK',
                         'extensionSetting': {'extensions': sitelinks}})

    if calls:
        operands.append({'extensionType': 'CALL',
                         'extensionSetting': {'extensions': calls}})

    ops = [{'operator': 'ADD', 'operand': op} for op in operands]

    acct_ext_svc = client.GetService('CustomerExtensionSettingService', version=API_VERSION)

    response = acct_ext_svc.mutate(ops)

    return response


def create_campaign_extensions(client, snippets=None, callouts=None,
                               sitelinks=None, calls=None):
    """
    Creates campaign level extensions. Currently supports structured snippets, call outs, and sitelinks
    https://developers.google.com/adwords/api/docs/reference/v201806/CampaignExtensionSettingService

    Args:
        client: adwords client for the given account
        snippets: list of StructuredSnippetFeedItems, e.g.
            [{'campaignId':  1543766848,'xsi_type': 'StructuredSnippetFeedItem', 'header': 'Brands', 'values': ['HSBC', 'UOB', 'BOC']}, ...]
            Note: only certain headers are allowed, more at link below
            https://developers.google.com/adwords/api/docs/appendix/structured-snippet-headers
        callouts: list of CalloutFeedItems, e.g
            [{'campaignId':  1543766848, 'calloutText': 'Completely free of charge', 'xsi_type': 'CalloutFeedItem'}, ...]
        sitelinks: list of SitelinkFeedItems, e.g
            [{'campaignId':  1543766848,
              'sitelinkFinalUrls': {'urls': ['https://www.moneymax.ph/car-insurance']},
              'sitelinkLine2': 'Your Car Insurance Expiring?',
              'sitelinkLine3': 'Get The Best Car Insurance Here!',
              'sitelinkText': 'Policy Expiring',
              'xsi_type': 'SitelinkFeedItem'}, ... ]
        calls: list of CallFeedItems, e.g.,
            [{'campaignId':  1543766848,
              'xsi_type': 'CallFeedItem',
              'callPhoneNumber': '021-501-01707',
              'callCountryCode': 'ID',
              'callTracking': False}, ... ]

    Returns:
        response from CampaignExtensionSettingService.mutate()

    """
    operands = []

    if snippets:
        for item in snippets:
            operands.append({'campaignId': item.pop('campaignId'), 'extensionType': 'STRUCTURED_SNIPPET',
                             'extensionSetting': {'extensions': item}})

    if callouts:
        for item in callouts:
            operands.append({'campaignId': item.pop('campaignId'), 'extensionType': 'CALLOUT',
                             'extensionSetting': {'extensions': item}})

    if sitelinks:
        for item in sitelinks:
            operands.append({'campaignId': item.pop('campaignId'), 'extensionType': 'SITELINK',
                             'extensionSetting': {'extensions': item}})

    if calls:
        for item in calls:
            operands.append({'campaignId': item.pop('campaignId'), 'extensionType': 'CALL',
                             'extensionSetting': {'extensions': item}})

    ops = [{'operator': 'ADD', 'operand': op} for op in operands]

    acct_ext_svc = client.GetService('CampaignExtensionSettingService', version=API_VERSION)

    response = acct_ext_svc.mutate(ops)

    return response


def create_adgroup_extensions(client, snippets=None, callouts=None,
                              sitelinks=None, calls=None):
    """
    Creates adgroup level extensions. Currently supports structured snippets, call outs, and sitelinks
    https://developers.google.com/adwords/api/docs/reference/v201806/AdGroupExtensionSettingService

    Args:
        client: adwords client for the given account
        snippets: list of StructuredSnippetFeedItems, e.g.
            [{'adGroupId':  61695900467,'xsi_type': 'StructuredSnippetFeedItem', 'header': 'Brands', 'values': ['HSBC', 'UOB', 'BOC']}, ...]
            Note: only certain headers are allowed, more at link below
            https://developers.google.com/adwords/api/docs/appendix/structured-snippet-headers
        callouts: list of CalloutFeedItems, e.g
            [{'adGroupId':  61695900467, 'calloutText': 'Completely free of charge', 'xsi_type': 'CalloutFeedItem'}, ...]
        sitelinks: list of SitelinkFeedItems, e.g
            [{'adGroupId':  61695900467,
              'sitelinkFinalUrls': {'urls': ['https://www.moneymax.ph/car-insurance']},
              'sitelinkLine2': 'Your Car Insurance Expiring?',
              'sitelinkLine3': 'Get The Best Car Insurance Here!',
              'sitelinkText': 'Policy Expiring',
              'xsi_type': 'SitelinkFeedItem'}, ... ]
        calls: list of CallFeedItems, e.g.,
            [{'adGroupId':  61695900467,
              'xsi_type': 'CallFeedItem',
              'callPhoneNumber': '021-501-01707',
              'callCountryCode': 'ID',
              'callTracking': False}, ... ]

    Returns:
        response from AdGroupExtensionSettingService.mutate()

    """
    operands = []

    if snippets:
        for item in snippets:
            operands.append({'adGroupId': item.pop('adGroupId'), 'extensionType': 'STRUCTURED_SNIPPET',
                             'extensionSetting': {'extensions': item}})

    if callouts:
        for item in callouts:
            operands.append({'adGroupId': item.pop('adGroupId'), 'extensionType': 'CALLOUT',
                             'extensionSetting': {'extensions': item}})

    if sitelinks:
        for item in sitelinks:
            operands.append({'adGroupId': item.pop('adGroupId'), 'extensionType': 'SITELINK',
                             'extensionSetting': {'extensions': item}})

    if calls:
        for item in calls:
            operands.append({'adGroupId': item.pop('adGroupId'), 'extensionType': 'CALL',
                             'extensionSetting': {'extensions': item}})

    ops = [{'operator': 'ADD', 'operand': op} for op in operands]

    acct_ext_svc = client.GetService('AdGroupExtensionSettingService', version=API_VERSION)

    response = acct_ext_svc.mutate(ops)

    return response


def set_campaign_device_bid_modifiers(client, cms_df=None):
    """
    Sets device bid modifiers according to campaign name.
    Note: assumes campaigns in the given account follow the expected naming convention.
    i.e. <type>:<nomae>:<vertical>:<device>:<matchtype>, eg. B:MoneyMax:CI:M:Broad

    Args:
        client: adwords client for the given account
        cms_df: campaigns dataframe with columns Id and Device. Optional and if omitted,
            will query for all campaigns within this account

    Returns:
        response from the CampaignCriterionService.mutate()

    """
    # todo: integrate this step into create_campaign_op

    # https://developers.google.com/adwords/api/docs/appendix/platforms
    mobile = {'xsi_type': 'Platform', 'id': '30001'}
    tablet = {'xsi_type': 'Platform', 'id': '30002'}
    desktop = {'xsi_type': 'Platform', 'id': '30000'}

    if not cms_df:
        # get all campaigns with name and Id
        cms_df = get_paged_data_from_svc('CampaignService', create_selector(['Name', 'Id']), client)

        # parse out device from campaign name, MUST FOLLOW NAMING CONVENTION
        cms_df['Device'] = cms_df.Name.apply(lambda x: x.split(':')[3])

    device_bid_adj_operand_list = []

    for t in cms_df.iterrows():
        r = t[1]
        if r.Device == 'D':
            device_bid_adj_operand_list.append({'campaignId': r['Id'], 'criterion': mobile, 'bidModifier': '0.0'})
        elif r.Device == 'M':
            device_bid_adj_operand_list.append({'campaignId': r['Id'], 'criterion': tablet, 'bidModifier': '0.0'})
            device_bid_adj_operand_list.append({'campaignId': r['Id'], 'criterion': desktop, 'bidModifier': '0.0'})
        else:
            # might have some 'A'?
            continue

    ops = [{'operator': 'SET', 'operand': operand} for operand in device_bid_adj_operand_list]
    cm_criterion_svc = client.GetService('CampaignCriterionService', version=API_VERSION)
    response = cm_criterion_svc.mutate(ops)
    return response


def create_tCPA_strategies(client, targets):
    """
    Creates shared target CPA bidding strategies in the given account
    Note: need to set up CONVERSIONS in the account first for tCPA, even if dummy

    Args:
        client: adwords client for the given account
        targets: targets as a list of tuples in the form of [(<name>, <cpa_in_dollars>), ...]
            CPA value will be multiplied by MIL

    Returns:
        dict containing the names and ids of the newly created tCPA strategies

    """
    bid_strat_svc = client.GetService('BiddingStrategyService', version=API_VERSION)

    ops = [{'operator': 'ADD',
            'operand': {'name': tgt[0],
                        'type': 'TARGET_CPA',
                        'biddingScheme': {'xsi_type': 'TargetCpaBiddingScheme',
                                          'targetCpa': {'microAmount': tgt[1] * MIL}
                                          }
                        }
            } for tgt in targets]

    response = bid_strat_svc.mutate(ops)

    fbs_lkup = {}

    rv = response['value']

    if rv:
        for bidding_scheme in rv:
            fbs_lkup[bidding_scheme['name']] = bidding_scheme['id']

    return fbs_lkup


def get_shared_bidding_strategies(client, selector=None):
    """
    Returns dataframe of shared bidding strategies available in the given client.
    By default only returns Status == ENABLED
    https://developers.google.com/adwords/api/docs/reference/v201806/BiddingStrategyService.SharedBiddingStrategy

    Args:
        client: adwords client for given account
        selector: selector containing fields and predicates to be used with BiddingStrategyService.get().
            Defaults to returning ENABLED only strategies.

    Returns:
        Pandas dataframe

    """
    if not selector:
        selector = create_selector(cols_list=['BiddingScheme', 'Id', 'Name', 'Status', 'Type'],
                                   pred_fields=['Status'],
                                   pred_operators=['EQUALS'],
                                   pred_values=['ENABLED'])

    return get_paged_data_from_svc('BiddingStrategyService', selector=selector, adwords_client=client)


def attach_fbs_to_campaigns(client, fbs_id, campaign_ids):
    """
    Attaches the given bid strategy to the list of campaigns

    Args:
        client: adwords client
        fbs_id: bid strategy id
        campaign_ids: list of campaign ids

    Returns:
        response from CampaignService.mutate()

    """
    ops = [{'operator': 'SET',
            'operand': {'id': cm_id,
                        'biddingStrategyConfiguration': {'biddingStrategyId': fbs_id}
                        }
            } for cm_id in campaign_ids]

    cm_svc = client.GetService('CampaignService', version=API_VERSION)

    response = cm_svc.mutate(ops)

    return response


def create_text_label(client, label_name=None, label_color=None, label_desc=None):
    """
    Creates a single text label with optional background color and/or description

    Args:
        client: adwords client for the given account
        label_name: name on the label, defaults to Ymd
        label_color: optional background color string must begin with a '#' character followed by either 6 or 3
            hexadecimal characters e.g. '#ffff00', which is yellow
        label_desc: optional description

    Returns:
        response from the mutate operation, which should contain label Id of the newly created label

    """
    label_svc = client.GetService('LabelService', version=API_VERSION)

    if not label_name:
        # default to Ymd
        from datetime import datetime as dt
        label_name = f'{dt.today():%Y%m%d}'

    op = {'operator': 'ADD',
          'operand': {'name': label_name,
                      'xsi_type': 'TextLabel'}
          }

    color, desc = {}, {}

    if label_color:
        color = {'xsi_type': 'DisplayAttribute',
                 'backgroundColor': label_color}

    if label_desc:
        desc = {'xsi_type': 'DisplayAttribute',
                'description': label_desc}

    op['operand']['attribute'] = {**color, **desc}

    response = label_svc.mutate(op)

    return response


def attach_label(client, label_id, entity_service_name, entity_id_name, entity_ids, ad_group_ids=None):
    """
    Attaches the given label to the list of entities. For keywords (i.e ad group criterion) and ads,
    you need to pass in the corresponding ad group ids also

    Args:
        client: adwords client for the given account
        label_id: id of the label, NOT the name
        entity_service_name: name of the service used to associate the label to the entities
            [CampaignService, AdGroupService, AdGroupAdService, AdGroupCriterionService]
        entity_id_name: name of the Id for the entity, used in the ops operand
            [campaignId, adGroupId, adId, criterionId]
        entity_ids: list of entity ids
        ad_group_ids: list of ad group ids, corresponding to entity_ids. Required if entity is keyword or ad copy

    Returns:
        response from the mutateLabel operation

    """
    svc = client.GetService(entity_service_name, version=API_VERSION)

    if ad_group_ids:
        if len(ad_group_ids) != len(entity_ids):
            raise Exception('Lists of ad group ids and entity_ids must match.')
        else:
            ops = [{'operator': 'ADD', 'operand': {'adGroupId': agid, entity_id_name: eid, 'labelId': label_id}} for
                   agid, eid in zip(ad_group_ids, entity_ids)]
    else:
        ops = [{
            'operator': 'ADD',
            'operand': {
                entity_id_name: eid,
                'labelId': label_id,
            }
        } for eid in entity_ids]

    return svc.mutateLabel(ops)


def get_label_ids_by_name(client, label_names):
    """
    Returns label Ids for a given list of label names
    Note: different from get_account_label_ids_by_name

    Args:
        client: adwords client for the given account
        label_names: list of label names to lookup

    Returns:
        Dict of {name: id} or None

    """
    svc = client.GetService('LabelService', version=API_VERSION)

    sel = create_selector(cols_list=['LabelId', 'LabelName'],
                          pred_fields=['LabelName'], pred_operators=['IN'],
                          pred_values=[label_names])

    response = svc.get(sel)

    if response['entries']:
        return {e['name']: e['id'] for e in response['entries']}

    return


def pause_ads_by_id(client, adgroup_ids, ad_ids, label=True, label_id=None, label_name=None, return_ops=True):
    """
    Pause list of ad copies and optionally label them. Works with partial failures but will need to
    client.partial_failure=True before calling this function
    Note: label will be applied to ads even if ad was already in paused statement previously
    Note: could run get_num_live_ads() before and after to make sure that the pausing hasn't left some live ad groups
    without any ads that can serve. Todo: include this as a check within this function.

    Args:
        client: adwords client for the given account
        adgroup_ids: list of ids for the parent ad groups, must MATCH ad_ids
        ad_ids: list of ad copy ids to be paused
        label: optionally attach label to the paused ads, defaults to True
        label_id: id of an existing label to be applied
        label_name: name of the NEW label to be applied, if not given, defaults to "YYYYmmdd - Paused"
        return_ops: return ops, defaults to True

    Returns:
        list of responses from various operations, as well as optionally the ops

    Raises:
        Exception: adgroup ids MUST MATCH ad ids
    """
    if len(adgroup_ids) != len(ad_ids):
        raise Exception('adgroup ids MUST MATCH ad ids')

    agad_svc = client.GetService('AdGroupAdService', version=API_VERSION)

    responses = []

    ops = [{
        'operator': 'SET',
        'operand': {
            'adGroupId': agid,
            'ad': {'id': aid},
            'status': 'PAUSED'
        }
    } for agid, aid in zip(adgroup_ids, ad_ids)]

    if return_ops:
        responses.append(ops)

    responses.append(agad_svc.mutate(ops))

    if label:
        if not label_id:
            # create new label
            if not label_name:
                # create new label with default
                from datetime import datetime as dt
                label_name = f'{dt.today():%Y%m%d} - Paused'
            responses.append(create_text_label(client, label_name))
            # noinspection PyTypeChecker
            label_id = responses[-1]['value'][0]['id']

        responses.append(attach_label(client, label_id, 'AdGroupAdService', 'adId', ad_ids, adgroup_ids))

    return responses


def get_all_but_removed_campaigns_ad_adgroups(client):
    """
    Returns list of existing campaigns and ad groups, with their names and IDs

    Args:
        client: adwords non-MCC client

    Returns:
        Dataframe containing meta data for all but removed campaigns and adgroups within the given client

    """
    cms_ags = get_report_as_df(client,
                               create_selector(
                                   cols_list=['AdGroupId', 'AdGroupName', 'CampaignId', 'CampaignName'],
                                   pred_fields=['CampaignStatus', 'AdGroupStatus'],
                                   pred_operators=['NOT_EQUALS', 'NOT_EQUALS'],
                                   pred_values=['REMOVED', 'REMOVED']),
                               report_type='ADGROUP_PERFORMANCE_REPORT')
    return cms_ags


# noinspection PyIncorrectDocstring,PyUnresolvedReferences
def create_eta_ads_from_df(client, new_ads_df, lp_lookup, label=True,
                           label_id=None, label_name=None):
    """
    higher level function that takes a dataframe, usually from sheets,
    containing new ads and regex for campaign and ad group mapping and
    creates new ads accordingly. Optionally label the new ads also.
    Note: Excludes REMOVED ad groups and campaigns
    Note: there are some differences between what's allowed via the UI
    and what's allowed via API
        e.g. you can NOT have "compare 100+ cards"; you can NOT have
        commas in the path like "10,000 miles"
    Note: can include Description2 and Headline3 in df
    todo: test Description2 and Headline3 - have not yet tested

    Args:
        client: adwords client of the given account
        new_ads_df: pandas dataframe with the following cols
            ['CampaignRegex', 'AdGroupRegex', 'Headline1', 'Headline2',
            'Description', 'Path1', 'Path2']. OK to have different order
            and/or additional cols. Can include Headline3, Description2
            todo: have some validation logic to make sure all ads are of correct lengths and doesn't have invalid chars
        lp_lookup: dict that maps campaign and ad group Id to final url
            e.g. {(cm_id, ag_id): 'abc.com'}
            Note: use create_final_urls_lookup_from_df() to create this
                lookup
            Note: final urls should NOT be provided on the New_Ads tab
                since, for consistency, lp should be specified within
                LP_Logic tab instead
        label (optional): attach label to the paused ads, defaults True
        label_id: id of an existing label to be applied
        label_name: name of the NEW label to be applied, if not given,
            defaults to "Ymd - Created"

    Returns:
        list of responses from various operations

    Raises:
        Exception: df MUST contain ['CampaignRegex', 'AdGroupRegex', 'Headline1', 'Headline2',
            'Description', 'Path1', 'Path2']

    """
    try:
        new_ads_df[['CampaignRegex', 'AdGroupRegex', 'Headline1', 'Headline2',
                    'Description', 'Path1', 'Path2', 'Headline3', 'Description2']]
    except KeyError:
        raise Exception("new_ads_df MUST contain ['CampaignRegex', 'AdGroupRegex', 'Headline1', 'Headline2', "
                        "'Description', 'Path1', 'Path2', 'Headline3', 'Description2'] but optional "
                        "fields can be left blank if needed.")

    # 0. Clean up new_ads_df
    new_ads_df = new_ads_df.replace(to_replace=r'\n', value='', regex=True)

    # 1. get existing campaigns and ad groups, names and ids, needed for mapping
    cms_ags = get_all_but_removed_campaigns_ad_adgroups(client)

    # 2. "explode" new_ads_df by matching it against cms_ags, via regex.
    # Note: unlike with LP matching this one to many, i.e. the default ad does NOT need to be the last row but this
    # also implies that the default row is added to all ad groups
    def match_ads(ads_df, _cms_ags):
        outdfs = []
        for idx, row in ads_df.iterrows():
            # case-INSENSITIVE re.search
            matched = _cms_ags[(_cms_ags.CampaignName.str.contains(row.CampaignRegex, case=False))
                               & (_cms_ags.AdGroupName.str.contains(row.AdGroupRegex, case=False))]
            if matched.empty:
                continue
            matched.index = [idx] * len(matched)
            outdfs.append(matched)

        return ads_df.merge(pd.concat(outdfs), how='inner', left_index=True, right_index=True)

    new_ads_df = match_ads(new_ads_df, cms_ags)

    # 3. create ads ops from each row of the exploded df
    if 'Headline3' and 'Description2' in new_ads_df.columns:
        ad_ops = [create_eta_op(
            r.AdGroupId, r.Headline1, r.Headline2, r.Description,
            lp_lookup[(r.CampaignId, r.AdGroupId)],
            r.Path1, r.Path2, r.Headline3,
            r.Description2) for _, r in new_ads_df.iterrows()]
    elif 'Headline3' in new_ads_df.columns:
        ad_ops = [create_eta_op(
            r.AdGroupId, r.Headline1, r.Headline2, r.Description,
            lp_lookup[(r.CampaignId, r.AdGroupId)],
            r.Path1, r.Path2, r.Headline3) for _, r in new_ads_df.iterrows()]
    elif 'Description2' in new_ads_df.columns:
        ad_ops = [create_eta_op(
            r.AdGroupId, r.Headline1, r.Headline2, r.Description,
            lp_lookup[(r.CampaignId, r.AdGroupId)],
            r.Path1, r.Path2,
            description2=r.Description2) for _, r in new_ads_df.iterrows()]
    else:
        ad_ops = [create_eta_op(
            r.AdGroupId, r.Headline1, r.Headline2, r.Description,
            p_lookup[(r.CampaignId, r.AdGroupId)],
            r.Path1, r.Path2) for _, r in new_ads_df.iterrows()]

    # 4. mutate and store response
    responses = [client.GetService('AdGroupAdService').mutate(ad_ops)]

    # 5. if label, then need both new ad group ids and new ads ids
    if label:
        new_adgroup_ids = [v['adGroupId'] for v in responses[0]['value']]
        new_ad_ids = [v['ad']['id'] for v in responses[0]['value']]

        # 6. same labeling logic as from pausing ads, except for 'Paused' suffix
        # todo: refactor out labeling logic following DRY
        if not label_id:
            # create new label
            if not label_name:
                # create new label with default
                from datetime import datetime as dt
                label_name = f'{dt.today():%Y%m%d} - Created'
            responses.append(create_text_label(client, label_name))
            label_id = responses[-1]['value'][0]['id']

        responses.append(attach_label(client, label_id, 'AdGroupAdService', 'adId', new_ad_ids, new_adgroup_ids))

    return responses


def get_eta_performance(client):
    """
    Get ETA performance for a given account, aggregated by ad text,
    across all campaigns and ad groups for L30D
    Includes only: Status is ENABLED, Ad Network Type is SEARCH, and
    AdType is ETA

    Note: the structure of RSA suggests they should be pulled in a
    separate report.

    Args:
        client: adwords client

    Returns:
        Hierarchical dataframe is sorted by impression, desc, and ready be uploaded to sheets if needed.
        E.g. sheets.write_data_to_ranges(sheets_rw_credential, _sheet_id, {'Ads_L30D': ads_dfg.reset_index()})

    """
    import json

    class NumpyEncoder(json.JSONEncoder):
        def default(self, obj):
            if isinstance(obj, np.ndarray):
                return obj.tolist()
            return json.JSONEncoder.default(self, obj)

    cols = [
        'AccountDescriptiveName', 'AdGroupId', 'AdGroupName', 'AdGroupStatus',
        'AdNetworkType2', 'AdType', 'CampaignId', 'CampaignName',
        'CampaignStatus', 'Clicks', 'Conversions', 'Cost', 'CreativeFinalUrls',
        'Description', 'ExpandedTextAdDescription2', 'HeadlinePart1',
        'HeadlinePart2', 'ExpandedTextAdHeadlinePart3', 'Id', 'Impressions',
        'LabelIds', 'Labels', 'Path1', 'Path2', 'Status'
    ]

    sel = create_selector(
        cols,
        pred_fields=['Status', 'AdNetworkType2', 'AdType'],
        pred_operators=['EQUALS', 'EQUALS', 'EQUALS'],
        pred_values=['ENABLED', 'SEARCH', 'EXPANDED_TEXT_AD'])

    ads_df = get_report_as_df(
        client,
        selector=sel,
        report_type='AD_PERFORMANCE_REPORT',
        date_range_type='LAST_30_DAYS',
    )

    # hack to exclude draft campaigns from this
    # todo: check https://groups.google.com/forum/#!starred/adwords-api/TLhoo3HjcPA for non-hack solution
    ads_df = ads_df[~ads_df.CampaignName.str.startswith('T:')]

    ads_df.Labels = ads_df.Labels.str.replace(r'(\"|\[|\])', '')

    ads_df.LabelIds = ads_df.LabelIds.str.replace(r'(\"|\[|\])', '')

    ads_df = clean_adwords_df_from_api(
        ads_df,
        obj2num_cols=['Clicks', 'Conversions', 'Impressions'],
        money_cols=['Cost'],
        url_cols=['CreativeFinalUrls'])

    ads_groupby_cols = [
        'HeadlinePart1', 'HeadlinePart2', 'ExpandedTextAdHeadlinePart3',
        'Description', 'ExpandedTextAdDescription2', 'Path1', 'Path2',
        'CreativeFinalUrls'
    ]

    ads_df['CampaignRoot'] = ads_df.CampaignName.str.split(':', expand=True)[1]

    cm_ag_df = ads_df.groupby(ads_groupby_cols + ['CampaignRoot']).agg({
        'AdGroupName': lambda x: ', '.join(x.unique())}).reset_index()

    cm_ag_df['CampaignRootAdGroupNames'] = cm_ag_df.CampaignRoot + ' -> ' + cm_ag_df.AdGroupName

    cm_ag_dfg = cm_ag_df.groupby(ads_groupby_cols).agg({'CampaignRootAdGroupNames': lambda x: '\n '.join(x)})

    ads_dfg = (ads_df.groupby(ads_groupby_cols)
               .agg({'Impressions': np.sum,
                     'Clicks': np.sum,
                     'Conversions': np.sum,
                     'Cost': np.sum,
                     'Labels': lambda x: '|'.join(pd.unique(x)),
                     'Id': lambda x: '|'.join(x),
                     'AdGroupId': lambda x: '|'.join(x)
                     }))

    ads_dfg = ads_dfg.sort_values('Impressions', ascending=False)

    # https://stackoverflow.com/questions/26646362/numpy-array-is-not-json-serializable
    ads_dfg.Labels = ads_dfg.Labels.apply(lambda x: json.dumps(x, cls=NumpyEncoder))

    ads_dfg = ads_dfg.join(cm_ag_dfg, how='left')
    assert ads_dfg.notnull().all().all()

    return ads_dfg


def get_live_ad_count(client, adTypesList=None):
    """
    Checks for live ad groups that are missing ad copies. Will generate assertion error if there are any live ad
    groups that do not have ANY live ads. Otherwise, will output a dataframe with the data.

    Note: the "enabled" status in the UI only indicates OUR decision to run or pause/remove the ad. Would need to
    use CombinedApprovalStatus to check for ads that are enabled but not running due to policy violations.

    Args:
        client: adwords client
        adTypesList: filter for specific ad types IN list, or will default to ETA if left as None

    Returns:
        dataframe with ad copy count data for live ad groups

    """
    live_cms_ags = get_report_as_df(client, create_selector(
        cols_list=[
            'AdGroupId', 'AdGroupName', 'CampaignId', 'CampaignName',
            'CampaignStatus', 'AdGroupStatus'
        ],
        pred_fields=['AdGroupStatus', 'CampaignStatus'],
        pred_operators=['EQUALS', 'EQUALS'],
        pred_values=['ENABLED', 'ENABLED']),
                                    report_type='ADGROUP_PERFORMANCE_REPORT')

    # add in logic for ad type
    if not adTypesList:
        adTypesList = ['EXPANDED_TEXT_AD']

    # live must be both ENABLED and NOT disapproved
    sel = create_selector(
        cols_list=[
            'AdGroupId', 'AdGroupName', 'AdGroupStatus', 'AdNetworkType2',
            'AdType', 'CampaignId', 'CampaignName', 'CampaignStatus', 'Clicks',
            'Conversions', 'Cost', 'Id', 'Impressions', 'Labels', 'Status',
            'CombinedApprovalStatus'
        ],
        pred_fields=['Status', 'AdNetworkType2', 'CombinedApprovalStatus', 'AdType'],
        pred_operators=['EQUALS', 'EQUALS', 'NOT_IN', 'IN'],
        pred_values=['ENABLED', 'SEARCH', ['DISAPPROVED', 'SITE_SUSPENDED'], adTypesList])

    ads_df = get_report_as_df(client,
                              selector=sel,
                              report_type='AD_PERFORMANCE_REPORT',
                              date_range_type='LAST_30_DAYS',
                              include_zero_impressions=True
                              )
    ads_df.Labels = ads_df.Labels.str.replace(r'(\"|\[|\])', '')
    ads_df = clean_adwords_df_from_api(ads_df,
                                       obj2num_cols=['Clicks', 'Conversions', 'Impressions'],
                                       money_cols=['Cost'])
    ads_df['NumEvergreenAds'] = ads_df['Labels'].str.contains('Default')
    ads_dfg = ads_df.groupby(['CampaignName', 'CampaignId', 'AdGroupName', 'AdGroupId']).agg({'Id': np.count_nonzero,
                                                                                              'NumEvergreenAds': np.sum
                                                                                              })
    ads_dfg = ads_dfg.rename(columns={'Id': 'NumLiveAds'})
    ads_dfg['NumNonEvergreenAds'] = ads_dfg.NumLiveAds - ads_dfg.NumEvergreenAds

    cms_ags_dfg = live_cms_ags.groupby(['CampaignName', 'CampaignId', 'AdGroupName', 'AdGroupId']).count()
    cms_ags_dfg = cms_ags_dfg.merge(ads_dfg, how='left', left_index=True, right_index=True, indicator=True)

    # noinspection PyProtectedMember
    assert all(cms_ags_dfg._merge == 'both')

    return cms_ags_dfg.sort_values('NumLiveAds')


def create_final_urls_lookup_from_df(client, lp_regex_df, default_url):
    """
    Creates a dict that maps ALL BUT REMOVED campaigns and ad groups to a single final url, using regex logic in the
    input df, usually from sheets. Note: unlike create_eta_ads_from_df, the mapping is 1-to-1 and therefore
    RETURNS THE FIRST MATCH, I.E. ORDERING MATTERS

    Args:
        client: adwords client for the given account
        lp_regex_df: dataframe with mapping logic. Must contain [CampaignRegex, AdGroupRegex, FinalURL] cols
            Note: since mapping is 1-to-1, first match will be returned and so the df should go from most granular to
            most broad
            Note: last row of df SHOULD match all, i.e. .* for both CampaignRegex and AdGroupRegex but if NOT provided
            then will add and map to default_url
        default_url: default url should nothing match but THIS SHOULD NOT HAPPEN (could be used if the catch all regex
        was omitted in the lp logic)

    Returns:
        dict that maps all campaigns and ad groups from the client to a final url, by IDs.
            I.e. {(campaignId, adgroupId): finalURL}

    Raises:
        Exception: lp_regex_df MUST contain ['CampaignRegex', 'AdGroupRegex', 'FinalURL']

    """
    try:
        lp_regex_df[['CampaignRegex', 'AdGroupRegex', 'FinalURL']]
    except KeyError:
        raise Exception("lp_regex_df MUST contain ['CampaignRegex', 'AdGroupRegex', 'FinalURL']")

    lp_regex_df = lp_regex_df.replace(to_replace=r'\n', value='', regex=True)

    # check for catch-all regex, if missing then append
    if lp_regex_df[(lp_regex_df.CampaignRegex == '.*') & (lp_regex_df.AdGroupRegex == '.*')].empty:
        lp_regex_df = lp_regex_df.append({'CampaignRegex': '.*', 'AdGroupRegex': '.*', 'FinalURL': default_url},
                                         ignore_index=True)

    cms_ags = get_all_but_removed_campaigns_ad_adgroups(client)

    def return_first_matching_lp(cms_ags_row):
        for i, v in lp_regex_df.iterrows():
            if ((re.search(v.CampaignRegex, cms_ags_row.CampaignName, flags=re.I)) and
                    (re.search(v.AdGroupRegex, cms_ags_row.AdGroupName, flags=re.I))):
                return v.FinalURL
        return default_url

    cms_ags['FinalURL'] = cms_ags.apply(return_first_matching_lp, axis=1)

    lkup = cms_ags.groupby(['CampaignId', 'AdGroupId'])['FinalURL'].first().to_dict()

    assert len(lkup) == len(cms_ags)

    return lkup


def update_kws_final_url_from_df(client, lp_lookup, label=True, label_id=None,
                                 label_name=None, only_if_blank_url=False):
    """
    updates final urls for ALL EXCEPT REMOVED keywords in the account (does NOT filter out paused)
    Optionally set only_if_blank_url to True to only update those kws currently WITHOUT urls, e.g. in case of new kws
    Note: seems like the current API will NOT update if final url update is the same as the existing urlm, in changelog

    Args:
        client: adwords client of the given account
        lp_lookup: dict that maps campaign and ad group Id to final url e.g. {(cm_id, ag_id): 'abc.com'}
            Note: use create_final_urls_lookup_from_df() to create this lookup
        label: optionally attach label to the updated kws, defaults to True
        label_id: id of an existing label to be applied
        label_name: name of the NEW label to be applied, if not given, defaults to "Ymd - FinalURL Updated"
        only_if_blank_url: only update and label kws currenlty WITHOUT final urls, defaults to False
            so all will be updated and labeled (though from the changelog it seems that the current API implementation
            will only update if final url is different from existing one)

    Returns:
        list of responses from various operations

    """
    # get all kws, except removed
    sel = create_selector(cols_list=[
        'AdGroupId', 'AdGroupName', 'CampaignId', 'CampaignName', 'Criteria',
        'FinalUrls', 'Id', 'KeywordMatchType', 'Clicks'], pred_fields=['CampaignStatus', 'AdGroupStatus'],
        pred_operators=['NOT_EQUALS', 'NOT_EQUALS'],
        pred_values=['REMOVED', 'REMOVED'])

    if only_if_blank_url:
        sel['predicates'].append({
            'field': 'FinalUrls',
            'operator': 'EQUALS',
            'values': ''
        })

    kws_df = get_report_as_df(client, sel, report_type='KEYWORDS_PERFORMANCE_REPORT')

    # create ops
    def update_keyword_urls(row):
        op = {
            'operand': {
                'xsi_type': 'BiddableAdGroupCriterion',
                'adGroupId': row.AdGroupId,
                'criterion': {
                    'id': row.Id
                },
                'finalUrls': {
                    'urls': [lp_lookup[(row.CampaignId, row.AdGroupId)]]
                }
            },
            'operator': 'SET'
        }

        return op

    ops = [update_keyword_urls(r) for _, r in kws_df.iterrows()]

    if len(ops) == 0:
        print('No updates required.')
        return

    # mutate
    responses = [client.GetService('AdGroupCriterionService', version=API_VERSION).mutate(ops)]

    # label
    if label:
        adgroup_ids = kws_df.AdGroupId.tolist()
        kw_ids = kws_df.Id.tolist()

        # todo: refactor out labeling logic following DRY
        if not label_id:
            # create new label
            if not label_name:
                # create new label with default
                from datetime import datetime as dt
                label_name = f'{dt.today():%Y%m%d} - KW FinalURL Updated'
            responses.append(create_text_label(client, label_name))
            label_id = responses[-1]['value'][0]['id']

        responses.append(attach_label(client, label_id, 'AdGroupCriterionService', 'criterionId', kw_ids, adgroup_ids))

    return responses


def batch_update_kws_final_url_from_df(client, lp_lookup, label=True, label_id=None, label_name=None):
    """
    Batch update final urls for ALL EXCEPT REMOVED keywords in the account (does NOT filter out paused)
    Needed due to 5000 ops limit per request by the API.

    Args:
        client: adwords client of the given account
        lp_lookup: dict that maps campaign and ad group Id to final url e.g. {(cm_id, ag_id): 'abc.com'}
            Note: use create_final_urls_lookup_from_df() to create this lookup
        label: optionally attach label to the updated kws, defaults to True
        label_id: id of an existing label to be applied
        label_name: name of the NEW label to be applied, if not given, defaults to "Ymd - FinalURL Updated"

    Returns:
        both the batch job helper (bjh) and the response from the batch job

    """
    # get all kws, except removed
    sel = create_selector(cols_list=[
        'AdGroupId', 'AdGroupName', 'CampaignId', 'CampaignName', 'Criteria',
        'FinalUrls', 'Id', 'KeywordMatchType', 'Clicks'], pred_fields=['CampaignStatus', 'AdGroupStatus'],
        pred_operators=['NOT_EQUALS', 'NOT_EQUALS'],
        pred_values=['REMOVED', 'REMOVED'])

    kws_df = get_report_as_df(client, sel, report_type='KEYWORDS_PERFORMANCE_REPORT')

    # create ops
    def update_keyword_urls(row):
        op = {
            'xsi_type': 'AdGroupCriterionOperation',  # need to specify xsi_type for batch jobs
            'operand': {
                'xsi_type': 'BiddableAdGroupCriterion',
                'adGroupId': row.AdGroupId,
                'criterion': {
                    'id': row.Id
                },
                'finalUrls': {
                    'urls': [lp_lookup[(row.CampaignId, row.AdGroupId)]]
                }
            },
            'operator': 'SET'
        }

        return op

    kw_ops = [update_keyword_urls(r) for _, r in kws_df.iterrows()]

    # label
    label_ops = []
    if label:
        adgroup_ids = kws_df.AdGroupId.tolist()
        kw_ids = kws_df.Id.tolist()

        # todo: refactor out labeling logic following DRY
        if not label_id:
            # create new label
            if not label_name:
                # create new label with default
                from datetime import datetime as dt
                label_name = f'{dt.today():%Y%m%d} - KW FinalURL Updated'
            label_id = (create_text_label(client, label_name))['value'][0]['id']

        # https://developers.google.com/adwords/api/docs/reference/v201809/AdGroupCriterionService.AdGroupCriterionLabelOperation
        label_ops = [{'xsi_type': 'AdGroupCriterionLabelOperation',
                      'operator': 'ADD',
                      'operand': {'adGroupId': agid, 'criterionId': kwid, 'labelId': label_id}} for
                     agid, kwid in zip(adgroup_ids, kw_ids)]

    # batch
    bjh = client.GetBatchJobHelper(version=API_VERSION)
    # Create a BatchJob.
    batch_job = add_batch_job(client)

    # Retrieve the URL used to upload the BatchJob operations.
    upload_url = batch_job['uploadUrl']['url']
    batch_job_id = batch_job['id']
    print('Created BatchJob with ID "%d", status "%s", and upload URL "%s"' %
          (batch_job['id'], batch_job['status'], upload_url))

    # Upload operations.
    bjh.UploadOperations(upload_url, kw_ops, label_ops)

    # Download and display results.
    download_url = get_batch_job_download_url(client, batch_job_id)
    response = request.urlopen(download_url).read()

    print_errors(bjh, response)

    return bjh, response


# Todo: commented out for now as there's no way to update ONLY the url for ads. Also triggers review (v201806)
# def update_ad_final_url_from_df(client, lp_lookup, label=True, label_id=None, label_name=None):
#     """
#     updates final urls for ALL EXCEPT REMOVED ad copies in the account (does NOT filter out paused).
#     WILL TRIGGER REVIEW
#
#     Note: as of v201806, which is the first time ads can be updated, headline1, 2,  desc are required. More @
#     https://groups.google.com/forum/#!searchin/adwords-api/adservice%7Csort:date/adwords-api/QnphGP6B2fs/IvtcfazHBAAJ
#
#     Args:
#         client: adwords client of the given account
#         lp_lookup: dict that maps campaign and ad group Id to final url e.g. {(cm_id, ag_id): 'abc.com'}
#             Note: use create_final_urls_lookup_from_df() to create this lookup
#         label: optionally attach label to the paused ads, defaults to True
#         label_id: id of an existing label to be applied
#         label_name: name of the NEW label to be applied, if not given, defaults to "Ymd - FinalURL Updated"
#
#     Returns:
#         list of responses from various operations
#
#     """
#     # get all ads, except removed
#     sel = create_selector(cols_list=['AdGroupId', 'AdGroupName', 'CampaignId', 'CampaignName',
#                                      'HeadlinePart1', 'HeadlinePart2', 'Description',
#                                      'CreativeFinalUrls', 'Id'],
#                           pred_fields=['CampaignStatus', 'AdGroupStatus', 'Status', 'AdType'],
#                           pred_operators=['NOT_EQUALS', 'NOT_EQUALS', 'NOT_EQUALS', 'EQUALS'],
#                           pred_values=['REMOVED', 'REMOVED', 'DISABLED', 'EXPANDED_TEXT_AD'])
#
#     ads_df = get_report_as_df(client, sel, report_type='AD_PERFORMANCE_REPORT')
#
#     def _trim_double_quotes(s):
#         # needed because report will sometimes enclose text in quotes
#         if (s[0] == '"') and (s[-1] == '"'):
#             return s[1:-1]
#         else:
#             return s
#
#     # create ops
#     def update_ad_urls(row):
#         op = {'xsi_type': 'AdOperation',
#               'operand': {
#                   'xsi_type': 'ExpandedTextAd',
#                   'id': row.Id,
#                   # just pass in original h1, h2, and desc, UNCHANGED
#                   'headlinePart1': _trim_double_quotes(row.HeadlinePart1).replace('""', '"'),
#                   'headlinePart2': _trim_double_quotes(row.HeadlinePart2).replace('""', '"'),
#                   'description': _trim_double_quotes(row.Description).replace('""', '"'),
#                   'finalUrls': [lp_lookup[(row.CampaignId, row.AdGroupId)]],
#               },
#               'operator': 'SET'}
#
#         return op
#
#     ops = [update_ad_urls(r) for _, r in ads_df.iterrows()]
#
#     # mutate, need to use AdService rather than AdGroupAdService to edit existing ads
#     responses = [client.GetService('AdService', version=API_VERSION).mutate(ops)]
#
#     # label
#     if label:
#         adgroup_ids = ads_df.AdGroupId.tolist()
#         ad_ids = ads_df.Id.tolist()
#
#         # todo: refactor out labeling logic following DRY
#         if not label_id:
#             # create new label
#             if not label_name:
#                 # create new label with default
#                 from datetime import datetime as dt
#                 label_name = f'{dt.today():%Y%m%d} - Ad FinalURL Updated'
#             responses.append(create_text_label(client, label_name))
#             label_id = responses[-1]['value'][0]['id']
#
#         responses.append(attach_label(client, label_id, 'AdGroupAdService', 'adId', ad_ids, adgroup_ids))
#
#     return responses


def check_ads_kws_final_urls_alignment(client):
    """
    Checks to make sure ENABLED ad copies and keywords within the same campaign and ad group have the same final urls.

    Args:
        client: adwords client of the given account

    Returns:
        tuple of whether all urls match, and dataframe of mismatched urls

    """
    sel = create_selector(
        cols_list=[
            'AdGroupId', 'AdGroupName', 'CampaignId', 'CampaignName', 'Criteria',
            'FinalUrls', 'Id'
        ],
        pred_fields=[
            'Status', 'CampaignStatus', 'AdGroupStatus', 'ApprovalStatus'
        ],
        pred_operators=['EQUALS', 'EQUALS', 'EQUALS', 'NOT_EQUALS'],
        pred_values=['ENABLED', 'ENABLED', 'ENABLED', 'DISAPPROVED'])

    kw_urls = get_report_as_df(client, sel, report_type='KEYWORDS_PERFORMANCE_REPORT')

    assert all(kw_urls.groupby(['CampaignName', 'AdGroupName']).FinalUrls.nunique() == 1)

    kw_urls_dfg = kw_urls.groupby(['CampaignName', 'AdGroupName']).FinalUrls.first()

    sel = create_selector(
        cols_list=[
            'AdGroupId', 'AdGroupName', 'CampaignId', 'CampaignName',
            'CreativeFinalUrls', 'Id'
        ],
        pred_fields=[
            'Status', 'CampaignStatus', 'AdGroupStatus', 'CombinedApprovalStatus'
        ],
        pred_operators=['EQUALS', 'EQUALS', 'EQUALS', 'NOT_EQUALS'],
        pred_values=['ENABLED', 'ENABLED', 'ENABLED', 'DISAPPROVED'])

    ad_urls = get_report_as_df(client, sel, report_type='AD_PERFORMANCE_REPORT')

    assert all(ad_urls.groupby(['CampaignName', 'AdGroupName']).CreativeFinalUrls.nunique() == 1)

    ad_urls_dfg = ad_urls.groupby(['CampaignName', 'AdGroupName']).CreativeFinalUrls.first()

    comp = pd.concat([kw_urls_dfg, ad_urls_dfg], axis=1)

    comp['UrlsMatch'] = comp.FinalUrls == comp.CreativeFinalUrls

    return all(comp.UrlsMatch), comp[~comp.UrlsMatch]


def get_targeting_ideas_for_urls(client, langs, locations, urls,
                                 cats_lookup,
                                 path_to_service_key,
                                 bq_project_id_str,
                                 bq_table,
                                 output_dir=None):
    """
    Get keyword ideas from adwords for each landing page URL
    Note: returned data columns is fixed as below since this will need to align with the schema in BigQuery table
    Note: also no need to create table ahead of time in BQ as otherwise would return schema misalignment issues

    Args:
        client: adwords client
        langs: list of languages in the form of str ids, e.g. ['1000'] for English
            https://developers.google.com/adwords/api/docs/appendix/languagecodes
        locations: list of geos in the form of str ids, e.g. ['2702'] for Singapore
            https://developers.google.com/adwords/api/docs/appendix/geotargeting
        urls: list of landing page urls
        cats_lookup: dict of category Ids, of type INT, to category hierarchy names
        path_to_service_key: service key used for BigQuery project
        bq_project_id_str: BigQuery project id as str
        bq_table: name of BigQuery <dataset>.<table_name>
        output_dir: optional local storage location for pickling
            Note: file_root will just use bq_table name

    Returns:
        Pandas Dataframe containing results for the given URLs

    """
    dfs_list = []

    # get everything that is available to get
    cols = [
        'KEYWORD_TEXT', 'SEARCH_VOLUME', 'CATEGORY_PRODUCTS_AND_SERVICES',
        'COMPETITION', 'EXTRACTED_FROM_WEBPAGE', 'IDEA_TYPE', 'AVERAGE_CPC', 'TARGETED_MONTHLY_SEARCHES']

    for url in urls:
        sel = dict(ideaType='KEYWORD', requestType='IDEAS', requestedAttributeTypes=cols,
                   searchParameters=[{'xsi_type': 'RelatedToUrlSearchParameter', 'urls': [url]},
                                     dict(xsi_type='NetworkSearchParameter', networkSetting={
                                         'targetGoogleSearch': True,
                                         'targetSearchNetwork': False,
                                         'targetContentNetwork': False,
                                         'targetPartnerSearchNetwork': False
                                     })
                                     ])

        for lang in langs:
            sel['searchParameters'].append(
                dict(xsi_type='LanguageSearchParameter', languages=[{'id': lang}])
            )

        for loc in locations:
            sel['searchParameters'].append(
                dict(xsi_type='LocationSearchParameter', locations=[{'id': loc}])
            )

        retries = 0

        while retries < 3:
            try:
                output_dict = {}
                for c in cols:  # these are the output columns
                    output_dict[c] = []

                svc = client.GetService('TargetingIdeaService')

                offset = 0
                _pg_size = 99

                sel['paging'] = dict(numberResults=str(_pg_size), startIndex=str(offset))

                more_pages = True

                while more_pages:

                    page = svc.get(sel)

                    for e in page['entries']:
                        for d in e['data']:
                            k = d['key']
                            v = d['value']['value']
                            output_dict[k].append(v)

                    offset += _pg_size
                    sel['paging']['startIndex'] = str(offset)
                    more_pages = offset < int(page['totalNumEntries'])
            except GoogleAdsError as e:
                print(e)
                time.sleep(30)
                retries += 1
            else:  # no exception, no need to retry
                break

        outputdf = pd.DataFrame(output_dict)

        outputdf['URL'] = url
        outputdf['relevance'] = outputdf.index + 1
        outputdf['languages'] = ', '.join(langs)
        outputdf['locations'] = ', '.join(locations)

        # expand out categories
        outputdf['categories'] = outputdf.CATEGORY_PRODUCTS_AND_SERVICES.apply(lambda x:
                                                                               [cats_lookup.get(i, '') for i in x])

        dfs_list.append(outputdf)

    df = pd.concat(dfs_list, ignore_index=True, sort=False)

    df['Ymd'] = dt.today().strftime('%Y%m%d')

    # OPTIONALLY: pickle to file after done with all queries
    file_root = bq_table
    if output_dir:
        with open(
                f'{output_dir}/Chunk_{file_root}_{int(dt.timestamp(dt.now()))}',
                'wb') as f:
            pickle.dump(df, f)

    # Stream insert into BigQuery

    bq_credentials = service_account.Credentials.from_service_account_file(path_to_service_key)

    df = df.fillna('')

    df.to_gbq(
        project_id=bq_project_id_str,
        destination_table=bq_table,
        if_exists='append', credentials=bq_credentials)

    return df


def debug():
    from blackbox.ytilities import googlesheets as sheets

    client = get_adwords_client(is_prod=True)
    client.SetClientCustomerId(7028184786)  # Google:S:SG:xx:CC

    _sheet_id = '1bW5NaenJZ285l4fgHk-1XTGSnVk-dWXdZCcwfcaKKeo'
    sheets_rw_credential = sheets.get_credentials_for_sheets(
        scopes='https://www.googleapis.com/auth/spreadsheets',
        credential_file='sheets.googleapis.com-python-CAG-rw.json')

    prod_lps = sheets.get_data_from_range(sheets_rw_credential, _sheet_id, range_name='LP_Logic_CC')

    lp_lkup = create_final_urls_lookup_from_df(client, prod_lps,
                                               default_url='https://www.singsaver.com.sg/'
                                                           'credit-card/best-deals/results')

    ads_df = sheets.get_data_from_range(sheets_rw_credential, _sheet_id, range_name='New_Ads_CC')

    responses = create_eta_ads_from_df(client, ads_df, lp_lkup)
    # responses = update_ad_final_url_from_df(client, lp_lkup)

    return responses


if __name__ == "__main__":
    debug()
