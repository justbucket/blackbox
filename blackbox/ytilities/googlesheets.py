#!/usr/bin/env python
"""Functions to work with Google Sheets API.
"""

import os
import argparse

from apiclient import discovery
from google.oauth2 import service_account
import oauth2client as o2c
import pandas as pd
import numpy as np

SCOPE_READONLY = 'https://www.googleapis.com/auth/spreadsheets.readonly'
SCOPE_RW = 'https://www.googleapis.com/auth/spreadsheets'
CLIENT_SECRET_FILE = 'client_secret.json'
APPLICATION_NAME = 'Google Sheets via Python API'

FLAGS = argparse.Namespace(auth_host_name='localhost',
                           auth_host_port=[8080, 8090],
                           logging_level='ERROR',
                           noauth_local_webserver=False)

TEST_SHEET_ID = '1_jeiE28qGvrTZxN6xQ6ZPuw16MhL9esQiHqvuGhQAbY'  # Klook: 20190831 - Sheets API Test Sheet
CREDENTIAL_FILE = 'sheets.googleapis.com-python-read-only.json'
CREDENTIAL_FILE_RW = 'sheets.googleapis.com-python-rw.json'
SERVICE_ACCOUNT_FILE = 'google_api_service_account.json'


def get_credentials_for_sheets(flags=FLAGS, scopes=SCOPE_READONLY, client_secret_file=CLIENT_SECRET_FILE,
                               application_name=APPLICATION_NAME, credential_file=CREDENTIAL_FILE,
                               alternate_home_dir=None
                               ):
    """Gets valid user credentials from storage.
    MOST OF THE TIME SHOULD ONLY NEED TO SET THE SCOPES PARAMETER AND NOT THE CREDENTIAL FILE

    If nothing has been stored, or if the stored credentials are invalid,
    the OAuth2 flow is completed to obtain the new credentials.

    Returns:
        Credentials, the obtained credential.
    """
    if alternate_home_dir:
        home_dir = alternate_home_dir
    else:
        home_dir = os.path.expanduser('~')

    credential_dir = os.path.join(home_dir, '.credentials')

    # change the credential_file based on scope
    if scopes == SCOPE_RW:
        credential_file = CREDENTIAL_FILE_RW

    if not os.path.exists(credential_dir):
        os.makedirs(credential_dir)
    credential_path = os.path.join(credential_dir, credential_file)

    store = o2c.file.Storage(credential_path)
    credentials = store.get()
    if not credentials or credentials.invalid:
        flow = o2c.client.flow_from_clientsecrets(os.path.join(credential_dir, client_secret_file), scopes)
        flow.user_agent = application_name
        credentials = o2c.tools.run_flow(flow, store, flags)
        print('Storing credentials to ' + credential_path)
    return credentials


def get_credentials_for_sheets_service_account(svc_acct_file=SERVICE_ACCOUNT_FILE, scopes=SCOPE_READONLY,
                                               alternate_home_dir=None
                                               ):
    """Get valid credentials from storage for service account

    Returns:
        Credentials
    """
    if alternate_home_dir:
        home_dir = alternate_home_dir
    else:
        home_dir = os.path.expanduser('~')

    credential_dir = os.path.join(home_dir, '.credentials')
    svc_acct_file_path = os.path.join(credential_dir, svc_acct_file)

    return service_account.Credentials.from_service_account_file(svc_acct_file_path, scopes=scopes)


def get_sheet_title_id_from_spreadsheet(credentials, spreadsheet_id, incl_hidden=True):
    service = discovery.build('sheets', 'v4', credentials=credentials)
    result = service.spreadsheets().get(spreadsheetId=spreadsheet_id).execute()

    shts = result['sheets']

    shts_info = []

    for sh in shts:
        props = sh['properties']

        if incl_hidden or (not props.get('hidden')):
            shts_info.append((props['title'], props['sheetId']))

    return shts_info


def get_data_from_range(credentials,
                        sheet_id=TEST_SHEET_ID,
                        range_name='TestTab',
                        trim_strs=True):
    print("Using new version ....")
    # https://github.com/googleapis/google-api-python-client/issues/299#issuecomment-1672301512
    service = discovery.build('sheets', 'v4', credentials=credentials, cache_discovery=False)
    result = service.spreadsheets().values().get(spreadsheetId=sheet_id,
                                                 range=range_name).execute()
    result = result.get('values', [])

    df = pd.DataFrame(data=result[1:], columns=result[0])

    if trim_strs:
        print('Trimming all cells, might be slow for large frames. Can disable with trim_str=False.')
        df = df.applymap(lambda x: x.strip() if type(x) is str else x)

    return df


def get_range_data_threaded(credentials, sheet_ids, range_names, thread_cnt=4, trim_strs=True):
    from concurrent.futures import ThreadPoolExecutor, as_completed

    if len(sheet_ids) != len(range_names):
        print('Number of sheets must match number of ranges.')
        return
    else:
        sh_rng_pairs = zip(sheet_ids, range_names)
        dfs = []

    with ThreadPoolExecutor(max_workers=thread_cnt) as ex:
        jobs = {ex.submit(get_data_from_range, credentials, *pair, trim_strs): pair for pair in sh_rng_pairs}
        for f in as_completed(jobs):
            sh, rng = jobs[f]  # for debugging

            try:
                df = f.result()
            except Exception as exc:
                print('(%s, %s) generated an exception: %s' % (sh, rng, exc))
            else:
                dfs.append(df)

    return pd.concat(dfs)


def write_data_to_ranges(credentials, sheet_id, data_dict, clear_first=True):
    update_service = discovery.build('sheets', 'v4', credentials=credentials)

    data = []
    responses = {}

    for rng_name, data_df in data_dict.items():
        values = [data_df.columns.values.tolist()]
        values.extend(data_df.values.tolist())
        data.append({'range': rng_name, 'values': values})

    contents = {'value_input_option': 'RAW', 'data': data}

    if clear_first:
        responses['batchClear'] = (update_service.spreadsheets().values()
                                   .batchClear(spreadsheetId=sheet_id, body={'ranges': [*data_dict]})
                                   .execute())

    responses['batchUpdate'] = update_service.spreadsheets().values().batchUpdate(spreadsheetId=sheet_id,
                                                                                  body=contents).execute()

    return responses


def create_new_tabs(credentials, sheet_id, tab_names):
    """
    Creates new tabs with names given in tab_names. Will generate exception if name already exists.
    Args:
        credentials: credentials for the given sheet, with write scope
        sheet_id: id of the sheet
        tab_names: list of valid names for the new tabs

    Returns:
        Response from tab creation if successful

    """

    update_service = discovery.build('sheets', 'v4', credentials=credentials)

    body = {'requests': [{"addSheet": {"properties": {"title": tn}}} for tn in tab_names]}

    response = update_service.spreadsheets().batchUpdate(spreadsheetId=sheet_id, body=body).execute()

    return response


def clear_rows(credentials, sheet_id, range_name=None, sheet_name=None,
               sheet_range=None, verbose=False):
    """ Gets rows from a spreadsheet and returns a DataFrame. Either
    range_name or sheet_name + sheet_range should be passed in.

    Args:
        credentials: valid credentials with access to file and write
            scope
        sheet_id: the id for the sheet, from url: e.g.,
            https://docs.google.com/spreadsheets/d/1AEUuw_Nbm8f1iF1PKRfjjZ2xPmfr1bCBcRLGtnNwoUg/edit#gid=0
            this part:  1AEUuw_Nbm8f1iF1PKRfjjZ2xPmfr1bCBcRLGtnNwoUg
        range_name (optional): combined name and range for Sheet, e.g.,
            Sheet1!A1:B6
        sheet_name (optional): the name of Google Sheet (default is Sheet1)
        sheet_range (optional): the range of cells to get
            e.g.,   A1:H is all rows from columns A through H
                    B4:E6 is columns B through E, rows 4, 5, and 6
                    or use a named range: e.g., test_range
        verbose (optional): False (default) will return only basic info
            from job, True will return complete info, None will return
            nothing (unless unexpected values are found)

    """

    service = discovery.build('sheets', 'v4', credentials=credentials)

    if range_name:
        pass
    elif sheet_name and sheet_range:
        # The A1 notation of the values to clear.
        # sheet_range needs to be in format like 'Sheet1!A1:B2'
        range_name = '{}!{}'.format(sheet_name, sheet_range)
    else:
        raise ValueError(
            'need values for range_name or sheet_name and sheet_range'
        )

    clear_values_request_body = {

    }

    request = service.spreadsheets().values().clear(
        spreadsheetId=sheet_id,
        range=range_name,
        body=clear_values_request_body)
    response = request.execute()

    if verbose is None and set(response.keys()) == {'spreadsheetId',
                                                    'clearedRange'}:
        return
    elif not verbose and set(response.keys()) == {'spreadsheetId',
                                                  'clearedRange'}:
        return '{} clearedRange: {}'.format(response['spreadsheetId'],
                                            response['clearedRange'])
    else:
        return response


def get_rows(credentials, sheet_id, range_name=None, sheet_name=None,
             sheet_range=None, verbose=False):
    """ Gets rows from a spreadsheet and returns a list of lists.
    Either range_name or sheet_name + sheet_range should be passed in.

    Args:
        credentials: valid credentials with access to file and write
            scope
        sheet_id: the id for the sheet, from url: e.g.,
            https://docs.google.com/spreadsheets/d/1AEUuw_Nbm8f1iF1PKRfjjZ2xPmfr1bCBcRLGtnNwoUg/edit#gid=0
            this part:  1AEUuw_Nbm8f1iF1PKRfjjZ2xPmfr1bCBcRLGtnNwoUg
        range_name (optional): combined name and range for Sheet, e.g.,
            Sheet1!A1:B6
        sheet_name (optional): the name of Google Sheet (default is Sheet1)
        sheet_range (optional): the range of cells to get
            e.g.,   A1:H is all rows from columns A through H
                    B4:E6 is columns B through E, rows 4, 5, and 6
                    or use a named range: e.g., test_range
        verbose (optional): False (default) will return only basic info
            from job, True will return complete info

    Returns:
        values: a list of list containing retrieved data, if any
    """

    service = discovery.build('sheets', 'v4', credentials=credentials)

    if range_name:
        pass
    elif sheet_name and sheet_range:
        # The A1 notation of the values to clear.
        # sheet_range needs to be in format like 'Sheet1!A1:B2'
        range_name = '{}!{}'.format(sheet_name, sheet_range)
    else:
        raise ValueError(
            'need values for range_name or sheet_name and sheet_range'
        )

    # this gets the columns and rows of sheet_range
    result = service.spreadsheets().values().get(
        spreadsheetId=sheet_id,
        range=range_name).execute()

    if not verbose and result.get('values'):
        return result.get('values')
    else:
        return result


def update_rows(values, credentials, sheet_id, range_name=None, sheet_name=None,
                sheet_range=None, dimension='ROWS',
                value_input_option='USER_ENTERED', verbose=False):
    """ Updates a range of rows (or columns) in a sheet
    Either range_name or sheet_name + sheet_range should be passed in.

    Args:
        values: list of lists (e.g, [[1,2]] or [[1,2],[3,4]])
        credentials: valid credentials with access to file and write
            scope
        sheet_id: the id for the sheet, from url: e.g.,
            https://docs.google.com/spreadsheets/d/1AEUuw_Nbm8f1iF1PKRfjjZ2xPmfr1bCBcRLGtnNwoUg/edit#gid=0
            this part:  1AEUuw_Nbm8f1iF1PKRfjjZ2xPmfr1bCBcRLGtnNwoUg
        range_name (optional): combined name and range for Sheet, e.g.,
            Sheet1!A1:B6
        sheet_name (optional): the name of Google Sheet (default is Sheet1)
        sheet_range (optional): the range of cells to get
            e.g.,   A1:H is all rows from columns A through H
                    B4:E6 is columns B through E, rows 4, 5, and 6
                    or use a named range: e.g., test_range
        dimension (optional): 'ROWS' (default) or 'COLUMNS'
        value_input_option (optional): 'USER_ENTERED' (default) or 'RAW'
        verbose (optional): False (default) will return only basic info
            from job, True will return complete info, None will return
            nothing (unless unexpected values are found)
    Returns:

    """

    service = discovery.build('sheets', 'v4', credentials=credentials)

    if range_name:
        pass
    elif sheet_name and sheet_range:
        # The A1 notation of the values to clear.
        # sheet_range needs to be in format like 'Sheet1!A1:B2'
        range_name = '{}!{}'.format(sheet_name, sheet_range)
    else:
        raise ValueError(
            'need values for range_name or sheet_name and sheet_range'
        )

    # How the input data should be interpreted. 'USER_ENTERED' or 'RAW'
    if not value_input_option in ['USER_ENTERED', 'RAW']:
        raise ValueError(
            'value_input_option must be \'USER_ENTERED\' or \'RAW\'')

    value_range_body = {
        "range": range_name,
        "majorDimension": dimension,
        "values": values
    }

    request = service.spreadsheets().values().update(
        spreadsheetId=sheet_id,
        range=range_name,
        valueInputOption=value_input_option,
        body=value_range_body)
    response = request.execute()

    if verbose is None and set(response.keys()) == {'spreadsheetId',
                                                    'updatedCells',
                                                    'updatedColumns',
                                                    'updatedRange',
                                                    'updatedRows'}:
        return
    elif not verbose and set(response.keys()) == {'spreadsheetId',
                                                  'updatedCells',
                                                  'updatedColumns',
                                                  'updatedRange',
                                                  'updatedRows'}:
        return '{} updatedRange: {}'.format(response['spreadsheetId'],
                                            response['updatedRange'])
    else:
        return response


def column_tools(df, date_cols=None, num_cols=None, rename_cols=None,
                 dt_errors='coerce', num_errors='coerce'):
    """ Perform functions on Pandas DataFrame columns to prepare for use
    by converting date string columns to datetime, number string columns
    by removing non-numerical symbols and converting to numeric, and
    renaming columns.

    Args:
        df: a DataFrame to be passed in
        date_cols (optional): a list of columns to convert to dates
        num_cols (optional): a list of columns to strip out non-numeric
            symbols ($,%) and convert to numeric
        rename_cols (optional): a dict of columns to rename
            e.g., {'ymd': 'Date'})
        dt_errors (optional): how to handle non-dates found in date
            column
                coerce (default) - convert to NaT
                ignore - keep as-is
                raise - raise an exception
        num_errors (optional): how to handle non-numbers found in number
                column:
                    coerce (default) - convert to NaN
                    ignore - keep as-is
                    raise - raise an exception
    Returns:
        df: the processed DataFrame
    """

    try:
        if date_cols is not None:
            for date_col in date_cols:
                df[date_col] = pd.to_datetime(df[date_col], errors=dt_errors)

        if num_cols is not None:
            df[num_cols] = df[num_cols].replace('[\$,()-]', '', regex=True)
            df[num_cols] = df[num_cols].apply(pd.to_numeric, errors=num_errors)

        if rename_cols is not None:
            df.rename(columns=rename_cols, inplace=True)

    except (TypeError, ValueError) as err:
        print('Error, one of the columns may not be a valid date or number',
              ' column. The following error was was observed:', err)

    return df


def retrieve_df(credentials, sheet_id, range_name=None, sheet_name=None,
                sheet_range=None, date_cols=None, num_cols=None,
                rename_cols=None, headers=True, drop_null=True):
    """Retrieve a DataFrame from a Google Sheet. Clean it up using the
    column_tools function if date_cols, num_cols, or rename_cols are
    defined. Either range_name or sheet_name + sheet_range should be
    passed in.

    Args:
        credentials: valid credentials with access to file and write
            scope
        sheet_id: the id for the sheet, from url: e.g.,
            https://docs.google.com/spreadsheets/d/1AEUuw_Nbm8f1iF1PKRfjjZ2xPmfr1bCBcRLGtnNwoUg/edit#gid=0
            this part:  1AEUuw_Nbm8f1iF1PKRfjjZ2xPmfr1bCBcRLGtnNwoUg
        range_name (optional): combined name and range for Sheet, e.g.,
            Sheet1!A1:B6
        sheet_name (optional): the name of Google Sheet (default is Sheet1)
        sheet_range (optional): the range of cells to get
            e.g.,   A1:H is all rows from columns A through H
                    B4:E6 is columns B through E, rows 4, 5, and 6
                    or use a named range: e.g., test_range
        date_cols (optional): a list of columns to convert to dates
        num_cols (optional): a list of columns to strip out non-numeric
            symbols ($,%) and convert to numeric
        rename_cols (optional): a dict of columns to rename
            e.g., {'ymd': 'Date'})
        headers (optional): whether the first row retrieved is the list
            of columns (default True) or not (False). If False, the
            DataFrame will have numbers as columns, e.g., 0, 1, .., n
        drop_null (optional): whether to drop rows with all null values
            (default True) or not (False)

    Returns:
        df: the retrieved and cleaned up DataFrame
    """

    # retrieve using get_rows function
    output_list = get_rows(credentials, sheet_id, range_name, sheet_name,
                           sheet_range)

    if headers:
        df = pd.DataFrame(
            output_list, columns=output_list[0]).drop(0).reset_index(drop=True)
    else:
        df = pd.DataFrame(output_list)

    if drop_null:
        df.dropna(axis=0, how='all', inplace=True)

    # if date_cols is not None or num_cols is not None or rename_cols is not None:
    if date_cols or num_cols or rename_cols:
        df = column_tools(df, date_cols, num_cols, rename_cols)

    return df


def upload_df(df, credentials, sheet_id, range_name=None, sheet_name=None,
              sheet_range=None, date_cols=None,
              fill_nan_inf=True, sort_cols=False, verbose=False):
    """Prepare and upload a DataFrame to Google Sheets. Either
    range_name or sheet_name + sheet_range should be passed in.


    Args:
        df: the DataFrame to upload
        credentials: valid credentials with access to file and write
            scope
        sheet_id: the id for the sheet, from url: e.g.,
            https://docs.google.com/spreadsheets/d/1AEUuw_Nbm8f1iF1PKRfjjZ2xPmfr1bCBcRLGtnNwoUg/edit#gid=0
            this part:  1AEUuw_Nbm8f1iF1PKRfjjZ2xPmfr1bCBcRLGtnNwoUg
        range_name (optional): combined name and range for Sheet, e.g.,
            Sheet1!A1:B6
        sheet_name (optional): the name of Google Sheet (default is Sheet1)
        sheet_range (optional): the range of cells to get
            e.g.,   A1:H is all rows from columns A through H
                    B4:E6 is columns B through E, rows 4, 5, and 6
                    or use a named range: e.g., test_range
        date_cols (optional): a list of columns to convert from datetime
            to string dates. Error will be raised if attempting to
            upload datetime objects to Sheets.
        fill_nan_inf (optional): fill NaN and Inf values (default True).
            If False, error will be raised if attempting to upload NaN
            or Inf values to Sheets.
        sort_cols (optional): False (default) or True to alphabetically
            sort the columns
        verbose (optional): False (default) will return only basic info
            from job, True will return complete info, None will return
            nothing (unless unexpected values are found
    """

    # first need to convert any date columns to datetime (in case of mixed
    # datetime and string dates) then convert back to string
    if date_cols:  # is not None:
        df = column_tools(df, date_cols)
        for date_col in date_cols:
            df[date_col] = df[date_col].dt.strftime('%Y-%m-%d')

    # fill any NaN or inf or NaT
    if fill_nan_inf:
        df.fillna(0, inplace=True)
        df.replace(np.inf, 0, inplace=True)

    # sort columns alphabetically (in the event that various sets reorder
    # columns)
    if sort_cols:
        df = df.reindex(sorted(df.columns), axis=1)

    response_list = list()
    # clear sheet prior to uploading
    response_list.append(
        clear_rows(credentials, sheet_id, range_name, sheet_name, sheet_range,
                   verbose)
    )

    # convert df to lists then upload
    upload_list = [df.columns.tolist()] + df.values.tolist()
    response_list.append(
        update_rows(upload_list, credentials, sheet_id, range_name, sheet_name,
                    sheet_range, verbose=verbose)
    )

    for response in response_list:  # print any responses
        if response:
            print(response)


def batch_get_data_from_ranges(credentials, sheet_id, range_list,
                               common_cols=None):
    """Get batched data using named ranges.


    Args:
        credentials: valid credentials with access to file and write
            scope
        sheet_id: the id for the sheet, from url: e.g.,
            https://docs.google.com/spreadsheets/d/1AEUuw_Nbm8f1iF1PKRfjjZ2xPmfr1bCBcRLGtnNwoUg/edit#gid=0
            this part:  1AEUuw_Nbm8f1iF1PKRfjjZ2xPmfr1bCBcRLGtnNwoUg
        range_list: a list of named ranges for which to retrieve Sheet
            data, e.g., ['HKYNA_CC', 'HKYNA_PL', ...]
        common_cols (optional): if not None (default) pass in list of
            column names to keep
    """

    service = discovery.build('sheets', 'v4', credentials=credentials)
    sh_resp = service.spreadsheets().values().batchGet(
        spreadsheetId=sheet_id, ranges=range_list
    ).execute()

    f_lst = []

    for idx, vr in enumerate(sh_resp['valueRanges']):
        v = vr['values']
        f = pd.DataFrame(data=v[1:], columns=v[0])

        # keep only the common columns
        if common_cols:
            f = f[common_cols]

        rng = vr['range']
        f['SheetRange'] = rng
        f_lst.append(f)

    return pd.concat(f_lst)
