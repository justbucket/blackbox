
# Functions with no test

## googleads.py

- text2cols - done
- get_shared_budgets - done
<!-- - print_response - not neede; no return value
- print_errors - not needed; no return value  -->
- create_bj_ops_for_campaigns - done
- upsert_bj_ops_for_campaigns 
- run_batch_create - done
- set_campaign_level_targeting - done
- create_shared_negatives_set - done
- update_negatives_in_set - done
- attach_negatives_to_campaigns - done
- remove_negatives_from_campaigns - done
- create_account_extensions - done
- create_campaign_extensions - done
- create_adgroup_extensions - done
- set_campaign_device_bid_modifiers - done
- create_tCPA_strategies - done
- get_shared_bidding_strategies - done
- attach_fbs_to_campaigns - done
- create_text_label - done
- attach_label - done
- pause_ads_by_id - done
- create_eta_ads_from_df - done
- get_eta_performance - done
- get_live_ad_count - done
- create_final_urls_lookup_from_df - done
- update_kws_final_url_from_df - done
- batch_update_kws_final_url_from_df - done
- check_ads_kws_final_urls_alignment
- create_rsa_op - done




- get_all_but_removed_campaigns_ad_adgroups - done
- get_all_shared_sets - done
- get_active_exact_kws - done
- get_label_ids_by_name - done
- get_report_as_df - doned
- get_report_as_df_threaded
- clean_adwords_df_from_ui - done
- clean_adwords_df_from_api


